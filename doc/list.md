dtml/list.h
===========

This library is a small collection of functions operating on lists (mostly from PreludeList).

Additionally, bind and lambda can receive their full definitions here, as they rely on few list operators.

## Table of contents
* [singletons](#the-following-singletons-ie-plain-c-types-are-defined-in-this-header)
* [functions](#the-following-functions-ie-lazy-dtml-templates-are-defined-in-this-header)
## The following singletons (i.e. plain C++ types) are defined in this header

- **[`naturals`](#naturals)**

### `naturals`
The list of all natural numbers*.
---
<sup>* Actually, up to the largest `size_t` value, and then foldover back to zero. Infinite precision integrals are currently not a part of this library but may be added later.</sup>

## The following functions (i.e. lazy DTML templates) are defined in this header

- **[`transform`](#transform)**
- **[`flat_transform`](#flat_transform)**
- **[`filter`](#filter)**
- **[`take`](#take)**
- **[`take_while`](#take_while)**
- **[`drop`](#drop)**
- **[`drop_while`](#drop_while)**
- **[`at`](#at)**
- **[`repeat`](#repeat)**
- **[`iterate`](#iterate)**
- **[`iota`](#iota)**

### `transform`
Map a function to a list.


#### Params
|||
|---|---|
|`car<L>`|a callable to be invoked on every element of the list|
|`cdr<L>`|a list|

#### Returns
a list comprised of results of the callable being applied to every element of the list

---

### `flat_transform`
Map a function to a list, concatenating the results.


#### Params
|||
|---|---|
|`car<L>`|a callable to be invoked on every element of the list|
|`cdr<L>`|a list|

#### Returns
a list comprised of results of the callable being applied to every element of the list, concatenated

---

### `filter`
Filter a list through a predicate, discarding all the elements dissatisfying the latter.

The closest analog from STL would be `std::remove_if`, save for the semantics of predicate is inverted there: while `filter` retains all the elements on which the predicate returns true,
`remove_if` does the inverse and discards them.


#### Params
|||
|---|---|
|`car<L>`|a callable that returns a truth value (i.e. castable to bool) on each element of the list|
|`cdr<L>`|a list to be filtered|

#### Returns
filtered list

---

### `take`
Take the prefix of specified length from a given list.


#### Params
|||
|---|---|
|`car<L>`|an integral value|
|`cdr<L>`|a list|

#### Returns
a list comprised of first `car<L>` elements of `cdr<L>`, or the whole `cdr<L>` if it's shorter

---

### `take_while`
Return the longest prefix of elements satisfying a predicate.


#### Params
|||
|---|---|
|`car<L>`|a callable that returns a truth value (i.e. castable to `bool`) on each element of the list, up until the first one that doesn't satisfy it|
|`cdr<L>`|a list|

#### Returns
_see above_

---

### `drop`
Return suffix of the list past first N elements.

The closest analog from STL would be `std::advance` of a container's `begin()`.


#### Params
|||
|---|---|
|`car<L>`|amount of elements to be dropped|
|`cdr<L>`|a list|

#### Returns
a list comprised of all `cdr<L>`'s elements past first `car<L>` of them

---

### `drop_while`
Return suffix of the list starting with the first element that does not satisfy the predicate.


#### Params
|||
|---|---|
|`car<L>`|a callable that returns a truth value (i.e. castable to `bool`) on each element of the list, up until the first element on which it returns a false value|
|`cdr<L>`|a list|

#### Returns
_see above_

---

### `at`
Take list element by index, zero-based.

It is an error if index is equal to, or greater than, the length of the list, or the list itself is empty.
On the other hand, if index in negative and list non-empty, the first element of the list is returned.


#### Params
|||
|---|---|
|`car<L>`|an integral value|
|`cdr<L>`|a list|

#### Returns
`car<L>`'th element of the list, zero based, or the first one, when index is negative

---

### `repeat`
Infinite repetition of the same value(s).


#### Params
|||
|---|---|
|`L`|list of values to cyclically repeat|

#### Returns
an infinite list comprised of L's elements, looped

---

### `iterate`
Build a list by infinitely applying a callable to initial value(s) (those latter can be empty).


#### Params
|||
|---|---|
|`car<L>`|a callable|
|`cdr<L>`|starting values|

#### Returns
a list starting with the first element of `cdr<L>`, if any, then with `car<L>` applied to `cdr<L>`, and then with each subsequent element being equal to `car<L>` applied to its predecessor

---

### `iota`
Python-style range function.

When called with only one argument N it returns a sequence of natural numbers from 0 to N-1, inclusive.<br/>
When called with two arguments, N and M, it returns the sequence [N, N+1, ..., M-1] (empty list if M <= N).<br/>
With three arguments, N, M and K, the sequence returned is [N, N+K, ..., J] where J < M <= J+K (or J+K <= M < J, with negative K). It is an error if K is zero.


#### Params
|||
|---|---|
|`car<L>`|N (_see above_)|
|`cadr<L>`|M (_see above_)|
|`caddr<L>`|K (_see above_)|

#### Returns
_see above_
