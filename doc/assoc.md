dtml/assoc.h
============

This library introduces a naive key-value storage, mostly a list of pairs (inspired by Scheme's `assoc` standard function), with possibly a default element for failed lookups.

## Table of contents
* [functions](#the-following-functions-ie-lazy-dtml-templates-are-defined-in-this-header)
## The following functions (i.e. lazy DTML templates) are defined in this header

- **[`insert`](#insert)**
- **[`insert_default`](#insert_default)**
- **[`insert_item`](#insert_item)**
- **[`unordered_map`](#unordered_map)**
- **[`assoc`](#assoc)**
- **[`contains`](#contains)**
- **[`lookup`](#lookup)**
- **[`map_union`](#map_union)**
- **[`map_equal_to`](#map_equal_to)**

### `insert`
Add a key-val pair to assoc list, update the value if key is found on the list.

Note: technically, this function works even if its list argument is not truly assoc (e.g. has duplicates or non-assoc structure past the element with the key updated).

Note 2: both calls are possible, `insert<Key, Val, Map>` and `using KeyVal = q<Key, Val>; insert<KeyVal, Map>`.


#### Params
|||
|---|---|
|`car<L>`|a key to insert or update|
|`cadr<L>`|associated value|
|`cddr<L>`|an assoc list|

#### Returns
the updated list

---

### `insert_default`
Update or create the default value in an assoc list

Note: technically, this function works even if its list argument is not truly assoc (e.g. has duplicates or non-assoc structure past the default updated).


#### Params
|||
|---|---|
|`car<L>`|new default value for failed lookups|
|`cdr<L>`|an assoc list|

#### Returns
the updated list

---

### `insert_item`
The superposition of the above: add/update a key-val pair, if the first argument is a pair (`q<Key, Val>`), or add/update default otherwise.

Note: a key-val item should be quoted twice (`insert_item_t<q<q<Key, Val>>, map>`) or be the head of the arg list (`insert_item_t<cons_t<q<Key, Val>, map>>`).


#### Params
|||
|---|---|
|`car<L>`|either a key-value association, or a new default element|
|`cdr<L>`|an assoc list|

#### Returns
the updated list

---

### `unordered_map`
assoc list

Accepts a flat list and splits it into pairs, so that `<Key1, Val1, Key2, Val2, ...>` becomes `<<Key1, Val1>, <Key2, Val2>, ...>`.

If the list is finite and has odd number of elements, the last element becomes the 'default' value that is returned (inside a singleton suffix) for unknown keys.


#### Params
|||
|---|---|
|`L`|list of keys and values, interspersed|

#### Returns
L split into pairs

---

### `assoc`
Accepts a key Key and a list of key-val pairs `<<Key1, Val1>, ...>`, and returns the suffix of list starting with `<Key, Val>`.

If an element of the list is a singleton (a list of one element), it is regarded as default and the suffix starting with it is returned unconditionally, provided no key to the left of it matches.

Note: technically, this function works even if its list argument is not truly assoc (e.g. has duplicates or non-assoc structure past the element with the key queried).


#### Params
|||
|---|---|
|`car<L>`|index to lookup in the assoc list|
|`cdr<L>`|assoc list of pairs|

#### Returns
suffix of cdr<L> such that the key in its first element is Idx (nil if not found)

---

### `contains`
Boolean predicate whether an assoc list contains a key.

Note: technically, this function works even if its list argument is not truly assoc (e.g. has duplicates or non-assoc structure past the element with the key queried).


#### Params
|||
|---|---|
|`car<L>`|index to lookup in the assoc list|
|`cdr<L>`|assoc list of pairs|

#### Returns
true_value iff this list contains the key

---

### `lookup`
Accepts an index Idx and a list of key-val pairs <<Key1, Val1>, ...>, and returns the value associated with Idx in the list.

If an element of the list is a singleton (a list of one element), it is regarded as default and is returned unconditionally, provided no key to the left of it matches.

Note: technically, this function works even if its list argument is not true-assoc (e.g. has duplicates or non-assoc structure past the element with the key queried).


#### Params
|||
|---|---|
|`car<L>`|index to lookup in the assoc list|
|`cdr<L>`|assoc list of pairs|

#### Returns
suffix of cdr<L> such that the key in its first element is Idx (null if not found)

---

### `map_union`
Merge assoc maps, eliminating duplicates.

The associations appear in the final result in first-come order: each key is associated with its leftmost value in simply concatenated maps, and default value, if any, is the leftmost one.


#### Params
|||
|---|---|
|`L`|assoc lists|

#### Returns
all the lists concatenated, with duplicates removed

---

### `map_equal_to`
Check maps for equality.

Two maps M1 and M2 are equal iff for every key-value association [K1, V1] in M1 there's an association [K2, V2] in M2 such that K2 is almost same as K1, and V2 is almost same as V1,
and either both maps don't have defaults, or they both have defaults that are almost same.

Note: it is not checked whether the two lists both have assoc structure, but chances are, if they don't, they are not equal.


#### Params
|||
|---|---|
|`L`|assoc list|
|`R`|assoc list|

#### Returns
whether L and R are equal
