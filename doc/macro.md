dtml/macro.h
============

This file contains limited versions of a few useful macros. Currently, as a proper macro language was never a goal for this library, they are of limited usefulness (tailored to code generation for a few dtml classes) but otherwise are not very expressive, as C++ macros can be made to be, with some effort.

Macros follow the namespace convention: their names all start with `DTML_`, and their definitions are all written inside `namespace dtml {` and `}` braces, just for the fun of it. As a rule, they usually have to be invoked inside the aforementioned namespace, save for `DTML_SHOW`, defined in **show.h**, that needs to be invoked from the top-level namespace (see [show.h](show.md)).
