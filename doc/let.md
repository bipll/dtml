dtml/let.h
==========

This library introduces let binding and pattern matching/decomposition mechanism for C++ types.

In brief, a let expression consists of a sequence of pattern-value pairs, that define bindings for variables, and an expression to be evaluated,
that may contain free variables that can be bound by matching against values. When all the patterns from bindings match their respective values, let expression returns the evaluated expression,
with free variables replaced with their respective bindings.

Few examples below illustrate this (note that `dtml::` qualification is omitted in DTML identifiers, as if by `using namespace dtml;` beforehand).

1. A pattern may simply be a literal (i.e. a plain C++ type), in which case it matches itself, or an equivalent type, in the sense of [`almost_same`](basic.md#almostsame).
   ```cpp
   using String = std::string;

   using CharList = let_t<       // let invocation
           // pattern , value
           std::string, String,
           // evaluated expression
           std::list<char>>;

   static_assert(std::is_same_v<CharList, std::list<char>>);

   // using NoCharList = let_t<
   //         std::list<char>, String,  // different types — no match
   //         std::list<char>>;         // cannot be evaluated
   ```
2. A pattern may be a variable, that is, `dtml::var<n>`.
   ```cpp
   using CharList = let_t<        // let invocation
           // pattern, value
           var<'c'>  , char,      // var<'c'> is bound to char
           // evaluated expression
           std::list<var<'c'>>>;

   static_assert(std::is_same_v<CharList, std::list<char>>);
   ```
   Of course, to use string literals as variable identifiers, you'd need to wrap them in a literal type, say, [`dtml::string`](string.md):
   ```cpp
   using CharList = let_t<                   // let invocation
           // pattern, value
           var<string{"char"}>, char,
           // evaluated expression
           std::list<var<string{"char"}>>>;

   static_assert(std::is_same_v<CharList, std::list<char>>);
   ```
3. Another irrefutable pattern is `dtml::ignore` (in languages less verbose it is often called simply `_`). Like `std::ignore`, it matches any value (and creates no bindings):
   ```cpp
   using String = std::string;

   using CharList = let_t<
           ignore, String,    // matches irrefutably
           std::list<char>>;  // and creates no bindings

   static_assert(std::is_same_v<CharList, std::list<char>>);
   ```
4. A pattern may be a template instantiation whose parameters are also patterns. It matches an instantiation of the same template when all parameter patterns match the actual type's parameters:
   ```cpp
   using String = std::string;  // std::basic_string<char>

   using CharList = let_t<                       // let invocation
           std::basic_string<var<'c'>>, String,  // pattern, value — var<'c'> is bound to char, first of String's parameters
           std::list<var<'c'>>>;                 // evaluated expression

   static_assert(std::is_same_v<CharList, std::list<char>>);
   ```
   The same variable can be bound several times in the same let-expression. In such a case, all the bindings must be identical, in the sense of [`almost_same`](basic.md#almostsame).
   (Note that it actually implicitly happens in the example above, as `std::basic_string` has parameter `Traits` defaulting to `std::char_traits`,
   so `std::char_traits<char>` in `String` is matched implicitly against `std::char_traits<var<'c'>>` in the pattern.)
   ```cpp
   using Point2D = std::pair<double, double>;

   using AnnotatedValue = std::pair<double, std::string>;

   using Real = let_t<
           std::pair<var<'R'>, var<'R'>>, Point2D,  // both times the variable matches the same type
           var<'R'>>;

   static_assert(std::is_same_v<Real, double>);

   // using Real = let_t<
   //         std::pair<var<'R'>, var<'R'>>, AnnotatedValue,  // two different values for the same variable, no match
   //         var<'R'>>;
   ```
   Mind the implicit bindings, like the one described above. For instance, this binding should fail:
   ```cpp
   using DamagedString = std::basic_string<char, std::char_traits<char>, std::allocator<std::char16_t>>;  // a strange instance

   // using DamagedCharList = let_t<
   //         std::basic_string<var<'c'>>, DamagedString,  // error — no match, var<'c'> is bound to char and thus std::allocator<var<'c'>> does not match std::allocator<std::char16_t>
   //         std::list<var<'c'>>>;
   ```
   One special pattern is an instantiation of [`dtml::invoke`](basic.md). It matches any type which is an instantiation of a template with an appropriate number of arguments,
   and its first argument can as well be a pattern, matching the corresponding (boxed) template:
   ```cpp
   using String = std::string;
   using IntList = std::list<int>;

   using CharList = let_t<
           std::basic_string<var<'c'>>,      String,   // var<'c'> is bound to char
           invoke<var<'l'>, ignore, ignore>, IntList,  // var<'l'> is bound to (boxed) std::list
           invoke<var<'l'>, var<'c'>>>;                // one bound argument invoked on another

   static_assert(std::is_same_v<CharList, std::list<char>>);
   ```
   Note how several pattern-value matches can be combined in the same let-expression.
5. One special, var-like, type of patterns is called `dtml::seq`. It is different from `dtml::var` in that it is designed specifically to match type parameter packs, or suffixes thereof.
   When matches, it is bound to [`dtml::q`](box.h) of actual parameters.
   ```cpp
   using Tuple = DTML_FUNC_BOX(std::tuple);  // boxed std::tuple

   using MyTuple = std::tuple<int, double, char, std::string>;

   using TupleSuffix = let_t<
           std::tuple<ignore, ignore, seq<'S'>>, MyTuple,  // seq<'S'> is bound to q<char, std::string>
           apply<Tuple, seq<'S'>>>;                        // construct tuple with seq<'S'> elements as arguments

   static_assert(std::is_same_v<TupleSuffix, std::tuple<char, std::string>>);
   ```
   To ignore the whole (sub)sequence, use `ignore{}` as seq's parameter (ignore is a literal type):
   ```cpp
   using MyTuple = std::tuple<int, double, char, std::string>;

   using TuplePrefix = let_t<
           std::tuple<var<0>, var<1>, seq<ignore{}>>, MyTuple,
           std::tuple<var<0>, var<1>>>;

   static_assert(std::is_same_v<TuplePrefix, std::tuple<int, double>>);
   ```
6. vars and seqs can be used in value parts of bindings same as in expression. They are not evaluated until the expression itself is.
   ```cpp
   using String = std::string;

   using CharList = let_t<
           std::basic_string<var<'c'>>, String,  // var<'c'> is bound to char
           var<'l'>, std::list<var<'c'>>,        // var<'l'> is bount to std::list<char>
           var<'l'>>;

   static_assert(std::is_same_v<CharList, std::list<char>>);

## TODO
Currently, `let` is abundantly strict (e.g. all the bindings are parsed and forced before evaluating the expression). This might change in the future. For instance,
```cpp
using CharList = let_t<
        var<0>, char,
        std::pair<var<string{"ambiguous var"}>, var<string{"ambiguous var"}>>, std::pair<int, double>,  // no strict match
        std::list<var<0>>>;                                                                             // unambiguously evaluated, even though var<string{"ambiguous var"}> is unbindable
```
could yield a concrete type even though not all patterns can be matched against all values.

This let is non-recursive. `letrec` is to be implemented somewhere in the future.

Pattern packs, possibly `seq`-ended, can only be bound to real template parameter packs.
It might prove useful, and not very hard to implement, to extend their area of definition on arbitrary DTML lists, especially infinite ones.

## Table of contents
* [singletons](#the-following-singletons-ie-plain-c-types-are-defined-in-this-header)
* [simple types](#the-following-simple-types-ie-simple-c-templates-are-defined-in-this-header)
* [functions](#the-following-functions-ie-lazy-dtmL templates)-are-defined-in-this-header)
## The following singletons (i.e. plain C++ types) are defined in this header

- **[`ignore`](#ignore)**

### `ignore`
Irrefutable wildcard pattern.

## The following simple types (i.e. simple C++ templates) are defined in this header

- **[`var`](#var)**
- **[`seq`](#seq)**

### `var`
Variable pattern.

When matched successfully, variable becomes bound to the respective value that is to be substituted for all occurences of this variable in resulting expression.

When unique across all binding pairs, this pattern is irrefutable and becomes bound to the corresponding value.

When two or more entries of var<n> with the same n are found in patterns they should correspond to the same type in values, otherwise there's no match.


---

### `seq`
Sequence pattern.

This pattern is used as the rightmost template parameter and matches the rest of the pack, not consumed by its siblings to the left. Think of it as the last parameter in a parameter pack,
the one with elipsis:

```cpp
template<class T> struct RebindToIntDouble;
template<template<class...> class F, class A, class B, class... Rest> struct RebindToIntDouble<F<A, B, Rest...>> {
    using type = std::tuple<int, double, Rest...>;
};
```
is roughly equivalent to
```cpp
using namespace dtml;
template<class T> using RebindToIntDouble = let<
                                                    // F     , A     , B     , Rest...
                                                invoke<ignore, ignore, ignore, seq<'R'>>, T,
                                                std::tuple<int, double, seq<'R'>>>;
```
Note how `seq<'R'>` is spliced in parameter pack in the resulting expression, same as `Rest...` would do.

Otherwise, seq follows the same rules as var.

## The following functions (i.e. lazy DTML templates) are defined in this header

- **[`let`](#let)**

### `let`
Attempts to match patterns against values and, if there's a match, binds variables to corresponding subexpressions and evaluates the resulting expression.


#### Params
|||
|---|---|
|`init<L>`|interpersed pattern-value pairs|
|`last<L>`|an expression to be evaluated|

#### Returns
the result of expression with free variables bound by pattern matching

#### Exceptions
- an exception if there's no match

