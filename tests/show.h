#ifndef TESTS__SHOW_H_4e494a33503192c6df98ba1cf0a4446f95062769
#define TESTS__SHOW_H_4e494a33503192c6df98ba1cf0a4446f95062769

#include <iomanip>
#include <iostream>

#include <dtml/show.h>

struct print {
	template<class T> print &operator<<(T &&t) {
		std::cout << std::boolalpha << t;
		return *this;
	}
	template<class T> print &operator,(T &&t) {
		std::cout << std::boolalpha << t;
		return *this;
	}
	~print() { std::cout << "\n\n"; }
};

#define print print() <<
#define PRINT(...) print std::setw(64) << std::left << #__VA_ARGS__ << ": " << __VA_ARGS__
#define SHOW(...) print std::setw(64) << std::left << #__VA_ARGS__ << ": " << show_v<__VA_ARGS__>

#endif
