#ifndef TESTS__FIXTURE_H_9c2aaec7d973368c15d89d6a65ac897b9a46dfa7
#define TESTS__FIXTURE_H_9c2aaec7d973368c15d89d6a65ac897b9a46dfa7

#include <iostream>
#include <stdexcept>
#include <string>
#include <tuple>
#include <type_traits>
#include <unordered_map>

#include <dtml/box.h>


namespace test_fixture {

template<auto v> inline constexpr dtml::bool_value<v> assert_true() noexcept { return {}; }

template<dtml::HasValue V> inline constexpr dtml::to_bool_t<V> assert_true() noexcept { return {}; }


template<auto v> inline constexpr dtml::bool_value<!v> assert_false() noexcept { return {}; }

template<dtml::HasValue V> inline constexpr dtml::logical_not_t<V> assert_false() noexcept { return {}; }


template<auto actual, auto expected> inline constexpr dtml::bool_value<(actual == expected)> assert_eq() noexcept { return {}; }


template<class Actual, class Expected> inline constexpr std::is_same<Actual, Expected> assert_eq() noexcept(noexcept(Expected{}, Actual{})) {
	Actual act = Expected{};
	Expected exp = Actual{};
	return {};
}


#define EXPECT_TRUE(...) { auto EXPECT_TRUE = test_fixture::assert_true<__VA_ARGS__>(); }

#define EXPECT_FALSE(...) { auto EXPECT_FALSE = test_fixture::assert_false<__VA_ARGS__>(); }

#define EXPECT_EQ(...) { auto EXPECT_EQ = test_fixture::assert_eq<__VA_ARGS__>(); }


#define ASSERT_TRUE(...) { auto ASSERT_TRUE = test_fixture::assert_true<__VA_ARGS__>(); static_assert(ASSERT_TRUE.value); }

#define ASSERT_FALSE(...) { auto ASSERT_FALSE = test_fixture::assert_false<__VA_ARGS__>(); static_assert(ASSERT_FALSE.value); }

#define ASSERT_EQ(...) { auto ASSERT_EQ = test_fixture::assert_eq<__VA_ARGS__>(); static_assert(ASSERT_EQ.value); }


// How does one implement a nullable bool in C prepro language?
#if defined RUN_DISABLED && RUN_DISABLED
#	define DISABLE
#else
#	define DISABLE(...) ++test_fixture::disabled_tests

std::size_t disabled_tests{};

#endif


#define test(...)													\
THIS_TEST_EXISTS_ ## __VA_ARGS__();											\
auto LOCATION_FOR_ ## __VA_ARGS__ = test_fixture::one_more_test(#__VA_ARGS__, THIS_TEST_EXISTS_ ## __VA_ARGS__);	\
void THIS_TEST_EXISTS_ ## __VA_ARGS__()

#define TEST void test


using test_case = std::function<void()>;

inline std::unordered_map<std::string, test_case> test_table;

inline bool one_more_test(std::string key, auto value) {
	auto [itr, fresh] = test_table.try_emplace(std::move(key), std::move(value));
	if(!fresh) throw std::invalid_argument("There is already a test named " + key);
	return fresh;
}

}


int main() {
	std::cout << "As it has compiled, all tests passed.\n";
	for(auto &&[_, test_case]: test_fixture::test_table) test_case();
#if defined RUN_DISABLED && RUN_DISABLED
#else
	if(test_fixture::disabled_tests) std::cout << "\nYou have " << test_fixture::disabled_tests << " disabled test" << (test_fixture::disabled_tests == 1? "" : "s") << ".\n";
#endif
}

#endif
