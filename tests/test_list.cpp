#include <string>
#include <utility>

#include <dtml/basic.h>
#include <dtml/box.h>
#include <dtml/list.h>

#include "fixture.h"

#include "show.h"

using args = DTML_FUNC_BOX(dtml::args_t);

TEST(transform) {
	using namespace dtml;

	ASSERT_EQ(transform_t<quote, zero_value, one_value, int, double>,
		  q<q<zero_value>, q<one_value>, q<int>, q<double>>);

	ASSERT_EQ(transform_t<quote, q<zero_value, one_value, q<int, double>>>,
		  q<q<zero_value>, q<one_value>, q<q<int, double>>>);

	ASSERT_EQ(transform_t<args, std::pair<int, char>, std::pair<float, double>>,
		  q<q<int, char>, q<float, double>>);

	ASSERT_EQ(transform_t<args>, nil);
	ASSERT_EQ(transform_t<quote>, nil);

	// if you take the set of all the natural numbers, zero-based, and increase every one of them by one,
	// the lower bound you get will equal one
	/*
	ASSERT_EQ(car_t<invoke<transform, inc, naturals>>,
		  one_size);
		  */
	SHOW(transform_t<quote, q<zero_value, one_value, q<int, double>>>);
	//SHOW(transform_t<quote, naturals);
	print "-----------------------------------------";
#define PROBE(...) __VA_ARGS__ x = 42;
	ASSERT_EQ(car_t<strict::transform<inc, naturals>>,
		  one_size);
	PRINT(finite_v<naturals>);
	PRINT(infinite_v<naturals>);
	SHOW(cons_t<zero_value, naturals>);
	SHOW(strict::sequence<invoke<transform, inc, naturals>>);

	//PRINT(finite_v<strict::transform<inc, naturals>>);
	//PRINT(infinite_v<strict::transform<inc, naturals>>);
	//SHOW(car_t<transform_t<inc, naturals>>);


	
	
	// using blya = type_t<wat>;
	//SHOW(unbox<dtml::strict::call<dtml::func_box<dtml::strict::transform>::func, dtml::quote, dtml::strict::iterate<dtml::q<dtml::func_box<dtml::strict::inc>, dtml::value_box<0> > > >::type::type>);
	/*
	SHOW(transform_t<inc, naturals>);
	PRINT(LongerThan<naturals, 0>);
	PRINT(LongerThan<naturals, 1>);
	PRINT(longer_than_v<zero_size, naturals>);
	PRINT(longer_than_v<zero_size, concat_t<inc, naturals>>);
	PRINT(longer_than_v<one_size, concat_t<inc, naturals>>);
	PRINT(longer_than_v<size_value<1>, concat_t<naturals>>);
	PRINT(longer_than_v<size_value<0>, concat_t<naturals>>);
	PRINT(empty_v<concat_t<naturals>>);
	PRINT(empty_v<naturals>);
	PRINT(longer_than_v<zero_size, cdr_t<naturals>>);
	SHOW(cdr_t<naturals>);
	PRINT(empty_v<cdr_t<naturals>>);

	ASSERT_EQ(car_t<strict::transform_t<inc, naturals>>,
		  one_size);
		  */
	/*
	SHOW(car_t<transform_t<inc, naturals>>);
	//SHOW(get_type<strict::cdr<strict::iterate<func_box<strict::inc>, value_box<0> >>>);
	PRINT(HasType<strict::cdr<strict::iterate<func_box<strict::inc>, value_box<0> >>>);
	PRINT(Boxed<strict::cdr<strict::iterate<func_box<strict::inc>, value_box<0> >>>);
	SHOW(unbox_t<strict::cdr<strict::iterate<func_box<strict::inc>, value_box<0> >>>);
	*/
}

#if 0
TEST(flat_transform) {
	using namespace dtml;

	ASSERT_EQ(flat_transform_t<args, std::pair<int, char>, std::pair<float, double>>,
		  q<int, char, float, double>);

	ASSERT_EQ(flat_transform_t<args>, nil);
	ASSERT_EQ(flat_transform_t<quote>, nil);
}

TEST(filter) {
	using namespace dtml;

	ASSERT_EQ(filter_t<to_bool, zero_value, one_value, zero_size, one_size, false_value, zero_value, true_value>,
		  q<one_value, one_size, true_value>);

	ASSERT_EQ(filter_t<to_bool, one_value, one_size, true_value>,
		  q<one_value, one_size, true_value>);

	ASSERT_EQ(filter_t<to_bool, zero_value, zero_size, false_value, zero_value>,
		  nil);
}

TEST(take) {
	using namespace dtml;

	ASSERT_EQ(take_t<size_value<3>, int, double, char, std::string, float, size_value<0>>,
		  q<int, double, char>);

	ASSERT_EQ(take_t<size_value<3>, int, double>,
		  q<int, double>);

	ASSERT_EQ(take_t<size_value<0>, int, double>,
		  nil);

	ASSERT_EQ(take_t<value_box<-1>, int, double>,
		  nil);

	ASSERT_EQ(take_t<value_box<-2>, int, double>,
		  nil);

	ASSERT_EQ(take_t<size_value<3>>,
		  nil);

	ASSERT_EQ(take_t<size_value<3>, naturals>,
		  q<zero_size, one_size, size_value<2>>);

	ASSERT_EQ(take_t<size_value<0>, naturals>,
		  nil);

	ASSERT_EQ(take_t<value_box<-1>, naturals>,
		  nil);

	ASSERT_EQ(take_t<value_box<-2>, naturals>,
		  nil);
}

TEST(take_while) {
	using namespace dtml;

	ASSERT_EQ(take_while_t<to_bool, one_size, size_value<2>, true_value, value_box<-1>, zero_value, size_value<3>, true_value>,
		  q<one_size, size_value<2>, true_value, value_box<-1>>);

	ASSERT_EQ(take_while_t<to_bool, false_value, one_size, size_value<2>, true_value, value_box<-1>, zero_value, size_value<3>, true_value>,
		  nil);

	ASSERT_EQ(take_while_t<to_bool>,
		  nil);

	ASSERT_EQ(take_while_t<to_bool, cons_t<true_value, naturals>>,
		  q<true_value>);

	ASSERT_EQ(take_while_t<to_bool, naturals>,
		  nil);
}

TEST(drop) {
	using namespace dtml;

	ASSERT_EQ(drop_t<size_value<3>, int, double, char, std::string, float, size_value<0>>,
		  q<std::string, float, size_value<0>>);

	ASSERT_EQ(drop_t<size_value<3>, int, double>,
		  nil);

	ASSERT_EQ(drop_t<size_value<0>, int, double>,
		  q<int, double>);

	ASSERT_EQ(drop_t<value_box<-1>, int, double>,
		  q<int, double>);

	ASSERT_EQ(drop_t<value_box<-2>, int, double>,
		  q<int, double>);

	ASSERT_EQ(drop_t<size_value<3>>,
		  nil);

	// no infinite lists here
}

TEST(drop_while) {
	using namespace dtml;

	ASSERT_EQ(drop_while_t<to_bool, one_size, size_value<2>, true_value, value_box<-1>, zero_value, size_value<3>, true_value>,
		  q<zero_value, size_value<3>, true_value>);

	ASSERT_EQ(drop_while_t<to_bool, false_value, one_size, size_value<2>, true_value, value_box<-1>, zero_value, size_value<3>, true_value>,
		  q<false_value, one_size, size_value<2>, true_value, value_box<-1>, zero_value, size_value<3>, true_value>);

	ASSERT_EQ(drop_while_t<to_bool>,
		  nil);

	/*
	ASSERT_EQ(drop_while_t<to_bool, cons<true_value, naturals>>,
		  naturals);
		  */

	ASSERT_EQ(drop_while_t<to_bool, naturals>,
		  naturals);
}
#endif
