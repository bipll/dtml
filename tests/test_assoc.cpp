#include <dtml/assoc.h>

#include "fixture.h"
#include "show.h"

TEST(assoc) {
	using namespace dtml;

	using m = unordered_map_t<
			  one_value       , value_box<'a'>,
			  value_box<2>    , value_box<'b'>,
			  untyped_value<2>, value_box<'d'>,
			  size_value<3>   , value_box<'c'>>;

	using mz = unordered_map_t<
			  one_value       , value_box<'a'>,
			  untyped_value<2>, value_box<'b'>,
			  value_box<2>    , value_box<'d'>,
			  size_value<3>   , value_box<'c'>,
			  value_box<'z'>>;

	// -----------------------------------------	

	ASSERT_EQ(assoc_t<size_value<2>, m>,
		  q<q<value_box<2>, value_box<'b'>>,
		    q<one_value   , value_box<'a'>>>);

	ASSERT_EQ(assoc_t<size_value<4>, m>,
		  nil);

	ASSERT_EQ(assoc_t<size_value<2>, mz>,
		  q<q<untyped_value<2>, value_box<'b'>>,
		    q<one_value       , value_box<'a'>>,
		    q<value_box<'z'>>>);

	ASSERT_EQ(assoc_t<size_value<4>, mz>,
		  q<q<value_box<'z'>>>);
}

TEST(lookup) {
	using namespace dtml;

	using m = unordered_map_t<
			  one_value       , value_box<'a'>,
			  value_box<2>    , value_box<'b'>,
			  untyped_value<2>, value_box<'d'>,
			  size_value<3>   , value_box<'c'>>;

	using mz = unordered_map_t<
			  one_value       , value_box<'a'>,
			  untyped_value<2>, value_box<'b'>,
			  value_box<2>    , value_box<'d'>,
			  size_value<3>   , value_box<'c'>,
			  value_box<'z'>>;

	// -----------------------------------------	

	ASSERT_EQ(lookup_t<size_value<2>, m>,
		  value_box<'b'>);

	ASSERT_EQ(lookup_t<size_value<4>, m>,
		  null);

	ASSERT_EQ(lookup_t<size_value<2>, mz>,
		  value_box<'b'>);

	ASSERT_EQ(lookup_t<size_value<4>, mz>,
		  value_box<'z'>);
}

TEST(contains) {
	using namespace dtml;

	using m = unordered_map_t<
			  one_value       , value_box<'a'>,
			  value_box<2>    , value_box<'b'>,
			  untyped_value<2>, value_box<'d'>,
			  size_value<3>   , value_box<'c'>>;

	using mz = unordered_map_t<
			  one_value       , value_box<'a'>,
			  untyped_value<2>, value_box<'b'>,
			  value_box<2>    , value_box<'d'>,
			  size_value<3>   , value_box<'c'>,
			  value_box<'z'>>;

	// -----------------------------------------	

	ASSERT_TRUE(contains_t<size_value<2>, m>);

	ASSERT_FALSE(contains_t<size_value<4>, m>);

	ASSERT_TRUE(contains_t<size_value<2>, mz>);

	ASSERT_FALSE(contains_t<size_value<4>, mz>);
}

TEST(map_equal_to) {
	using namespace dtml;

	using m = unordered_map_t<
			  one_value       , value_box<'a'>,
			  value_box<2>    , value_box<'b'>,
			  untyped_value<2>, value_box<'d'>,
			  size_value<3>   , value_box<'c'>>;

	using m1 = unordered_map_t<
			  one_size        , value_box<'a'>,
			  untyped_value<2>, value_box<'b'>,
			  value_box<2>    , value_box<'d'>,
			  size_value<3>   , value_box<'c'>>;

	using m2 = unordered_map_t<
			  one_size        , value_box<'a'>,
			  value_box<2>    , value_box<'d'>,
			  size_value<3>   , value_box<'c'>>;

	using m3 = unordered_map_t<
			  one_size        , value_box<'a'>,
			  value_box<42>   , value_box<'b'>,
			  size_value<3>   , value_box<'c'>>;

	using mz = unordered_map_t<
			  one_value       , value_box<'a'>,
			  untyped_value<2>, value_box<'b'>,
			  value_box<2>    , value_box<'d'>,
			  size_value<3>   , value_box<'c'>,
			  value_box<'z'>>;

	using m1z = unordered_map_t<
			  one_value       , value_box<'a'>,
			  untyped_value<2>, value_box<'b'>,
			  value_box<2>    , value_box<'d'>,
			  size_value<3>   , value_box<'c'>,
			  untyped_value<'z'>>;

	using m2z = unordered_map_t<
			  one_size        , value_box<'a'>,
			  value_box<2>    , value_box<'d'>,
			  size_value<3>   , value_box<'c'>,
			  untyped_value<'z'>>;

	using m3z = unordered_map_t<
			  one_size        , value_box<'a'>,
			  value_box<42>   , value_box<'b'>,
			  size_value<3>   , value_box<'c'>,
			  untyped_value<'z'>>;


	// -----------------------------------------	

	ASSERT_TRUE(map_equal_to_v<m, m>);
	ASSERT_TRUE(map_equal_to_v<m, m1>);
	ASSERT_TRUE(map_equal_to_v<m1, m>);
	ASSERT_TRUE(map_equal_to_v<m1, m1>);

	ASSERT_TRUE(map_equal_to_v<mz, mz>);
	ASSERT_TRUE(map_equal_to_v<mz, m1z>);
	ASSERT_TRUE(map_equal_to_v<m1z, mz>);
	ASSERT_TRUE(map_equal_to_v<m1z, m1z>);

	ASSERT_FALSE(map_equal_to_v<m, mz>);
	ASSERT_FALSE(map_equal_to_v<m, m1z>);
	ASSERT_FALSE(map_equal_to_v<m1, mz>);
	ASSERT_FALSE(map_equal_to_v<m1, m1z>);

	ASSERT_FALSE(map_equal_to_v<mz, m>);
	ASSERT_FALSE(map_equal_to_v<mz, m1>);
	ASSERT_FALSE(map_equal_to_v<m1z, m>);
	ASSERT_FALSE(map_equal_to_v<m1z, m1>);

	ASSERT_FALSE(map_equal_to_v<m, m2>);
	ASSERT_FALSE(map_equal_to_v<m, m3>);
	ASSERT_FALSE(map_equal_to_v<m, m2z>);
	ASSERT_FALSE(map_equal_to_v<m, m3z>);

	ASSERT_FALSE(map_equal_to_v<m1, m2>);
	ASSERT_FALSE(map_equal_to_v<m1, m3>);
	ASSERT_FALSE(map_equal_to_v<m1, m2z>);
	ASSERT_FALSE(map_equal_to_v<m1, m3z>);

	ASSERT_FALSE(map_equal_to_v<mz, m2>);
	ASSERT_FALSE(map_equal_to_v<mz, m3>);
	ASSERT_FALSE(map_equal_to_v<mz, m2z>);
	ASSERT_FALSE(map_equal_to_v<mz, m3z>);

	ASSERT_FALSE(map_equal_to_v<m1z, m2>);
	ASSERT_FALSE(map_equal_to_v<m1z, m3>);
	ASSERT_FALSE(map_equal_to_v<m1z, m2z>);
	ASSERT_FALSE(map_equal_to_v<m1z, m3z>);

	ASSERT_FALSE(map_equal_to_v<m2, m>);
	ASSERT_FALSE(map_equal_to_v<m3, m>);
	ASSERT_FALSE(map_equal_to_v<m2z, m>);
	ASSERT_FALSE(map_equal_to_v<m3z, m>);

	ASSERT_FALSE(map_equal_to_v<m2, m1>);
	ASSERT_FALSE(map_equal_to_v<m3, m1>);
	ASSERT_FALSE(map_equal_to_v<m2z, m1>);
	ASSERT_FALSE(map_equal_to_v<m3z, m1>);

	ASSERT_FALSE(map_equal_to_v<m2, mz>);
	ASSERT_FALSE(map_equal_to_v<m3, mz>);
	ASSERT_FALSE(map_equal_to_v<m2z, mz>);
	ASSERT_FALSE(map_equal_to_v<m3z, mz>);

	ASSERT_FALSE(map_equal_to_v<m2, m1z>);
	ASSERT_FALSE(map_equal_to_v<m3, m1z>);
	ASSERT_FALSE(map_equal_to_v<m2z, m1z>);
	ASSERT_FALSE(map_equal_to_v<m3z, m1z>);
}

TEST(map_union) {
	using namespace dtml;

	using a = value_box<'a'>;
	using b = value_box<'b'>;
	using c = value_box<'c'>;
	using d = value_box<'d'>;
	using e = value_box<'e'>;
	using f = value_box<'f'>;
	using y = value_box<'y'>;
	using z = value_box<'z'>;

	using m = unordered_map_t<
		one_value       , a,
		value_box<2>    , b,
		untyped_value<2>, d,
		size_value<3>   , c>;

	using n = unordered_map_t<
		one_size, e,
		untyped_value<3>, f>;

	using mz = unordered_map_t<
			  one_value       , a,
			  untyped_value<2>, b,
			  value_box<2>    , d,
			  size_value<3>   , c,
			  z>;

	using nz = unordered_map_t<
		one_size,         e,
		untyped_value<3>, f,
		y>;

#define ASSERT_MAP_EQ(...) ASSERT_TRUE(map_equal_to_v<__VA_ARGS__>);

	ASSERT_MAP_EQ(map_union_t<m, m>,
		      m);

	ASSERT_MAP_EQ(map_union_t<m, n>,
		      m);

	ASSERT_MAP_EQ(map_union_t<n, m>,
		      unordered_map_t<one_size, e,
		                      value_box<2>, b,
		                      untyped_value<3>, f>);

	ASSERT_MAP_EQ(map_union_t<m, mz>,
		      unordered_map_t<one_value       , a,
		                      value_box<2>    , b,
		                      size_value<3>   , c,
		                      z>);

	ASSERT_MAP_EQ(map_union_t<mz, m>,
		      mz);

	ASSERT_MAP_EQ(map_union_t<m, nz>,
		      unordered_map_t<one_value       , a,
		                      value_box<2>    , b,
		                      size_value<3>   , c,
		                      y>);

	ASSERT_MAP_EQ(map_union_t<nz, m>,
		      unordered_map_t<one_size, e,
		                      value_box<2>, b,
		                      untyped_value<3>, f,
		                      y>);
}
