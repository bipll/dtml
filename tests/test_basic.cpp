#include <iostream>
#include <tuple>

#include <dtml/basic.h>

#include "fixture.h"

template<class... Ts> using q1 = dtml::invoke<dtml::cons, dtml::size_value<1>, Ts...>;
template<class... Ts> using q2 = dtml::invoke<dtml::cons, dtml::size_value<2>, Ts...>;
template<class... Ts> using q3 = dtml::invoke<dtml::cons, dtml::size_value<3>, Ts...>;


namespace dtml::strict  {

template<class T> struct infinitely_repeat {};

template<class T> struct short_circuiting<infinitely_repeat<T>, car>: true_value {};
template<class T> struct unbox<car<infinitely_repeat<T>>>: box<T> {};

template<class T> struct short_circuiting<infinitely_repeat<T>, cdr>: true_value {};
template<class T> struct unbox<cdr<infinitely_repeat<T>>>: call<sequence, infinitely_repeat<T>> {};

template<class T> struct short_circuiting<infinitely_repeat<T>, finite>: true_value {};
template<class T> struct unbox<finite<infinitely_repeat<T>>>: box<false_value> {};

template<class T> struct short_circuiting<infinitely_repeat<T>, infinite>: true_value {};
template<class T> struct unbox<infinite<infinitely_repeat<T>>>: box<true_value> {};

}

void test() {
	using namespace dtml;

	using one_int = invoke_t<quote, int>;
	using quote1 = DTML_FUNC_BOX(q1);
	using quote2 = DTML_FUNC_BOX(q2);
	using quote3 = DTML_FUNC_BOX(q3);

	// -----------------------------------------

	ASSERT_EQ(Finite<one_int>,
	          true);

	ASSERT_EQ(one_int,
	          q<int>);

	ASSERT_EQ(car_t<one_int>,
	          int);

	ASSERT_EQ(cdr_t<one_int>,
	          q<>);

	ASSERT_EQ(car_t<box<one_int>>,
	          int);

	ASSERT_EQ(cdr_t<box<one_int>>,
	          q<>);

	ASSERT_EQ(car_t<box<box<box<one_int>>>>,
	          int);

	ASSERT_EQ(cdr_t<box<box<box<one_int>>>>,
	          q<>);


	// -----------------------------------------

	ASSERT_EQ(value_v<strict::to_bool<size_value<12>>>,
	          true);

	ASSERT_EQ(value_v<strict::to_bool<zero_value>>,
	          false);


	// -----------------------------------------

	ASSERT_EQ(cond_t<zero_value, null, false_value, box<size_value<42>>, box<one_value>, value_box<string{"Hi!"}>, null, size_value<314>>,
	          value_box<string{"Hi!"}>);


	// -----------------------------------------

	ASSERT_EQ(empty_v<nil>,
	          true);

	ASSERT_EQ(empty_v<box<nil>>,
	          true);

	ASSERT_EQ(empty_v<box<box<nil>>>,
	          true);

	ASSERT_EQ(empty_v<zero_value>,
	          false);

	ASSERT_EQ(empty_v<false_value>,
	          false);

	ASSERT_EQ(empty_v<q<>>,
	          true);

	ASSERT_EQ(empty_v<q<false_value>>,
	          false);


	// -----------------------------------------

	ASSERT_EQ(logical_not_v<invoke<empty, nil>>,
	          false);

	ASSERT_EQ(logical_not_v<invoke<empty, box<nil>>>,
	          false);

	ASSERT_EQ(logical_not_v<invoke<empty, box<box<nil>>>>,
	          false);

	ASSERT_EQ(logical_not_v<invoke<empty, zero_value>>,
	          true);

	ASSERT_EQ(logical_not_v<invoke<empty, false_value>>,
	          true);

	ASSERT_EQ(logical_not_v<invoke<empty, q<>>>,
	          false);

	ASSERT_EQ(logical_not_v<invoke<empty, q<false_value>>>,
	          true);


	// -----------------------------------------

	ASSERT_EQ(logical_and_v<true_value, one_value, zero_value, null, size_value<42>>,
	          0);

	ASSERT_EQ(logical_and_v<true_value, one_value, size_value<42>>,
	          42);


	// -----------------------------------------

	ASSERT_EQ(logical_or_v<true_value, one_value, zero_value, null, size_value<42>>,
	          true);

	ASSERT_EQ(logical_or_v<true_value, one_value, size_value<42>>,
	          true);

	ASSERT_EQ(logical_or_v<zero_value, false_value, value_box<0>, size_value<0>>,
	          0);


	// -----------------------------------------

	ASSERT_EQ(finite_v<one_int>,
	          true);

	ASSERT_EQ(finite_v<nil>,
	          true);

	ASSERT_EQ(infinite_v<one_int>,
	          false);

	ASSERT_EQ(infinite_v<nil>,
	          false);


	// -----------------------------------------

	ASSERT_EQ(finite_v<box<box<box<one_int>>>>,
	          true);

	ASSERT_EQ(finite_v<box<box<box<nil>>>>,
	          true);

	ASSERT_EQ(infinite_v<box<box<box<one_int>>>>,
	          false);

	ASSERT_EQ(infinite_v<box<box<box<nil>>>>,
	          false);


	// -----------------------------------------

	ASSERT_EQ(force_list_t<one_int>,
	          q<int>);

	ASSERT_EQ(force_list_t<nil>,
	          q<>);

	ASSERT_EQ(force_list_t<box<box<box<one_int>>>>,
	          q<int>);


	// -----------------------------------------

	ASSERT_EQ(Finite<one_int>,
	          true);

	ASSERT_EQ(!empty_v<one_int>,
	          true);


	// -----------------------------------------

	ASSERT_EQ(one_int,
	          q<int>);

	ASSERT_EQ(force_list_t<one_int>,
	          q<int>);

	ASSERT_EQ(force_list_t<box<box<box<box<one_int>>>>>,
	          q<int>);

	ASSERT_EQ(force_list_t<thunk<int, q>>,
	          q<int>);


	// -----------------------------------------

	ASSERT_EQ(concat_t<one_int>,
	          q<int>);

	ASSERT_EQ(concat_t<one_int, one_int>,
	          q<int, int>);

	ASSERT_EQ(concat_t<one_int, q<zero_value, one_value>, char, invoke<cons, true_value, box<box<box<q<false_value, double>>>>>>,
	          q<int, value_box<untyped_constant<0>{}>, value_box<untyped_constant<1>{}>, char, value_box<true>, value_box<false>, double>);


	// -----------------------------------------

	ASSERT_EQ(cdr_t<one_int>,
	          q<>);

	ASSERT_EQ(cdr_t<one_int, one_int>,
	          q<int>);

	ASSERT_EQ(cdr_t<one_int, q<zero_value, one_value>, char, invoke<cons, true_value, box<box<box<q<false_value, double>>>>>>,
	          q<value_box<untyped_constant<0>{}>, value_box<untyped_constant<1>{}>, char, value_box<true>, value_box<false>, double>);


	// -----------------------------------------

	ASSERT_EQ(plus_t<>,
	          value_box<untyped_constant<0>{}>);

	ASSERT_EQ(plus_t<size_value<42>>,
	          size_value<42>);

	ASSERT_EQ(plus_t<size_value<42>, size_value<272>>,
	          size_value<314>);

	ASSERT_EQ(plus_t<untyped_value<-1>, size_value<1>>,
	          size_value<0>);

	ASSERT_EQ(plus_t<size_value<1>, untyped_value<-1>>,
	          size_value<0>);

	ASSERT_EQ(plus_t<zero_value, one_value, size_value<2>, size_value<3>, size_value<36>>,
	          size_value<42>);


	// -----------------------------------------

	ASSERT_EQ(minus_t<>,
	          value_box<untyped_constant<0>{}>);

	ASSERT_EQ(minus_t<value_box<42>>,
	          value_box<-42>);

	ASSERT_EQ(minus_t<value_box<-1>, value_box<1>>,
	          value_box<-2>);

	ASSERT_EQ(minus_t<size_value<1>, untyped_value<-1>>,
	          size_value<2>);

	ASSERT_EQ(minus_t<value_box<1>, value_box<2>>,
	          value_box<-1>);

	ASSERT_EQ(minus_t<value_box<1>, value_box<2>, value_box<3>>,
	          value_box<-4>);

	ASSERT_EQ(minus_t<value_box<1>, value_box<2>, value_box<3>, value_box<36>>,
	          value_box<-40>);

	ASSERT_EQ(minus_t<value_box<1>, value_box<2>, value_box<3>, value_box<36>, value_box<5>>,
	          value_box<-45>);


	// -----------------------------------------

	ASSERT_EQ(multiplies_t<>,
	          value_box<untyped_constant<1>{}>);

	ASSERT_EQ(multiplies_t<value_box<42>>,
	          value_box<42>);

	ASSERT_EQ(multiplies_t<value_box<-1>, value_box<1>>,
	          value_box<-1>);

	ASSERT_EQ(multiplies_t<value_box<1>, value_box<2>>,
	          value_box<2>);

	ASSERT_EQ(multiplies_t<value_box<1>, value_box<2>, value_box<3>>,
	          value_box<6>);

	ASSERT_EQ(multiplies_t<value_box<1>, value_box<2>, value_box<3>, value_box<36>>,
	          value_box<216>);

	ASSERT_EQ(multiplies_t<value_box<1>, value_box<2>, value_box<3>, value_box<36>, value_box<5>>,
	          value_box<1080>);


	// -----------------------------------------

	ASSERT_EQ(divides_t<>,
	          value_box<untyped_constant<1>{}>);

	ASSERT_EQ(divides_t<size_value<42>, size_value<2>>,
	          size_value<21>);

	ASSERT_EQ(divides_t<size_value<42>, size_value<2>, size_value<3>>,
	          size_value<7>);

	ASSERT_EQ(divides_t<size_value<42>, size_value<2>, size_value<3>, size_value<7>>,
	          size_value<1>);


	// -----------------------------------------

	ASSERT_EQ(modulus_t<>,
	          value_box<untyped_constant<0>{}>);

	ASSERT_EQ(modulus_t<value_box<2>>,
	          value_box<0>);

	ASSERT_EQ(modulus_t<value_box<150>, value_box<20>>,
	          value_box<10>);

	ASSERT_EQ(modulus_t<value_box<150>, value_box<20>, value_box<7>>,
	          value_box<3>);

	ASSERT_EQ(modulus_t<value_box<150>, value_box<20>, value_box<7>, value_box<2>>,
	          value_box<1>);


	// -----------------------------------------

	ASSERT_EQ(negate_t<>,
	          value_box<untyped_constant<0>{}>);

	ASSERT_EQ(negate_t<value_box<42>>,
	          value_box<-42>);

	ASSERT_EQ(negate_t<value_box<-1>, value_box<1>>,
	          value_box<-2>);

	ASSERT_EQ(negate_t<size_value<1>, untyped_value<-1>>,
	          size_value<2>);

	ASSERT_EQ(negate_t<value_box<1>, value_box<2>>,
	          value_box<-1>);

	ASSERT_EQ(negate_t<value_box<1>, value_box<2>, value_box<3>>,
	          value_box<-4>);

	ASSERT_EQ(negate_t<value_box<1>, value_box<2>, value_box<3>, value_box<36>>,
	          value_box<-40>);

	ASSERT_EQ(negate_t<value_box<1>, value_box<2>, value_box<3>, value_box<36>, value_box<5>>,
	          value_box<-45>);


	// -----------------------------------------

	ASSERT_EQ(bit_and_t<>,
	          untyped_value<-1>);

	ASSERT_EQ(bit_and_t<one_value>,
	          one_value);

	ASSERT_EQ(bit_and_t<value_box<254>, value_box<127>>,
	          value_box<126>);

	ASSERT_EQ(bit_and_t<value_box<254>, value_box<127>, value_box<9>>,
	          value_box<8>);


	// -----------------------------------------

	ASSERT_EQ(bit_or_t<>,
	          value_box<untyped_constant<0>{}>);

	ASSERT_EQ(bit_or_t<one_value>,
	          one_value);

	ASSERT_EQ(bit_or_t<one_value, value_box<4>>,
	          value_box<5>);

	ASSERT_EQ(bit_or_t<one_value, value_box<4>, value_box<4>>,
	          value_box<5>);

	ASSERT_EQ(bit_or_t<one_value, value_box<4>, value_box<4>, value_box<16>>,
	          value_box<21>);


	// -----------------------------------------

	ASSERT_EQ(bit_xor_t<>,
	          value_box<untyped_constant<0>{}>);

	ASSERT_EQ(bit_xor_t<one_value>,
	          one_value);

	ASSERT_EQ(bit_xor_t<one_value, value_box<4>>,
	          value_box<5>);

	ASSERT_EQ(bit_xor_t<one_value, value_box<4>, value_box<4>>,
	          value_box<1>);

	ASSERT_EQ(bit_xor_t<one_value, value_box<4>, value_box<4>, value_box<16>>,
	          value_box<17>);


	// -----------------------------------------

	ASSERT_EQ(bit_not_t<>,
		  value_box<untyped_constant<-1>{}>);

	ASSERT_EQ(bit_not_t<one_value>,
	          untyped_value<-2>);

	ASSERT_EQ(bit_not_t<one_value, value_box<4>>,
	          value_box<5>);

	ASSERT_EQ(bit_not_t<one_value, value_box<4>, value_box<4>>,
	          value_box<1>);

	ASSERT_EQ(bit_not_t<one_value, value_box<4>, value_box<4>, value_box<16>>,
	          value_box<17>);


	// -----------------------------------------

	ASSERT_EQ(equal_to_t<>,
	          value_box<true>);

	ASSERT_EQ(equal_to_t<one_value>,
	          value_box<true>);

	ASSERT_EQ(equal_to_t<one_value, one_value>,
	          value_box<true>);

	ASSERT_EQ(equal_to_t<one_value, one_value, one_value>,
	          value_box<true>);

	ASSERT_EQ(equal_to_t<one_value, one_value, value_box<2>>,
	          value_box<false>);

	ASSERT_EQ(equal_to_t<one_value, one_value, value_box<2>, null>,
	          value_box<false>);

	ASSERT_EQ(equal_to_t<one_value, one_value, one_value, zero_value>,
	          value_box<false>);

	ASSERT_EQ(equal_to_t<one_value, one_value, one_value, zero_value, one_value>,
	          value_box<false>);

	ASSERT_EQ(equal_to_t<one_value, one_value, one_value, zero_value, one_value, one_value>,
	          value_box<false>);

	ASSERT_EQ(equal_to_t<zero_value, zero_value, strict::infinitely_repeat<one_value>>,
	          value_box<false>);

	// ASSERT_EQ(equal_to_t<strict::infinitely_repeat<one_value>>, ???);	// never compiles

	// -----------------------------------------

	ASSERT_EQ(left_accumulate_t<quote, zero_value>,
	          value_box<untyped_constant<0>{}>);

	ASSERT_EQ(left_accumulate_t<quote, one_value>,
	          value_box<untyped_constant<1>{}>);

	ASSERT_EQ(left_accumulate_t<quote, value_box<5>>,
	          value_box<5>);

	ASSERT_EQ(left_accumulate_t<quote, value_box<2>, value_box<4>, value_box<6>, value_box<8>>,
	          q<q<q<value_box<2>, value_box<4>>, value_box<6>>, value_box<8>>);

	ASSERT_EQ(left_accumulate_t<logical_and, true_value, true_value, false_value, null>,
	          value_box<false>);


	// -----------------------------------------

	ASSERT_EQ(right_accumulate_t<quote, zero_value>,
	          value_box<untyped_constant<0>{}>);

	ASSERT_EQ(right_accumulate_t<quote, one_value>,
	          value_box<untyped_constant<1>{}>);

	ASSERT_EQ(right_accumulate_t<quote, value_box<5>>,
	          value_box<5>);

	ASSERT_EQ(right_accumulate_t<quote, value_box<2>, value_box<4>, value_box<6>, value_box<8>>,
		  q<value_box<2>, q<value_box<4>, q<value_box<6>, value_box<8>>>>);

	ASSERT_EQ(right_accumulate_t<logical_and, true_value, strict::infinitely_repeat<false_value>>,
	          value_box<false>);

	ASSERT_EQ(right_accumulate_t<logical_and, true_value, true_value, false_value, null>,
	          value_box<false>);


	// -----------------------------------------

	ASSERT_EQ(left_accumulate2_t<quote, zero_value>,
	          value_box<untyped_constant<0>{}>);

	ASSERT_EQ(left_accumulate2_t<quote, one_value>,
	          value_box<untyped_constant<1>{}>);

	ASSERT_EQ(left_accumulate2_t<quote, zero_value, one_value>,
	          value_box<untyped_constant<0>{}>);

	ASSERT_EQ(left_accumulate2_t<quote, value_box<5>>,
	          value_box<5>);

	ASSERT_EQ(left_accumulate2_t<quote, zero_value, value_box<5>>,
	          value_box<untyped_constant<0>{}>);

	ASSERT_EQ(left_accumulate2_t<quote, value_box<5>, zero_value>,
	          value_box<5>);

	ASSERT_EQ(left_accumulate2_t<quote, value_box<2>, value_box<4>, value_box<6>, value_box<8>>,
	          q<q<value_box<2>, value_box<4>, value_box<6>>, value_box<6>, value_box<8>>);

	ASSERT_EQ(left_accumulate2_t<quote, value_box<2>, value_box<4>, value_box<6>, value_box<8>, value_box<10>, value_box<12>>,
	          q<q<q<q<value_box<2>, value_box<4>, value_box<6>>, value_box<6>, value_box<8>>, value_box<8>, value_box<10>>, value_box<10>, value_box<12>>);


	// -----------------------------------------

	ASSERT_EQ(right_accumulate2_t<quote, zero_value>,
	          value_box<untyped_constant<0>{}>);

	ASSERT_EQ(right_accumulate2_t<quote, one_value>,
	          value_box<untyped_constant<1>{}>);

	ASSERT_EQ(right_accumulate2_t<quote, zero_value, one_value>,
	          value_box<untyped_constant<1>{}>);

	ASSERT_EQ(right_accumulate2_t<quote, value_box<5>>,
	          value_box<5>);

	ASSERT_EQ(right_accumulate2_t<quote, zero_value, value_box<5>>,
	          value_box<5>);

	ASSERT_EQ(right_accumulate2_t<quote, value_box<5>, zero_value>,
	          value_box<untyped_constant<0>{}>);

	ASSERT_EQ(right_accumulate2_t<quote, value_box<2>, value_box<4>, value_box<6>, value_box<8>>,
	          q<value_box<2>, value_box<4>, q<value_box<4>, value_box<6>, value_box<8>>>);

	ASSERT_EQ(right_accumulate2_t<quote, value_box<2>, value_box<4>, value_box<6>, value_box<8>, value_box<10>, value_box<12>>,
	          q<value_box<2>, value_box<4>, q<value_box<4>, value_box<6>, q<value_box<6>, value_box<8>, q<value_box<8>, value_box<10>, value_box<12>>>>>);

	ASSERT_EQ(right_accumulate2_t<logical_and, true_value, true_value, strict::infinitely_repeat<false_value>>,
	          value_box<false>);


	// -----------------------------------------

	ASSERT_EQ(less_t<>,
	          value_box<true>);

	ASSERT_EQ(less_t<zero_value>,
	          value_box<true>);

	ASSERT_EQ(less_t<zero_value, one_value>,
	          value_box<true>);

	ASSERT_EQ(less_t<zero_value, one_value, value_box<2>, size_value<3>>,
	          value_box<true>);

	ASSERT_EQ(less_t<zero_value, one_value, value_box<2>, value_box<-1>>,
	          value_box<false>);

	ASSERT_EQ(less_t<value_box<2>, value_box<-1>>,
	          value_box<false>);

	ASSERT_EQ(less_t<value_box<2>, value_box<-1>, size_value<3>>,
	          value_box<false>);

	ASSERT_EQ(less_t<zero_value, one_value, value_box<2>, value_box<-1>, size_value<3>>,
	          value_box<false>);

	ASSERT_EQ(less_t<zero_value, one_value, value_box<2>, size_value<3>, value_box<-1>>,
	          value_box<false>);

	ASSERT_EQ(less_t<zero_value, one_value, value_box<2>, size_value<3>, value_box<4>>,
	          value_box<true>);

	ASSERT_EQ(less_t<zero_value, one_value, value_box<2>, value_box<2>, size_value<3>, value_box<4>>,
	          value_box<false>);

	ASSERT_EQ(less_t<zero_value, one_value, value_box<2>, size_value<3>, untyped_value<-1>>,
	          value_box<true>);

	ASSERT_EQ(less_t<zero_value, one_value, value_box<2>, size_value<3>, value_box<-1>>,
	          value_box<false>);

	ASSERT_EQ(less_t<zero_value, one_value, value_box<2>, value_box<-1>, null>,
	          value_box<false>);

	ASSERT_EQ(less_t<value_box<2>, value_box<-1>, null>,
	          value_box<false>);

	ASSERT_EQ(less_t<value_box<2>, value_box<-1>, size_value<3>, null>,
	          value_box<false>);

	ASSERT_EQ(less_t<zero_value, one_value, value_box<2>, value_box<-1>, size_value<3>, null>,
	          value_box<false>);

	ASSERT_EQ(less_t<zero_value, one_value, value_box<2>, size_value<3>, value_box<-1>, null>,
	          value_box<false>);

	ASSERT_EQ(less_t<zero_value, one_value, value_box<2>>,
	          value_box<true>);

	ASSERT_EQ(less_t<zero_value, one_value, value_box<2>, strict::infinitely_repeat<one_value>>,
	          value_box<false>);

	ASSERT_EQ(less_t<strict::infinitely_repeat<one_value>>,
	          value_box<false>);


	// -----------------------------------------

	ASSERT_EQ(less_equal_t<>,
	          value_box<true>);

	ASSERT_EQ(less_equal_t<zero_value>,
	          value_box<true>);

	ASSERT_EQ(less_equal_t<zero_value, one_value>,
	          value_box<true>);

	ASSERT_EQ(less_equal_t<zero_value, one_value, value_box<2>, size_value<3>>,
	          value_box<true>);

	ASSERT_EQ(less_equal_t<zero_value, one_value, one_value, value_box<2>, size_value<3>>,
	          value_box<true>);

	ASSERT_EQ(less_equal_t<zero_value, one_value, one_value, value_box<2>, size_value<3>, size_value<3>>,
	          value_box<true>);

	ASSERT_EQ(less_equal_t<zero_value, one_value, value_box<2>, value_box<-1>>,
	          value_box<false>);

	ASSERT_EQ(less_equal_t<value_box<2>, value_box<-1>>,
	          value_box<false>);

	ASSERT_EQ(less_equal_t<value_box<2>, value_box<-1>, size_value<3>>,
	          value_box<false>);

	ASSERT_EQ(less_equal_t<zero_value, one_value, value_box<2>, value_box<-1>, size_value<3>>,
	          value_box<false>);

	ASSERT_EQ(less_equal_t<zero_value, one_value, value_box<2>, size_value<3>, value_box<-1>>,
	          value_box<false>);

	ASSERT_EQ(less_equal_t<zero_value, one_value, value_box<2>, size_value<3>, untyped_value<-1>>,
	          value_box<true>);

	ASSERT_EQ(less_equal_t<zero_value, one_value, value_box<2>, value_box<-1>, null>,
	          value_box<false>);

	ASSERT_EQ(less_equal_t<value_box<2>, value_box<-1>, null>,
	          value_box<false>);

	ASSERT_EQ(less_equal_t<value_box<2>, value_box<-1>, size_value<3>, null>,
	          value_box<false>);

	ASSERT_EQ(less_equal_t<zero_value, one_value, value_box<2>, value_box<-1>, size_value<3>, null>,
	          value_box<false>);

	ASSERT_EQ(less_equal_t<zero_value, one_value, value_box<2>, size_value<3>, value_box<-1>, null>,
	          value_box<false>);

	ASSERT_EQ(less_equal_t<zero_value, one_value, value_box<2>>,
	          value_box<true>);

	ASSERT_EQ(less_equal_t<zero_value, one_value, value_box<2>, strict::infinitely_repeat<one_value>>,
	          value_box<false>);

	// ASSERT_EQ(less_equal_t<strict::infinitely_repeat<one_value>>, ???);	// never compiles

	// -----------------------------------------

	ASSERT_EQ(greater_t<>,
	          value_box<true>);

	ASSERT_EQ(greater_t<zero_value>,
	          value_box<true>);

	ASSERT_EQ(greater_t<one_value, zero_value>,
	          value_box<true>);

	ASSERT_EQ(greater_t<size_value<3>, value_box<2>, one_value, zero_value>,
	          value_box<true>);

	ASSERT_EQ(greater_t<value_box<-1>, value_box<2>, one_value, zero_value>,
	          value_box<false>);

	ASSERT_EQ(greater_t<value_box<2>, one_value, value_box<-1>, zero_value>,
	          value_box<false>);

	ASSERT_EQ(greater_t<value_box<-1>, value_box<2>>,
	          value_box<false>);

	ASSERT_EQ(greater_t<size_value<3>, value_box<-1>, value_box<2>>,
	          value_box<false>);

	ASSERT_EQ(greater_t<size_value<3>, value_box<-1>, value_box<2>, one_value, zero_value>,
	          value_box<false>);

	ASSERT_EQ(greater_t<value_box<-1>, size_value<3>, value_box<2>, one_value, zero_value>,
	          value_box<false>);

	ASSERT_EQ(greater_t<untyped_value<-1>, size_value<3>, value_box<2>, one_value, zero_value>,
	          value_box<true>);

	ASSERT_EQ(greater_t<value_box<-1>, value_box<2>, one_value, zero_value, null>,
	          value_box<false>);

	ASSERT_EQ(greater_t<value_box<-1>, value_box<2>, null>,
	          value_box<false>);

	ASSERT_EQ(greater_t<size_value<3>, value_box<-1>, value_box<2>, null>,
	          value_box<false>);

	ASSERT_EQ(greater_t<size_value<3>, value_box<-1>, value_box<2>, one_value, zero_value, null>,
	          value_box<false>);

	ASSERT_EQ(greater_t<value_box<-1>, size_value<3>, value_box<2>, one_value, zero_value, null>,
	          value_box<false>);

	ASSERT_EQ(greater_t<value_box<3>, value_box<2>, strict::infinitely_repeat<one_value>>,
	          value_box<false>);

	ASSERT_EQ(greater_t<strict::infinitely_repeat<one_value>>,
	          value_box<false>);


	// -----------------------------------------

	ASSERT_EQ(greater_equal_t<>,
	          value_box<true>);

	ASSERT_EQ(greater_equal_t<zero_value>,
	          value_box<true>);

	ASSERT_EQ(greater_equal_t<one_value, zero_value>,
	          value_box<true>);

	ASSERT_EQ(greater_equal_t<size_value<3>, value_box<2>, one_value, zero_value>,
	          value_box<true>);

	ASSERT_EQ(greater_equal_t<size_value<3>, value_box<2>, one_value, one_value, zero_value>,
	          value_box<true>);

	ASSERT_EQ(greater_equal_t<size_value<3>, size_value<3>, value_box<2>, one_value, one_value, zero_value>,
	          value_box<true>);

	ASSERT_EQ(greater_equal_t<value_box<-1>, value_box<2>, one_value, zero_value>,
	          value_box<false>);

	ASSERT_EQ(greater_equal_t<value_box<-1>, value_box<2>>,
	          value_box<false>);

	ASSERT_EQ(greater_equal_t<size_value<3>, value_box<-1>, value_box<2>>,
	          value_box<false>);

	ASSERT_EQ(greater_equal_t<size_value<3>, value_box<-1>, value_box<2>, one_value, zero_value>,
	          value_box<false>);

	ASSERT_EQ(greater_equal_t<value_box<-1>, size_value<3>, value_box<2>, one_value, zero_value>,
	          value_box<false>);

	ASSERT_EQ(greater_equal_t<untyped_value<-1>, size_value<3>, value_box<2>, one_value, zero_value>,
	          value_box<true>);

	ASSERT_EQ(greater_equal_t<value_box<-1>, value_box<2>, one_value, zero_value, null>,
	          value_box<false>);

	ASSERT_EQ(greater_equal_t<value_box<-1>, value_box<2>, null>,
	          value_box<false>);

	ASSERT_EQ(greater_equal_t<size_value<3>, value_box<-1>, value_box<2>, null>,
	          value_box<false>);

	ASSERT_EQ(greater_equal_t<size_value<3>, value_box<-1>, value_box<2>, one_value, zero_value, null>,
	          value_box<false>);

	ASSERT_EQ(greater_equal_t<value_box<-1>, size_value<3>, value_box<2>, one_value, zero_value, null>,
	          value_box<false>);

	ASSERT_EQ(greater_equal_t<value_box<2>, one_value, zero_value>,
	          value_box<true>);

	ASSERT_EQ(greater_equal_t<value_box<2>, one_value, zero_value, strict::infinitely_repeat<one_value>>,
	          value_box<false>);


	// -----------------------------------------

	ASSERT_EQ(not_equal_to_t<>,
	          value_box<false>);

	ASSERT_EQ(not_equal_to_t<one_value>,
	          value_box<false>);

	ASSERT_EQ(not_equal_to_t<one_value, one_value>,
	          value_box<false>);

	ASSERT_EQ(not_equal_to_t<one_value, one_value, one_value>,
	          value_box<false>);

	ASSERT_EQ(not_equal_to_t<one_value, one_value, value_box<2>>,
	          value_box<true>);

	ASSERT_EQ(not_equal_to_t<one_value, one_value, value_box<2>, null>,
	          value_box<true>);

	ASSERT_EQ(not_equal_to_t<one_value, one_value, one_value, zero_value>,
	          value_box<true>);

	ASSERT_EQ(not_equal_to_t<one_value, one_value, one_value, zero_value, one_value>,
	          value_box<true>);

	ASSERT_EQ(not_equal_to_t<one_value, one_value, one_value, one_value, one_value>,
	          value_box<false>);

	ASSERT_EQ(not_equal_to_t<one_value, one_value, zero_value, one_value, null, one_value>,
	          value_box<true>);

	ASSERT_EQ(not_equal_to_t<one_value, one_value, one_value, zero_value, one_value, one_value>,
	          value_box<true>);

	ASSERT_EQ(not_equal_to_t<one_value, one_value, zero_value, strict::infinitely_repeat<one_value>>,
	          value_box<true>);


	// -----------------------------------------

	ASSERT_EQ(dec_t<zero_value>,
	          untyped_value<-1>);

	ASSERT_EQ(dec_t<one_value>,
	          zero_value);

	ASSERT_EQ(dec_t<value_box<0>>,
	          value_box<-1>);

	ASSERT_EQ(dec_t<size_value<3>>,
	          size_value<2>);

	ASSERT_EQ(dec_t<size_value<0>>,
	          size_value<(size_t)-1>);


	// -----------------------------------------

	ASSERT_EQ(inc_t<zero_value>,
	          one_value);

	ASSERT_EQ(inc_t<one_value>,
	          untyped_value<2>);

	ASSERT_EQ(inc_t<value_box<-1>>,
	          value_box<0>);

	ASSERT_EQ(inc_t<size_value<3>>,
	          size_value<4>);

	ASSERT_EQ(inc_t<size_value<0>>,
	          size_value<1>);


	// -----------------------------------------

	ASSERT_EQ(zero_t<zero_value>,
	          value_box<true>);

	ASSERT_EQ(zero_t<one_value>,
	          value_box<false>);

	ASSERT_EQ(zero_t<box<zero_value>>,
	          value_box<true>);

	ASSERT_EQ(zero_t<box<one_value>>,
	          value_box<false>);

	ASSERT_EQ(zero_t<box<box<zero_value>>>,
	          value_box<true>);

	ASSERT_EQ(zero_t<box<box<one_value>>>,
	          value_box<false>);

	ASSERT_EQ(zero_t<one_value, null>,
	          value_box<false>);

	ASSERT_EQ(zero_t<box<one_value>, null>,
	          value_box<false>);

	ASSERT_EQ(zero_t<box<box<one_value>>, null>,
	          value_box<false>);

	ASSERT_EQ(zero_t<one_value, strict::infinitely_repeat<zero_value>>,
	          value_box<false>);

	// ASSERT_EQ(zero_t<strict::infinitely_repeat<zero_value>>, ???);	// should be true but never compiles

	// -----------------------------------------

	ASSERT_EQ(positive_t<zero_value>,
	          value_box<false>);

	ASSERT_EQ(positive_t<value_box<-1>>,
	          value_box<false>);

	ASSERT_EQ(positive_t<one_value>,
	          value_box<true>);

	ASSERT_EQ(positive_t<zero_value, null>,
	          value_box<false>);

	ASSERT_EQ(positive_t<value_box<-1>, null>,
	          value_box<false>);

	ASSERT_EQ(positive_t<zero_value, value_box<-1>, one_value>,
	          value_box<false>);

	ASSERT_EQ(positive_t<one_value, value_box<2>, size_value<3>>,
	          value_box<true>);

	ASSERT_EQ(positive_t<one_value, zero_value, null, value_box<-1>>,
	          value_box<false>);

	ASSERT_EQ(positive_t<one_value, strict::infinitely_repeat<zero_value>>,
	          value_box<false>);

	ASSERT_EQ(positive_t<strict::infinitely_repeat<zero_value>>,
	          value_box<false>);


	// -----------------------------------------

	ASSERT_EQ(negative_t<zero_value, value_box<-1>, one_value>,
	          value_box<false>);

	ASSERT_EQ(negative_t<value_box<-1>, value_box<-2>, value_box<-3>>,
	          value_box<true>);

	ASSERT_EQ(negative_t<value_box<-1>, value_box<-2>, value_box<-3>, one_value>,
	          value_box<false>);

	ASSERT_EQ(negative_t<value_box<-1>, value_box<-2>, value_box<-3>, strict::infinitely_repeat<zero_value>>,
	          value_box<false>);

	ASSERT_EQ(negative_t<value_box<-1>, value_box<-2>, value_box<3>, strict::infinitely_repeat<value_box<-1>>>,
	          value_box<false>);

	ASSERT_EQ(negative_t<strict::infinitely_repeat<zero_value>>,
	          value_box<false>);


	// -----------------------------------------

	ASSERT_EQ(non_positive_t<zero_value, value_box<-1>, one_value>,
	          value_box<false>);

	ASSERT_EQ(non_positive_t<zero_value, value_box<-1>, zero_value>,
	          value_box<true>);

	ASSERT_EQ(non_positive_t<one_value, value_box<2>, size_value<3>>,
	          value_box<false>);

	ASSERT_EQ(non_positive_t<one_value, null, value_box<2>, size_value<3>>,
	          value_box<false>);

	ASSERT_EQ(non_positive_t<one_value, value_box<2>, untyped_value<-3>>,
	          value_box<false>);

	ASSERT_EQ(non_positive_t<one_value, strict::infinitely_repeat<zero_value>>,
	          value_box<false>);

	ASSERT_EQ(non_positive_t<zero_value, strict::infinitely_repeat<one_value>>,
	          value_box<false>);

	ASSERT_EQ(non_positive_t<strict::infinitely_repeat<one_value>>,
	          value_box<false>);


	// -----------------------------------------

	ASSERT_EQ(non_negative_t<zero_value>,
	          value_box<true>);

	ASSERT_EQ(non_negative_t<value_box<-1>>,
	          value_box<false>);

	ASSERT_EQ(non_negative_t<one_value>,
	          value_box<true>);

	ASSERT_EQ(non_negative_t<value_box<-1>, null>,
	          value_box<false>);

	ASSERT_EQ(non_negative_t<zero_value, value_box<-1>, one_value>,
	          value_box<false>);

	ASSERT_EQ(non_negative_t<zero_value, value_box<-1>, null, one_value>,
	          value_box<false>);

	ASSERT_EQ(non_negative_t<one_value, value_box<2>, size_value<3>>,
	          value_box<true>);

	ASSERT_EQ(non_negative_t<one_value, zero_value, value_box<-1>, null>,
	          value_box<false>);

	ASSERT_EQ(non_negative_t<value_box<-1>, strict::infinitely_repeat<one_value>>,
	          value_box<false>);

	ASSERT_EQ(non_negative_t<zero_value, strict::infinitely_repeat<value_box<-1>>>,
	          value_box<false>);

	ASSERT_EQ(non_negative_t<strict::infinitely_repeat<value_box<-1>>>,
	          value_box<false>);


	// -----------------------------------------

	ASSERT_EQ(length_t<>,
	          size_value<0>);

	ASSERT_EQ(length_t<one_value>,
	          size_value<1>);

	ASSERT_EQ(length_t<int>,
	          size_value<1>);

	ASSERT_EQ(length_t<int, double>,
	          size_value<2>);

	ASSERT_EQ(length_t<q<int, double>>,
	          size_value<2>);

	ASSERT_EQ(length_t<invoke<cdr, int>>,
	          size_value<0>);


	// -----------------------------------------

	ASSERT_EQ(cons_t<nil, nil>,
	          q<q<>>);

	ASSERT_EQ(cons_t<nil, int>,
	          q<q<>, int>);

	ASSERT_EQ(cons_t<nil, box<box<box<int>>>>,
	          q<q<>, int>);

	ASSERT_EQ(cons_t<nil, q<nil>>,
	          q<q<>, q<>>);

	ASSERT_EQ(cons_t<nil, box<box<box<q<nil>>>>>,
	          q<q<>, q<>>);

	ASSERT_EQ(cons_t<zero_value, strict::infinitely_repeat<one_value>>,
	          strict::sequence<q<value_box<untyped_constant<0>{}>>, strict::infinitely_repeat<value_box<untyped_constant<1>{}>>>);


	// -----------------------------------------

	ASSERT_EQ(append_t<nil, nil>,
	          q<q<>>);

	ASSERT_EQ(append_t<nil, int>,
	          q<int, q<>>);

	ASSERT_EQ(append_t<nil, box<box<box<int>>>>,
	          q<int, q<>>);

	ASSERT_EQ(append_t<nil, q<nil>>,
	          q<q<>, q<>>);

	ASSERT_EQ(append_t<nil, box<box<box<q<nil>>>>>,
	          q<q<>, q<>>);

	ASSERT_EQ(append_t<zero_value, strict::infinitely_repeat<one_value>>,
	          strict::sequence<strict::infinitely_repeat<value_box<untyped_constant<1>{}>>, q<value_box<untyped_constant<0>{}>>>);


	// -----------------------------------------

	ASSERT_EQ(last_t<int>,
	          int);

	ASSERT_EQ(last_t<one_value>,
	          value_box<untyped_constant<1>{}>);

	ASSERT_EQ(last_t<q<int>>,
	          int);

	ASSERT_EQ(last_t<q<int, double>>,
	          double);

	ASSERT_EQ(last_t<int, double>,
	          double);

	ASSERT_EQ(last_t<q<nil, double>>,
	          double);

	ASSERT_EQ(last_t<q<int, nil>>,
	          q<>);

	// ASSERT_EQ(last_t<nil>, ???);	// last: empty list

	// -----------------------------------------

	ASSERT_EQ(init_t<int>,
	          q<>);

	ASSERT_EQ(init_t<one_value>,
	          q<>);

	ASSERT_EQ(init_t<q<int>>,
	          q<>);

	ASSERT_EQ(init_t<q<int, double>>,
	          q<int>);

	ASSERT_EQ(init_t<int, double>,
	          q<int>);

	ASSERT_EQ(init_t<q<nil, double>>,
	          q<q<>>);

	ASSERT_EQ(init_t<q<int, nil>>,
	          q<int>);

	// ASSERT_EQ(init_t<nil>, ???);	// init: empty list

	// -----------------------------------------

	ASSERT_EQ(logical_xor_v<>,
	          false);

	ASSERT_EQ(logical_xor_v<true_value>,
	          true);

	ASSERT_EQ(logical_xor_v<true_value, false_value>,
	          true);

	ASSERT_EQ(logical_xor_v<true_value, false_value, size_value<42>>,
	          false);

	ASSERT_EQ(logical_xor_v<true_value, false_value, size_value<42>, untyped_value<-1>>,
	          true);

	ASSERT_EQ(logical_xor_v<true_value, false_value, size_value<42>, untyped_value<-1>, zero_value>,
	          true);


	// -----------------------------------------

	ASSERT_EQ(max_v<zero_value>,
	          0);

	ASSERT_EQ(max_v<zero_value, size_value<1>>,
	          1);

	ASSERT_EQ(max_v<zero_value, size_value<1>, untyped_value<-2>>,
	          -2);

	ASSERT_EQ(max_v<zero_value, size_value<1>, value_box<-2>>,
	          1);


	// -----------------------------------------

	ASSERT_EQ(min_v<zero_value>,
	          0);

	ASSERT_EQ(min_v<zero_value, size_value<1>>,
	          0);

	ASSERT_EQ(min_v<size_value<1>, zero_value>,
	          0);

	ASSERT_EQ(min_v<zero_value, size_value<1>, untyped_value<-2>>,
	          -2);

	ASSERT_EQ(min_v<zero_value, size_value<1>, value_box<-2>>,
	          -2);

	ASSERT_EQ(min_v<size_value<1>, untyped_value<-2>>,
	          1);

	ASSERT_EQ(min_v<size_value<1>, value_box<-2>>,
	          -2);


	// -----------------------------------------

	ASSERT_EQ(apply_t<quote, q<int, double>, q<char, quote>>,
	          q<int, double, char, quote>);

	using Tuple = DTML_FUNC_BOX(std::tuple);

	ASSERT_EQ(apply_t<Tuple, q<int, double, char>>,
		  std::tuple<int, double, char>);


	// -----------------------------------------

	ASSERT_EQ(invoke_t<compose_t<quote, quote1, quote2, quote3>, int, double, box<int>, box<double>>,
	          q<q<size_value<1>, int, double, int, double>, q<size_value<2>, int, double, int, double>, q<size_value<3>, int, double, int, double>>);


	// -----------------------------------------

	ASSERT_EQ(until_t<positive, inc, value_box<-5>>,
	          q<value_box<1>>);

	ASSERT_EQ(until_t<negative, dec, value_box<3>>,
	          q<value_box<-1>>);


	// -----------------------------------------

	ASSERT_EQ(maybe_t<int, quote, double>,
	          int);

	ASSERT_EQ(maybe_t<int, quote, box<double>>,
	          q<double>);


	// -----------------------------------------

	ASSERT_EQ(fmap_t<quote, double>,
	          null);

	ASSERT_EQ(fmap_t<quote, box<double>>,
	          q<double>);


	// -----------------------------------------

	ASSERT_EQ(invoke_t<power_t<size_value<3>, quote>, int>,
	          q<q<q<int>>>);

	ASSERT_EQ(invoke_t<power_t<size_value<3>, quote>, int, double>,
	          q<q<q<int, double>>>);

	ASSERT_EQ(invoke_t<power_t<size_value<3>, quote>, q<int, double>>,
	          q<q<q<q<int, double>>>>);

	ASSERT_EQ(invoke_t<power_t<size_value<3>, quote>, int, nil, double>,
	          q<q<q<int, q<>, double>>>);

	ASSERT_EQ(invoke_t<power_t<one_value, quote>, int>,
	          q<int>);

	ASSERT_EQ(invoke_t<power_t<one_value, quote>, int, double>,
	          q<int, double>);

	ASSERT_EQ(invoke_t<power_t<one_value, quote>, q<int, double>>,
	          q<q<int, double>>);

	ASSERT_EQ(invoke_t<power_t<one_value, quote>, int, nil, double>,
	          q<int, q<>, double>);

	ASSERT_EQ(invoke_t<power_t<zero_value, quote>, int>,
	          int);

	ASSERT_EQ(invoke_t<power_t<zero_value, quote>, int, double>,
	          int);

	ASSERT_EQ(invoke_t<power_t<zero_value, quote>, q<int, double>>,
	          q<int, double>);

	ASSERT_EQ(invoke_t<power_t<zero_value, quote>, int, nil, double>,
	          int);

	ASSERT_EQ(invoke_t<power_t<value_box<-2>, quote>, int>,
	          int);

	ASSERT_EQ(invoke_t<power_t<value_box<-2>, quote>, int, double>,
	          int);

	ASSERT_EQ(invoke_t<power_t<value_box<-2>, quote>, q<int, double>>,
	          q<int, double>);

	ASSERT_EQ(invoke_t<power_t<value_box<-2>, quote>, int, nil, double>,
	          int);

	ASSERT_EQ(invoke_t<power_t<untyped_value<-2>, quote>, int>,
	          int);

	ASSERT_EQ(invoke_t<power_t<untyped_value<-2>, quote>, int, double>,
	          int);

	ASSERT_EQ(invoke_t<power_t<untyped_value<-2>, quote>, q<int, double>>,
	          q<int, double>);

	ASSERT_EQ(invoke_t<power_t<untyped_value<-2>, quote>, int, nil, double>,
	          int);


	// -----------------------------------------

	ASSERT_EQ(almost_same_v<int, double>,
	          false);

	ASSERT_EQ(almost_same_v<int, int, double>,
	          false);

	ASSERT_EQ(almost_same_v<int, int, int, double>,
	          false);

	ASSERT_EQ(almost_same_v<int, int, int, int>,
	          true);

	ASSERT_EQ(almost_same_v<size_value<2>, untyped_value<2>, value_box<2>, invoke<dec, invoke<inc, invoke<inc, invoke<car, invoke<cons, one_value, q<int, double, char>>>>>>>,
	          true);

	ASSERT_EQ(almost_same_v<int, strict::infinitely_repeat<double>>,
	          false);

	ASSERT_EQ(almost_same_v<size_value<2>, untyped_value<2>, invoke<dec, invoke<inc, invoke<inc, invoke<car, invoke<cons, one_value, q<int, double, char>>>>>>>,
	          true);

	ASSERT_EQ(almost_same_v<size_value<2>, untyped_value<2>, invoke<dec, invoke<inc, invoke<inc, invoke<car, invoke<cons, one_value, q<int, double, char>>>>>>,
				  strict::infinitely_repeat<zero_value>>,
	          false);


	// -----------------------------------------

	ASSERT_EQ(find_if_t<identity, zero_value, false_value, size_value<0>, value_box<0>, one_value, true_value, null, size_value<42>>,
	          q<value_box<untyped_constant<1>{}>, value_box<true>, null, size_value<42>>);

	ASSERT_EQ(find_if_t<identity, zero_value, false_value, size_value<0>, value_box<0>, true_value, null, size_value<42>>,
	          q<value_box<true>, null, size_value<42>>);

	ASSERT_EQ(find_if_t<identity, zero_value, false_value, size_value<0>, value_box<0>, size_value<42>>,
	          q<size_value<42>>);

	ASSERT_EQ(find_if_t<identity, zero_value, false_value, size_value<0>, value_box<0>>,
	          q<>);

	DISABLE(ASSERT_EQ(find_if_t<identity, zero_value, false_value, size_value<0>, value_box<0>, size_value<42>, strict::infinitely_repeat<zero_value>>,
		  strict::sequence<size_value<42>, strict::infinitely_repeat<zero_value>>));


	// -----------------------------------------

	ASSERT_EQ(find_t<size_value<0>, zero_value, false_value, size_value<0>, value_box<0>, one_value, true_value, null, size_value<42>>,
	          value_box<true>);

	ASSERT_EQ(find_t<false_value, zero_value, false_value, size_value<0>, value_box<0>, one_value, true_value, null, size_value<42>>,
	          value_box<true>);

	ASSERT_EQ(find_t<one_value, zero_value, false_value, size_value<0>, value_box<0>, one_value, true_value, null, size_value<42>>,
	          value_box<true>);

	ASSERT_EQ(find_t<true_value, zero_value, false_value, size_value<0>, value_box<0>, one_value, true_value, null, size_value<42>>,
	          value_box<true>);

	ASSERT_EQ(find_t<null, zero_value, false_value, size_value<0>, value_box<0>, one_value, true_value, null, size_value<42>>,
	          value_box<true>);

	ASSERT_EQ(find_t<untyped_value<42>, zero_value, false_value, size_value<0>, value_box<0>, one_value, true_value, null, size_value<42>>,
	          value_box<true>);

	ASSERT_EQ(find_t<identity, zero_value, false_value, size_value<0>, value_box<0>, one_value, true_value, null, size_value<42>>,
	          value_box<false>);

	ASSERT_EQ(find_t<untyped_value<42>, invoke<concat, zero_value, false_value, size_value<0>, value_box<0>, one_value, true_value, null, size_value<42>, strict::infinitely_repeat<int>>>,
	          value_box<true>);


	// -----------------------------------------

	ASSERT_EQ(maybe_to_list_t<null>, nil);

	ASSERT_EQ(maybe_to_list_t<fmap_t<car, null>>,
		  nil);

	ASSERT_EQ(maybe_to_list_t<value_box<42>>,
		  nil);

	ASSERT_EQ(maybe_to_list_t<box<value_box<42>>>,
		  q<value_box<42>>);


	// -----------------------------------------

	ASSERT_FALSE(HasType<list_to_maybe_t<nil>>);

	ASSERT_EQ(list_to_maybe_t<nil>, null);


	ASSERT_TRUE(HasType<list_to_maybe_t<value_box<42>>>);

	ASSERT_EQ(type_t<list_to_maybe_t<value_box<42>>>,
		  value_box<42>);


	ASSERT_TRUE(HasType<list_to_maybe_t<q<value_box<42>>>>);

	ASSERT_EQ(type_t<list_to_maybe_t<q<value_box<42>>>>,
		  value_box<42>);


	// -----------------------------------------

	ASSERT_TRUE(HasType<when_t<true_value, value_box<42>>>);

	ASSERT_FALSE(HasType<when_t<false_value, value_box<42>>>);

	ASSERT_EQ(when_t<false_value, value_box<42>>, null);
}
