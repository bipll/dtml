#include <list>
#include <string>
#include <tuple>
#include <utility>

#include <dtml/let.h>

#include "fixture.h"

#include <iostream>
#include "show.h"

TEST(literal_pattern) {
	using namespace dtml;

	using String = std::string;
  
	ASSERT_EQ(let_t<std::string, String,
		        std::list<char>>,
		  std::list<char>);

	/*
	using NoCharList = let_t<std::list<char>, String,  // different types — no match
		                 std::list<char>>;         // cannot be evaluated
				 */
}

TEST(variable) {
	using namespace dtml;

	ASSERT_EQ(let_t<var<'c'>, char,
		        std::list<var<'c'>>>,
		  std::list<char>);
 
	ASSERT_EQ(let_t<var<string{"char"}>, char,
		        std::list<var<string{"char"}>>>,
		  std::list<char>);
}

TEST(ignore) {
	using namespace dtml;

	using String = std::string;

	ASSERT_EQ(let_t<ignore, String,
		        std::list<char>>,
		  std::list<char>);
}

TEST(template) {
	using namespace dtml;

	// -----------------------------------------

	using String = std::string;  // std::basic_string<char>

	ASSERT_EQ(let_t<std::basic_string<var<'c'>>, String,
		        std::list<var<'c'>>>,
		  std::list<char>);

	// -----------------------------------------


	using Point2D = std::pair<double, double>;
  
	ASSERT_EQ(let_t<std::pair<var<'R'>, var<'R'>>, Point2D,
		        var<'R'>>,
		  double);

	// using AnnotatedValue = std::pair<double, std::string>;

	// ASSERT_EQ(let_t<std::pair<var<'R'>, var<'R'>>, AnnotatedValue,  // no match — ambiguous binding for var<'R'>
	//	           var<'R'>>,
	//	     double);

	// using DamagedString = std::basic_string<char, std::char_traits<char>, std::allocator<char16_t>>;  // a strange instance

	// using DamagedCharList = let_t<
	//	std::basic_string<arg<'c'>>, DamagedString,  // error — no match, arg<'c'> is bound to char and thus std::allocator<arg<'c'>> does not match std::allocator<std::char16_t>
	//	std::list<arg<'c'>>>;
	//
	// using CharList = let_t<
	// 	var<0>, char,
	// 	std::pair<var<string{"ambiguous var"}>, var<string{"ambiguous var"}>>, std::pair<int, double>,  // ambiguous binding for "ambiguous var"
	// 	std::list<var<0>>>;
}

TEST(invoke) {
	using namespace dtml;

	using String = std::string;
	using IntList = std::list<int>;

	ASSERT_EQ(let_t<std::basic_string<var<'c'>>,      String,
		        invoke<var<'l'>, ignore, ignore>, IntList,
		        invoke<var<'l'>, var<'c'>>>,
		  std::list<char>);
}

TEST(seq) {
	using namespace dtml;

	using MyTuple = std::tuple<int, double, char, std::string>;

	using MyOtherTuple = std::tuple<MyTuple, std::list<char>, float, unsigned>;
  
	ASSERT_EQ(let_t<std::tuple<ignore, ignore, seq<'S'>>, MyTuple,
		        std::tuple< std::size_t, std::string, seq<'S'>>>,
		  std::tuple<std::size_t, std::string, char, std::string>);

	ASSERT_EQ(let_t<std::tuple<var<0>, var<1>, seq<ignore{}>>, MyTuple,
		        std::tuple<var<0>, var<1>>>,
		  std::tuple<int, double>);

	ASSERT_EQ(let_t<std::tuple<ignore, ignore, seq<'S'>>, MyTuple,
		        std::tuple<var<0>, ignore, seq<'T'>>, MyOtherTuple,
		        std::tuple<seq<'S'>, var<0>, seq<'T'>>>,
		  std::tuple<char, std::string, MyTuple, float, unsigned>);
}

TEST(bindings) {
	using namespace dtml;

	using String = std::string;

	ASSERT_EQ(let_t<std::basic_string<var<'c'>>, String,
		        var<'l'>, std::list<var<'c'>>,
			var<'l'>>,
		  std::list<char>);

	ASSERT_EQ(let_t<std::tuple<ignore, seq<0>>, std::tuple<int, double, char>,
		        var<0>, seq<0>,
			std::tuple<var<0>>>,
		  std::tuple<double, char>);
}
