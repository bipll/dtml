#ifndef DTML__STRING_H_fd11f06e2409641c7a835ad4dd62c7cd2d7243e9
#define DTML__STRING_H_fd11f06e2409641c7a835ad4dd62c7cd2d7243e9

#include <algorithm>
#include <compare>
#include <iterator>
#include <limits>
#include <ostream>
#include <type_traits>

namespace dtml {

template<class N> class string;

template<std::size_t k> using make_string = string<std::integral_constant<std::size_t, k>>;

namespace string_ {

template<class S> struct strlen;
template<class S> static constexpr auto strlen_v = strlen<std::remove_cvref_t<S>>::value;

template<std::size_t m> struct strlen<char const[m]>: std::integral_constant<std::size_t, m - 1> {};
template<std::size_t m> struct strlen<char[m]>: std::integral_constant<std::size_t, m - 1> {};
template<class M> struct strlen<string<M>>: M {};
template<> struct strlen<char>: std::integral_constant<std::size_t, 1> {};

}

template<class N> class string {
	static constexpr std::size_t n = N::value;
	static constexpr std::size_t buf_sz = n + 1;
public:
	using Width = N;
	using This = string;
	using iterator = char const *;
	using const_iterator = char const *;

	constexpr string() = default;

	constexpr string(char c): buf{c}, l{1} {}

	constexpr string(std::size_t sz, char c): l{sz} {
		std::fill(unsafe_begin(), unsafe_end(), c);
	}

	constexpr string(char const *s) { for(; l < n && s[l]; ++l) buf[l] = s[l]; }

	constexpr string(char const *s, std::size_t len) {
		l = std::min(n, len);
		std::copy(s, s + l, unsafe_begin());
	}

	constexpr string &operator=(char c) {
		const_cast<char &>(buf[0]) = c;
		const_cast<char &>(buf[1]) = '\0';
		l = 1;
		return *this;
	}

	constexpr string &operator=(char const *s) {
		for(l = 0; l <= n && s[l]; ++l) const_cast<char &>(buf[l]) = s[l];
		const_cast<char &>(buf[l]) = '\0';
		return *this;
	}

	template<std::size_t m> constexpr string(char const (&that)[m]): l(std::min(m - 1, n)) { std::copy(that, that + l, buf); }

	constexpr string(string const &that): l{that.l} { std::copy(that.begin(), that.end(), unsafe_begin()); }
	
	template<class M> constexpr string(string<M> const &that): l{std::min(that.size(), n)} { std::copy(that.begin(), that.begin() + l, unsafe_begin()); }

	constexpr std::size_t size() const { return l; }
	constexpr std::size_t length() const { return size(); }

	constexpr char operator[](std::size_t i) const { return buf[i]; }

	constexpr char const *c_str() const { return buf; }
	constexpr char const *data() const { return c_str(); }

	constexpr char const *begin() { return buf; }
	constexpr char const *begin() const { return buf; }
	constexpr char const *cbegin() const { return buf; }

	constexpr char const *end() { return buf + l; }
	constexpr char const *end() const { return buf + l; }
	constexpr char const *cend() const { return buf + l; }


	template<class S> constexpr auto operator<=>(S const &that) const { return std::lexicographical_compare_three_way(begin(), end(), std::cbegin(that), std::cend(that)); }

	template<std::size_t M> constexpr auto operator<=>(char const (&that)[M]) const {
		return that[M - 1]?
			std::lexicographical_compare_three_way(begin(), end(), std::cbegin(that), std::cend(that))
			: std::lexicographical_compare_three_way(begin(), end(), std::cbegin(that), std::cend(that) - 1);
	}

	template<std::size_t M> constexpr auto operator<=>(char (&that)[M]) const { return operator<=>(const_cast<char const (&)[M]>(that)); }

	constexpr auto operator<=>(char const *that) const {
		auto here = *buf <=> *that;
		for(std::size_t i{1}; i <= l && std::is_eq(here); ++i) {
			here = buf[i] <=> that[i];
		}
		return here;
	}

	constexpr auto operator<=>(char *that) const { return operator<=>(const_cast<char const *>(that)); }


	template<class S> constexpr bool operator==(S const &that) const { return std::is_eq  (*this <=> that); }
	template<class S> constexpr bool operator!=(S const &that) const { return std::is_neq (*this <=> that); }
	template<class S> constexpr bool operator< (S const &that) const { return std::is_lt  (*this <=> that); }
	template<class S> constexpr bool operator> (S const &that) const { return std::is_gt  (*this <=> that); }
	template<class S> constexpr bool operator<=(S const &that) const { return std::is_lteq(*this <=> that); }
	template<class S> constexpr bool operator>=(S const &that) const { return std::is_gteq(*this <=> that); }

	template<class S> friend constexpr auto operator<=>(S const &that, string const &self) { return self.operator<=>(that); }
	template<class S> friend constexpr bool operator==(S const &that, string const &self) { return self.operator==(that); }
	template<class S> friend constexpr bool operator!=(S const &that, string const &self) { return self.operator!=(that); }
	template<class S> friend constexpr bool operator< (S const &that, string const &self) { return self.operator< (that); }
	template<class S> friend constexpr bool operator> (S const &that, string const &self) { return self.operator> (that); }
	template<class S> friend constexpr bool operator<=(S const &that, string const &self) { return self.operator<=(that); }
	template<class S> friend constexpr bool operator>=(S const &that, string const &self) { return self.operator>=(that); }

	template<class S> constexpr auto operator+(S const &that) const {
		make_string<n + string_::strlen_v<S>> ret_val(*this);
		ret_val.append(that);
		return ret_val;
	}

	template<class M> constexpr This &append(string<M> const &that) {
		std::size_t amount = std::min(n - l, that.size());
		std::copy(that.begin(), that.begin() + amount, unsafe_end());
		l += amount;
		return *this;
	}

	template<std::size_t m> constexpr This &append(char const (&that)[m]) {
		std::size_t amount = std::min(n - l, m - 1);
		std::copy(that, that + amount, end());
		l += amount;
		return *this;
	}

	template<std::size_t m> constexpr This &append(char (&that)[m]) {
		std::size_t amount = std::min(n - l, m - 1);
		std::copy(that, that + amount, end());
		l += amount;
		return *this;
	}

	constexpr This &append(char const *that) {
		std::size_t i{};
		for(; i + l <= n && that[i]; ++i) const_cast<char &>(buf[l + i]) = that[i];
		l += i;
		return *this;
	}

	constexpr This &append(char c) {
		if(l < n) const_cast<char &>(buf[l++]) = c;
		return *this;
	}

	template<class S> constexpr This &operator+=(S const &that) { return append(that); }

	constexpr auto join() const { return make_string<1>{}; }

	template<class S, class... Ss> constexpr auto join(S const &that, Ss const &...those) const {
		make_string<(string_::strlen_v<S> + ... + string_::strlen_v<Ss>) + n * sizeof...(those)> ret_val(that);
		if(size() == 0) (ret_val.append(those), ...);
		else (ret_val.append(*this).append(those), ...);
		return ret_val;
	}

	constexpr This reverse() const {
		auto ret_val = *this;
		for(std::size_t i{}; i < l / 2; ++i) std::swap(const_cast<char &>(ret_val.buf[i]), const_cast<char &>(ret_val.buf[l - 1 - i]));
		return ret_val;
	}

	constexpr char const *find(char c, const_iterator start) const {
		return std::find(start, end(), c);
	}

	constexpr char const *find(char c) const {
		return find(c, begin());
	}

	char buf[buf_sz]{};
	std::size_t l{};
private:
	constexpr char *unsafe_begin() { return const_cast<char *>(begin()); }
	constexpr char *unsafe_end  () { return const_cast<char *>(end  ()); }
};

template<class N, class M> inline constexpr bool operator<=>(string<N> const &left, string<M> const &right) { return left.operator<=>(right); }
template<class N, class M> inline constexpr bool operator==(string<N> const &left, string<M> const &right) { return left.operator==(right); }
template<class N, class M> inline constexpr bool operator!=(string<N> const &left, string<M> const &right) { return left.operator!=(right); }
template<class N, class M> inline constexpr bool operator< (string<N> const &left, string<M> const &right) { return left.operator< (right); }
template<class N, class M> inline constexpr bool operator> (string<N> const &left, string<M> const &right) { return left.operator> (right); }
template<class N, class M> inline constexpr bool operator<=(string<N> const &left, string<M> const &right) { return left.operator<=(right); }
template<class N, class M> inline constexpr bool operator>=(string<N> const &left, string<M> const &right) { return left.operator>=(right); }

template<std::size_t n> string(char const (&s)[n]) -> string<std::integral_constant<std::size_t, n - 1>>;

string(char) -> string<std::integral_constant<std::size_t, 1>>;

string() -> string<std::integral_constant<std::size_t, 0>>;

template<class CharT, class Traits, class N> std::basic_ostream<CharT, Traits> &operator<<(std::basic_ostream<CharT, Traits> &stream, string<N> const &s) {
	return stream << s.c_str();
}

template<class M> constexpr auto operator+(char left, string<M> const &right) {
	string<std::integral_constant<std::size_t, M::value + 1>> ret_val{left};
	ret_val.append(right);
	return ret_val;
}

template<std::size_t n, class M> constexpr auto operator+(char const (&left)[n], string<M> const &right) {
	static_assert(M::value + n > 0, "Illegal operands for +");

	string<std::integral_constant<std::size_t, M::value + n - 1>> ret_val{left};
	ret_val.append(right);
	return ret_val;
}

template<std::size_t n, class M> constexpr auto operator+(char (&left)[n], string<M> const &right) {
	static_assert(M::value + n > 0, "Illegal operands for +");

	string<std::integral_constant<std::size_t, M::value + n - 1>> ret_val{left};
	ret_val.append(right);
	return ret_val;
}

constexpr auto to_string(char c) {
	return string{c};
}

constexpr make_string<4> to_string(std::nullptr_t) {
	return "null";
}

constexpr make_string<5> to_string(bool b) {
	return b? "true" : "false";
}

namespace string_ {

template<class Int> constexpr std::size_t required_width() {
	auto max = std::numeric_limits<Int>::max();
	std::size_t ret_val = 8;
	while(max > 100'000'000) {
		ret_val += 8;
		max /= 100'000'000;
	}
	return max >= 10'000'000? ret_val + 1 : ret_val;
}

template<class Unsigned> static constexpr auto render_unsigned(Unsigned x, char prefix = '\0') {
	constexpr auto width = required_width<Unsigned>();
	if(x == 0) return make_string<width>('0');

	char ret_val[width + 1];
	ret_val[width] = '\0';
	std::size_t i = width;
	while(x > 0) {
		ret_val[--i] = '0' + x % 10;
		x /= 10;
	}
	if(prefix) ret_val[--i] = prefix;
	return make_string<width>(ret_val + i, width - i);
}

template<class Signed> static constexpr auto render_signed(Signed s) {
	using Unsigned = std::make_unsigned_t<Signed>;
	if(s == std::numeric_limits<Signed>::min()) return render_unsigned(static_cast<Unsigned>(-(std::numeric_limits<Signed>::min() + 1)), '-');
	return s < 0? render_unsigned(static_cast<Unsigned>(-s), '-') : render_unsigned(static_cast<Unsigned>(s));
}

}

#define INT_TO_STRING(type)								\
constexpr auto to_string(unsigned type x) { return string_::render_unsigned(x); }	\
constexpr auto to_string(signed   type x) { return string_::render_signed  (x); }

INT_TO_STRING(char);
INT_TO_STRING(short int);
INT_TO_STRING(int);
INT_TO_STRING(long int);

#undef INT_TO_STRING

namespace string_ {

template<class Dst, class Float> constexpr Dst render_float(Float x) {
	if(x == 0) return '0';

	Dst ret_val;
	if(x < 0) ret_val = '-', x = -x;

	int exp{};
	while(x < 1) --exp, x *= 10;
	while(x > 10) ++exp, x /= 10;
	int whole = x;
	x -= whole;

	int y = x * 10'000'000;
	if(y % 10 >= 5) y += 10;
	y /= 10;
	if(y >= 1'000'000) ++whole, y = 0;

	ret_val.append(to_string(whole));
	if(y > 0) {
		ret_val.append('.');
		std::size_t lost_prec{};
		while(!(y % 10)) ++lost_prec, y /= 10;
		make_string<6> frac{to_string(y)};
		for(std::size_t i = frac.size() + lost_prec; i < 6; ++i) ret_val.append('0');
		ret_val.append(frac);
	}
	if(exp != 0) ret_val.append('e').append(to_string(exp));
	return ret_val;
}

}

constexpr auto to_string(float x) { return string_::render_float<make_string<16>, float>(x); }
constexpr auto to_string(double x) { return string_::render_float<make_string<24>, double>(x); }

template<class M> constexpr auto to_string(string<M> const &s) {
	make_string<M::value * 2 + 2> ret_val{'"'};
	for(auto i = s.begin(); i != s.end(); ++i) {
		if(*i == '"' || *i == '\\') ret_val.append('\\');
		ret_val.append(*i);
	}
	ret_val.append('"');
	return ret_val;
}

template<std::size_t m> constexpr auto to_string(char const (&s)[m]) {
	make_string<(m - 1) * 2 + 2> ret_val{'"'};
	for(std::size_t i{}; i != m - 1; ++i) {
		if(s[i] == '"' || s[i] == '\\') ret_val.append('\\');
		ret_val.append(s[i]);
	}
	ret_val.append('"');
	return ret_val;
}

template<std::size_t m> constexpr auto to_string(char (&s)[m]) {
	make_string<(m - 1) * 2 + 2> ret_val{'"'};
	for(std::size_t i{}; i != m - 1; ++i) {
		if(s[i] == '"' || s[i] == '\\') ret_val.append('\\');
		ret_val.append(s[i]);
	}
	ret_val.append('"');
	return ret_val;
}

}

#endif
