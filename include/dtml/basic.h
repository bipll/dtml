#ifndef DTML__BASIC_H_2dccb290be9274644dc45eba5d25e270a09ba114
#define DTML__BASIC_H_2dccb290be9274644dc45eba5d25e270a09ba114

#include <type_traits>

#include "box.h"
#include "func_box.h"
#include "macro.h"
#include "string.h"

namespace dtml {

// Bottom type for error situations.
template<string message> struct exception;


#define DTML_THROW(T, msg) static_assert(invoke_v<konst<false_value>, T>, msg)


using identity = DTML_FUNC_BOX(box);


namespace strict {

template<class...> struct invoke: lazy {};

template<HasFunc F, class... Ts> struct invoke<F, Ts...>: box<call<get_func<F>::template func, Ts...>> {};

template<class... Ts> struct invoke<q<Ts...>>: invoke<Ts...> {};

}

using strict::invoke;

template<class... Ts> using invoke_t = type_t<invoke<Ts...>>;

template<class... Ts> inline constexpr auto invoke_v = value_v<invoke<Ts...>>;

template<class T, class... Ts> concept Satisfies = static_cast<bool>(invoke_v<Ts..., T>);

template<class T, class... Ts> concept Unsatisfies = !Satisfies<T, Ts...>;


#define BOXED_BY_DEFAULT true_value

#define LAZY_TEMPLATE(name, ...)					\
namespace strict {							\
									\
template<class... Ts> struct name;					\
									\
template<class... Ts> using name ## _t = type_t<name<Ts...>>;		\
									\
template<class... Ts> struct name: make_lazy<__VA_ARGS__> {};		\
									\
template<class... Ts> struct boxed<name<Ts...>>: BOXED_BY_DEFAULT {};	\
									\
}									\
									\
using name = DTML_FUNC_BOX(strict::name);				\
									\
using strict::name ## _t;						\
									\
namespace strict


#define LAZY_VALUE_TEMPLATE(name, ...)							\
LAZY_TEMPLATE(name, __VA_ARGS__) {							\
	template<class... Ts> inline constexpr auto name ## _v = value_v<name<Ts...>>;	\
}											\
											\
using strict::name ## _v;								\
											\
namespace strict


#define LAZY_LIST_TRANSFORM(TYPE, name, ...)							\
TYPE(name, __VA_ARGS__) {									\
	template<class... Ts> struct unbox<name<Ts...>>: call<name, concat_t<Ts...>> {};	\
}												\
												\
namespace strict


#define LAZY_LIST_OPERATOR(name, ...) LAZY_LIST_TRANSFORM(LAZY_TEMPLATE, name, __VA_ARGS__)


#define LAZY_LIST_SCALAR_MAPPING(name, SingleArgument, UnsingleArgument, ...)			\
LAZY_LIST_TRANSFORM(LAZY_VALUE_TEMPLATE, name, __VA_ARGS__) {					\
	template<SingleArgument T> struct unbox<name<T>>: call<name, q<T>> {};			\
												\
	template<HasType T> UnsingleArgument struct unbox<name<T>>: call<name, type_t<T>> {};	\
}												\
												\
namespace strict

#define LAZY_LIST_FUNCTIONAL(name, ...) LAZY_LIST_SCALAR_MAPPING(name, HasValue, requires (!HasValue<T>), __VA_ARGS__)

#define LAZY_LIST_SCALAR(name, ...) LAZY_LIST_SCALAR_MAPPING(name, class,, __VA_ARGS__)


#define DTML_STRICT_WITH_PACKED_ARGS(func)					\
template<HasType T> struct unbox<func<T>>: unbox<func<type_t<T>>> {};		\
template<class... Ts> struct unbox<func<q<Ts...>>>: unbox<func<Ts...>> {}

namespace strict { DTML_STRICT_WITH_PACKED_ARGS(invoke); }


LAZY_VALUE_TEMPLATE(to_bool) {
	template<class B, class... Bs> struct unbox<to_bool<B, Bs...>>: box<bool_value<static_cast<bool>(value_v<B>)>> {};
}


LAZY_VALUE_TEMPLATE(cond) {
	template<class If, class Then, class... Else> struct unbox<cond<If, Then, Else...>>: call<cond, Else...> {};

	template<HasProperty<to_bool> If, class Then, class... Else> struct unbox<cond<If, Then, Else...>>: box<Then> {};

	template<class Default> struct unbox<cond<Default>>: box<Default> {};
}


#define DTML_STRICT_NON_UNARY(func)										\
template<class ArgPack> struct unbox<func<ArgPack>> { DTML_THROW(ArgPack, #func ": insufficient args"); };	\
														\
DTML_STRICT_WITH_PACKED_ARGS(func)


LAZY_VALUE_TEMPLATE(empty) {
	template<class... Ts> struct unbox<empty<Ts...>>: box<false_value> {};

	template<class T, class... Ts> requires std::is_same_v<type_t<T>, nil> struct unbox<empty<T, Ts...>>: call<empty, Ts...> {};

	template<> struct unbox<empty<>>: box<true_value> {};
}


LAZY_VALUE_TEMPLATE(car) {
	template<class T, class... Ts> struct unbox<car<T, Ts...>>: box<T> {};

	template<class T, class... Ts> requires HasType<T> struct unbox<car<T, Ts...>>: unbox<car<type_t<T>, Ts...>> {};

	template<class T, class... Ts, class... Us> struct unbox<car<q<T, Ts...>, Us...>>: box<T> {};

	template<class... Ts> struct unbox<car<nil, Ts...>>: unbox<car<Ts...>> {};

	template<> struct unbox<car<>>: box<exception<string{"car: empty list"}>> {};
}


LAZY_VALUE_TEMPLATE(logical_not) {
	template<class B, class... Bs> struct unbox<logical_not<B, Bs...>>: box<bool_value<!value_v<B>>> {};

	template<> struct unbox<logical_not<>>: box<true_value> {};
}


template<class... Ts> concept PartlyIterable = requires { typename car_t<Ts...>; } && (sizeof...(Ts) > 1 || sizeof...(Ts) == 1 && !call_v<std::is_same, invoke<car, Ts...>, get_type<Ts...>>);

namespace strict { template<class... Ts> using partly_iterable = bool_value<PartlyIterable<Ts...>>; }

using strict::partly_iterable;

template<class... Ts> inline constexpr auto partly_iterable_v = value_v<partly_iterable<Ts...>>;


#define DTML_BASIC_LOGICAL(name, defval, ...)								\
LAZY_VALUE_TEMPLATE(name) {										\
	template<class B, class... Bs> struct unbox<name<B, Bs...>>: call<cond, B, __VA_ARGS__> {};	\
													\
	template<class B> struct unbox<name<B>>: box<B> {};						\
													\
	template<> struct unbox<name<>>: box<defval ## _value> {};					\
}

DTML_BASIC_LOGICAL(logical_and, true, logical_and<Bs...>, B);
DTML_BASIC_LOGICAL(logical_or, false, B, logical_or<Bs...>);

#undef DTML_BASIC_LOGICAL


LAZY_VALUE_TEMPLATE(finite) {
	template<class... Ts> struct unbox<finite<Ts...>>: box<true_value> {};

	template<class T, class... Ts> struct unbox<finite<T, Ts...>>: call<logical_and, finite<T>, finite<Ts...>> {};

	template<class T> struct unbox<finite<T>>: box<true_value> {};

	template<HasType T> struct unbox<finite<T>>: call<finite, type_t<T>> {};
}

template<class... Ts> concept Finite = finite_v<Ts...>;


LAZY_VALUE_TEMPLATE(infinite) {
	template<Finite... Ts> struct unbox<infinite<Ts...>>: box<false_value> {};
}

template<class... Ts> concept Infinite = infinite_v<Ts...>;


#define INFINITE(...) struct finite<__VA_ARGS__>: false_value {}


LAZY_TEMPLATE(force_list) {
	template<class... Ts> struct unbox<force_list<Ts...>> {};
}


namespace strict {

template<class... Ts> struct sequence: lazy {};

}

using strict::sequence;

namespace strict {
	template<class... Ts> struct short_circuiting<sequence<Ts...>, finite>: true_value {};
	template<class... Ts> struct unbox<finite<sequence<Ts...>>>: call<finite, Ts...> {};

	template<class... Ts> struct short_circuiting<sequence<Ts...>, infinite>: true_value {};
	template<class... Ts> struct unbox<infinite<sequence<Ts...>>>: call<infinite, Ts...> {};

	template<class... Ts> struct short_circuiting<sequence<Ts...>, empty>: true_value {};
	template<class... Ts> struct unbox<empty<sequence<Ts...>>>: call<empty, Ts...> {};

	template<class... Ts> struct short_circuiting<sequence<Ts...>, car>: true_value {};
	template<class... Ts> struct unbox<car<sequence<Ts...>>>: call<car, Ts...> {};

	template<class... Ts> struct short_circuiting<sequence<Ts...>, sequence>: true_value {};
	template<class... Ts> struct unbox<sequence<sequence<Ts...>>>: call<sequence, Ts...> {};
}


LAZY_TEMPLATE(concat) {
	template<class... Ts> struct short_circuiting<concat<Ts...>, finite>: true_value {};
	template<class... Ts> struct finite<concat<Ts...>>: call<finite, Ts...> {};

	template<class... Ts> struct short_circuiting<concat<Ts...>, infinite>: true_value {};
	template<class... Ts> struct infinite<concat<Ts...>>: call<infinite, Ts...> {};

	template<class... Ts> struct short_circuiting<concat<Ts...>, empty>: true_value {};
	template<class... Ts> struct empty<concat<Ts...>>: call<empty, Ts...> {};

	template<class... Ts> struct short_circuiting<concat<Ts...>, car>: true_value {};
	template<class... Ts> struct unbox<car<concat<Ts...>>>: call<car, Ts...> {};

	template<class... Ts> struct unbox<concat<Ts...>>: box<sequence<Ts...>> {};
	template<Finite... Ts> struct unbox<concat<Ts...>>: call<force_list, Ts...> {};
	template<class... Ts> struct unbox<concat<sequence<Ts...>>>: box<sequence<Ts...>> {};
	//template<class... Ts, class... Us, class... Rs> struct unbox<concat<sequence<Ts...>, sequence<Us...>, Rs...>>: call<concat, sequence<Ts..., Us...>, Rs...> {};

	template<> struct unbox<concat<>>: box<nil> {};

}


LAZY_TEMPLATE(cdr) {
	template<class T, class... Ts> struct unbox<cdr<T, Ts...>>: call<cond,
		empty<T>, call<cdr, Ts...>,
		logical_not<finite<T>>, call<concat, cdr<T>, Ts...>,
		partly_iterable<T>, call<cdr, force_list<T>, Ts...>,
		call<concat, Ts...>> {};
	template<HasType T, class... Ts> struct unbox<cdr<T, Ts...>>: call<cdr, type_t<T>, Ts...> {};

	template<class T, class... Ts, class... Qs> struct unbox<cdr<q<T, Ts...>, Qs...>>: call<concat, q<Ts...>, Qs...> {};

	template<> struct unbox<cdr<>>: box<exception<string{"cdr: empty list"}>> {};


	template<class... Ts> struct short_circuiting<concat<Ts...>, cdr>: true_value {};
	template<class... Ts> struct unbox<cdr<concat<Ts...>>>: call<cdr, Ts...> {};

	template<class... Ts> struct short_circuiting<sequence<Ts...>, cdr>: true_value {};
	template<class... Ts> struct unbox<cdr<sequence<Ts...>>>: call<cdr, Ts...> {};
}


LAZY_VALUE_TEMPLATE(longer_than) {
	template<class... Ts> struct unbox<longer_than<Ts...>>: false_value {};

	template<HasValue N, class... Ts> struct unbox<longer_than<N, q<Ts...>>>: bool_value<(sizeof...(Ts) >= value_v<N>)> {};

	template<HasValue N, class... Ts> requires (value_v<N> <= 0) struct unbox<longer_than<N, sequence<Ts...>>>: box<true_value> {};

	template<HasValue N, class... Ts> requires (value_v<N> > 0 && !empty_v<Ts...>) class unbox<longer_than<N, sequence<Ts...>>> {
		using tail = cdr_t<Ts...>;
		using N1 = size_value<(value_v<N> - 1)>;
	public:
		using type = call<longer_than, N1, tail>;
	};
}

template<class T, std::size_t n> concept LongerThan = longer_than_v<size_value<n>, T>;

template<class T, std::size_t n> concept ShorterThan = LongerThan<T, 0> && !LongerThan<T, n>;

template<class T, std::size_t n, std::size_t m> concept Between = LongerThan<T, n> && ShorterThan<T, m>;

template<class T, std::size_t n> concept ExactSize = Between<T, n, n + 1>;


namespace strict {

template<class N, class T> using shorter_than = bool_value<ShorterThan<T, value_v<N>>>;

template<class N, class T> inline constexpr auto shorter_than_v = value_v<shorter_than<T, N>>;


template<class N, class M, class T> using between = bool_value<Between<T, value_v<N>, value_v<M>>>;

template<class N, class M, class T> inline constexpr auto between_v = value_v<between<T, N, M>>;


template<class N, class T> using exact_size = bool_value<ExactSize<T, value_v<N>>>;

template<class N, class T> inline constexpr auto exact_size_v = value_v<exact_size<T, N>>;

}

using strict::shorter_than;

using strict::shorter_than_v;


using strict::between;

using strict::between_v;


using strict::exact_size;

using strict::exact_size_v;



template<class... Ts> concept Iterable = PartlyIterable<Ts...> && requires { typename cdr_t<Ts...>; };


namespace strict {

template<class... Ts> requires empty_v<Ts...> struct unbox<force_list<Ts...>>: box<nil> {};

template<class... Ts> requires (!empty_v<Ts...> && Finite<Ts...>) class unbox<force_list<Ts...>> {
	template<class, class> struct cons;
	template<class U, class Us> using cons_t = type_t<cons<U, Us>>;
	template<class U, class... Us> struct cons<U, q<Us...>>: box<q<U, Us...>> {};
public:
	using type = cons_t<car_t<Ts...>, force_list_t<cdr<Ts...>>>;
};

}


LAZY_TEMPLATE(cons) {
	template<class T, class... Ts> struct unbox<cons<T, Ts...>>: call<concat, q<T>, Ts...> {};

	template<class... Ts> struct short_circuiting<cons<Ts...>, empty>: true_value {};
	template<class... Ts> struct unbox<empty<cons<Ts...>>>: box<false_value> {};

	template<class... Ts> struct short_circuiting<cons<Ts...>, car>: true_value {};
	template<class T, class... Ts> struct unbox<car<cons<T, Ts...>>>: box<T> {};

	template<class... Ts> struct short_circuiting<cons<Ts...>, cdr>: true_value {};
	template<class T, class... Ts> struct unbox<cdr<cons<T, Ts...>>>: call<concat, Ts...> {};
}


// %-)
namespace strict {

template<class... Ts> using caar   = call<car, call<car, Ts...>>;
template<class... Ts> using cadr   = call<car, call<cdr, Ts...>>;
template<class... Ts> using cdar   = call<cdr, call<car, Ts...>>;
template<class... Ts> using cddr   = call<cdr, call<cdr, Ts...>>;
template<class... Ts> using caaar  = call<car, call<car, call<car, Ts...>>>;
template<class... Ts> using cadar  = call<car, call<cdr, call<car, Ts...>>>;
template<class... Ts> using cdaar  = call<cdr, call<car, call<car, Ts...>>>;
template<class... Ts> using cddar  = call<cdr, call<cdr, call<car, Ts...>>>;
template<class... Ts> using caadr  = call<car, call<car, call<cdr, Ts...>>>;
template<class... Ts> using caddr  = call<car, call<cdr, call<cdr, Ts...>>>;
template<class... Ts> using cdadr  = call<cdr, call<car, call<cdr, Ts...>>>;
template<class... Ts> using cdddr  = call<cdr, call<cdr, call<cdr, Ts...>>>;
template<class... Ts> using caaaar = call<car, call<car, call<car, call<car, Ts...>>>>;
template<class... Ts> using cadaar = call<car, call<cdr, call<car, call<car, Ts...>>>>;
template<class... Ts> using cdaaar = call<cdr, call<car, call<car, call<car, Ts...>>>>;
template<class... Ts> using cddaar = call<cdr, call<cdr, call<car, call<car, Ts...>>>>;
template<class... Ts> using caadar = call<car, call<car, call<cdr, call<car, Ts...>>>>;
template<class... Ts> using caddar = call<car, call<cdr, call<cdr, call<car, Ts...>>>>;
template<class... Ts> using cdadar = call<cdr, call<car, call<cdr, call<car, Ts...>>>>;
template<class... Ts> using cdddar = call<cdr, call<cdr, call<cdr, call<car, Ts...>>>>;
template<class... Ts> using caaadr = call<car, call<car, call<car, call<cdr, Ts...>>>>;
template<class... Ts> using cadadr = call<car, call<cdr, call<car, call<cdr, Ts...>>>>;
template<class... Ts> using cdaadr = call<cdr, call<car, call<car, call<cdr, Ts...>>>>;
template<class... Ts> using cddadr = call<cdr, call<cdr, call<car, call<cdr, Ts...>>>>;
template<class... Ts> using caaddr = call<car, call<car, call<cdr, call<cdr, Ts...>>>>;
template<class... Ts> using cadddr = call<car, call<cdr, call<cdr, call<cdr, Ts...>>>>;
template<class... Ts> using cdaddr = call<cdr, call<car, call<cdr, call<cdr, Ts...>>>>;
template<class... Ts> using cddddr = call<cdr, call<cdr, call<cdr, call<cdr, Ts...>>>>;

template<class... Ts> using caar_t   = type_t<caar<Ts...>>;
template<class... Ts> using cadr_t   = type_t<cadr<Ts...>>;
template<class... Ts> using cdar_t   = type_t<cdar<Ts...>>;
template<class... Ts> using cddr_t   = type_t<cddr<Ts...>>;
template<class... Ts> using caaar_t  = type_t<caaar<Ts...>>;
template<class... Ts> using cadar_t  = type_t<cadar<Ts...>>;
template<class... Ts> using cdaar_t  = type_t<cdaar<Ts...>>;
template<class... Ts> using cddar_t  = type_t<cddar<Ts...>>;
template<class... Ts> using caadr_t  = type_t<caadr<Ts...>>;
template<class... Ts> using caddr_t  = type_t<caddr<Ts...>>;
template<class... Ts> using cdadr_t  = type_t<cdadr<Ts...>>;
template<class... Ts> using cdddr_t  = type_t<cdddr<Ts...>>;
template<class... Ts> using caaaar_t = type_t<caaaar<Ts...>>;
template<class... Ts> using cadaar_t = type_t<cadaar<Ts...>>;
template<class... Ts> using cdaaar_t = type_t<cdaaar<Ts...>>;
template<class... Ts> using cddaar_t = type_t<cddaar<Ts...>>;
template<class... Ts> using caadar_t = type_t<caadar<Ts...>>;
template<class... Ts> using caddar_t = type_t<caddar<Ts...>>;
template<class... Ts> using cdadar_t = type_t<cdadar<Ts...>>;
template<class... Ts> using cdddar_t = type_t<cdddar<Ts...>>;
template<class... Ts> using caaadr_t = type_t<caaadr<Ts...>>;
template<class... Ts> using cadadr_t = type_t<cadadr<Ts...>>;
template<class... Ts> using cdaadr_t = type_t<cdaadr<Ts...>>;
template<class... Ts> using cddadr_t = type_t<cddadr<Ts...>>;
template<class... Ts> using caaddr_t = type_t<caaddr<Ts...>>;
template<class... Ts> using cadddr_t = type_t<cadddr<Ts...>>;
template<class... Ts> using cdaddr_t = type_t<cdaddr<Ts...>>;
template<class... Ts> using cddddr_t = type_t<cddddr<Ts...>>;

template<class... Ts> static constexpr auto caar_v   = value_v<caar<Ts...>>;
template<class... Ts> static constexpr auto cadr_v   = value_v<cadr<Ts...>>;
template<class... Ts> static constexpr auto cdar_v   = value_v<cdar<Ts...>>;
template<class... Ts> static constexpr auto cddr_v   = value_v<cddr<Ts...>>;
template<class... Ts> static constexpr auto caaar_v  = value_v<caaar<Ts...>>;
template<class... Ts> static constexpr auto cadar_v  = value_v<cadar<Ts...>>;
template<class... Ts> static constexpr auto cdaar_v  = value_v<cdaar<Ts...>>;
template<class... Ts> static constexpr auto cddar_v  = value_v<cddar<Ts...>>;
template<class... Ts> static constexpr auto caadr_v  = value_v<caadr<Ts...>>;
template<class... Ts> static constexpr auto caddr_v  = value_v<caddr<Ts...>>;
template<class... Ts> static constexpr auto cdadr_v  = value_v<cdadr<Ts...>>;
template<class... Ts> static constexpr auto cdddr_v  = value_v<cdddr<Ts...>>;
template<class... Ts> static constexpr auto caaaar_v = value_v<caaaar<Ts...>>;
template<class... Ts> static constexpr auto cadaar_v = value_v<cadaar<Ts...>>;
template<class... Ts> static constexpr auto cdaaar_v = value_v<cdaaar<Ts...>>;
template<class... Ts> static constexpr auto cddaar_v = value_v<cddaar<Ts...>>;
template<class... Ts> static constexpr auto caadar_v = value_v<caadar<Ts...>>;
template<class... Ts> static constexpr auto caddar_v = value_v<caddar<Ts...>>;
template<class... Ts> static constexpr auto cdadar_v = value_v<cdadar<Ts...>>;
template<class... Ts> static constexpr auto cdddar_v = value_v<cdddar<Ts...>>;
template<class... Ts> static constexpr auto caaadr_v = value_v<caaadr<Ts...>>;
template<class... Ts> static constexpr auto cadadr_v = value_v<cadadr<Ts...>>;
template<class... Ts> static constexpr auto cdaadr_v = value_v<cdaadr<Ts...>>;
template<class... Ts> static constexpr auto cddadr_v = value_v<cddadr<Ts...>>;
template<class... Ts> static constexpr auto caaddr_v = value_v<caaddr<Ts...>>;
template<class... Ts> static constexpr auto cadddr_v = value_v<cadddr<Ts...>>;
template<class... Ts> static constexpr auto cdaddr_v = value_v<cdaddr<Ts...>>;
template<class... Ts> static constexpr auto cddddr_v = value_v<cddddr<Ts...>>;

}

using caar   = DTML_FUNC_BOX(strict::caar);
using cadr   = DTML_FUNC_BOX(strict::cadr);
using cdar   = DTML_FUNC_BOX(strict::cdar);
using cddr   = DTML_FUNC_BOX(strict::cddr);
using caaar  = DTML_FUNC_BOX(strict::caaar);
using cadar  = DTML_FUNC_BOX(strict::cadar);
using cdaar  = DTML_FUNC_BOX(strict::cdaar);
using cddar  = DTML_FUNC_BOX(strict::cddar);
using caadr  = DTML_FUNC_BOX(strict::caadr);
using caddr  = DTML_FUNC_BOX(strict::caddr);
using cdadr  = DTML_FUNC_BOX(strict::cdadr);
using cdddr  = DTML_FUNC_BOX(strict::cdddr);
using caaaar = DTML_FUNC_BOX(strict::caaaar);
using cadaar = DTML_FUNC_BOX(strict::cadaar);
using cdaaar = DTML_FUNC_BOX(strict::cdaaar);
using cddaar = DTML_FUNC_BOX(strict::cddaar);
using caadar = DTML_FUNC_BOX(strict::caadar);
using caddar = DTML_FUNC_BOX(strict::caddar);
using cdadar = DTML_FUNC_BOX(strict::cdadar);
using cdddar = DTML_FUNC_BOX(strict::cdddar);
using caaadr = DTML_FUNC_BOX(strict::caaadr);
using cadadr = DTML_FUNC_BOX(strict::cadadr);
using cdaadr = DTML_FUNC_BOX(strict::cdaadr);
using cddadr = DTML_FUNC_BOX(strict::cddadr);
using caaddr = DTML_FUNC_BOX(strict::caaddr);
using cadddr = DTML_FUNC_BOX(strict::cadddr);
using cdaddr = DTML_FUNC_BOX(strict::cdaddr);
using cddddr = DTML_FUNC_BOX(strict::cddddr);

template<class... Ts> using caar_t   = call_t<strict::caar, Ts...>;
template<class... Ts> using cadr_t   = call_t<strict::cadr, Ts...>;
template<class... Ts> using cdar_t   = call_t<strict::cdar, Ts...>;
template<class... Ts> using cddr_t   = call_t<strict::cddr, Ts...>;
template<class... Ts> using caaar_t  = call_t<strict::caaar, Ts...>;
template<class... Ts> using cadar_t  = call_t<strict::cadar, Ts...>;
template<class... Ts> using cdaar_t  = call_t<strict::cdaar, Ts...>;
template<class... Ts> using cddar_t  = call_t<strict::cddar, Ts...>;
template<class... Ts> using caadr_t  = call_t<strict::caadr, Ts...>;
template<class... Ts> using caddr_t  = call_t<strict::caddr, Ts...>;
template<class... Ts> using cdadr_t  = call_t<strict::cdadr, Ts...>;
template<class... Ts> using cdddr_t  = call_t<strict::cdddr, Ts...>;
template<class... Ts> using caaaar_t = call_t<strict::caaaar, Ts...>;
template<class... Ts> using cadaar_t = call_t<strict::cadaar, Ts...>;
template<class... Ts> using cdaaar_t = call_t<strict::cdaaar, Ts...>;
template<class... Ts> using cddaar_t = call_t<strict::cddaar, Ts...>;
template<class... Ts> using caadar_t = call_t<strict::caadar, Ts...>;
template<class... Ts> using caddar_t = call_t<strict::caddar, Ts...>;
template<class... Ts> using cdadar_t = call_t<strict::cdadar, Ts...>;
template<class... Ts> using cdddar_t = call_t<strict::cdddar, Ts...>;
template<class... Ts> using caaadr_t = call_t<strict::caaadr, Ts...>;
template<class... Ts> using cadadr_t = call_t<strict::cadadr, Ts...>;
template<class... Ts> using cdaadr_t = call_t<strict::cdaadr, Ts...>;
template<class... Ts> using cddadr_t = call_t<strict::cddadr, Ts...>;
template<class... Ts> using caaddr_t = call_t<strict::caaddr, Ts...>;
template<class... Ts> using cadddr_t = call_t<strict::cadddr, Ts...>;
template<class... Ts> using cdaddr_t = call_t<strict::cdaddr, Ts...>;
template<class... Ts> using cddddr_t = call_t<strict::cddddr, Ts...>;

template<class... Ts> static constexpr auto caar_v   = call_v<strict::caar, Ts...>;
template<class... Ts> static constexpr auto cadr_v   = call_v<strict::cadr, Ts...>;
template<class... Ts> static constexpr auto cdar_v   = call_v<strict::cdar, Ts...>;
template<class... Ts> static constexpr auto cddr_v   = call_v<strict::cddr, Ts...>;
template<class... Ts> static constexpr auto caaar_v  = call_v<strict::caaar, Ts...>;
template<class... Ts> static constexpr auto cadar_v  = call_v<strict::cadar, Ts...>;
template<class... Ts> static constexpr auto cdaar_v  = call_v<strict::cdaar, Ts...>;
template<class... Ts> static constexpr auto cddar_v  = call_v<strict::cddar, Ts...>;
template<class... Ts> static constexpr auto caadr_v  = call_v<strict::caadr, Ts...>;
template<class... Ts> static constexpr auto caddr_v  = call_v<strict::caddr, Ts...>;
template<class... Ts> static constexpr auto cdadr_v  = call_v<strict::cdadr, Ts...>;
template<class... Ts> static constexpr auto cdddr_v  = call_v<strict::cdddr, Ts...>;
template<class... Ts> static constexpr auto caaaar_v = call_v<strict::caaaar, Ts...>;
template<class... Ts> static constexpr auto cadaar_v = call_v<strict::cadaar, Ts...>;
template<class... Ts> static constexpr auto cdaaar_v = call_v<strict::cdaaar, Ts...>;
template<class... Ts> static constexpr auto cddaar_v = call_v<strict::cddaar, Ts...>;
template<class... Ts> static constexpr auto caadar_v = call_v<strict::caadar, Ts...>;
template<class... Ts> static constexpr auto caddar_v = call_v<strict::caddar, Ts...>;
template<class... Ts> static constexpr auto cdadar_v = call_v<strict::cdadar, Ts...>;
template<class... Ts> static constexpr auto cdddar_v = call_v<strict::cdddar, Ts...>;
template<class... Ts> static constexpr auto caaadr_v = call_v<strict::caaadr, Ts...>;
template<class... Ts> static constexpr auto cadadr_v = call_v<strict::cadadr, Ts...>;
template<class... Ts> static constexpr auto cdaadr_v = call_v<strict::cdaadr, Ts...>;
template<class... Ts> static constexpr auto cddadr_v = call_v<strict::cddadr, Ts...>;
template<class... Ts> static constexpr auto caaddr_v = call_v<strict::caaddr, Ts...>;
template<class... Ts> static constexpr auto cadddr_v = call_v<strict::cadddr, Ts...>;
template<class... Ts> static constexpr auto cdaddr_v = call_v<strict::cdaddr, Ts...>;
template<class... Ts> static constexpr auto cddddr_v = call_v<strict::cddddr, Ts...>;


// TODO: we can probably postulate 0 $ ..., $ ∈ {*, &}, equal to zero, and even more so 0 / ...
// (and likewise with ... | -1 | ...)
#define DTML_BASIC_ARITHMETIC(name, sign, zero)									\
LAZY_LIST_FUNCTIONAL(name) {											\
	template<class... Vs> struct unbox<name<q<Vs...>>>: box<value_box<(... sign value_v<Vs>)>> {};	\
														\
	template<class V> struct unbox<name<q<V>>>: box<value_box<(zero sign value_v<V>)>> {};		\
														\
	template<> struct unbox<name<nil>>: box<value_box<zero>> {};						\
}

DTML_BASIC_ARITHMETIC(plus,       + , untyped_zero);
DTML_BASIC_ARITHMETIC(minus,      - , untyped_zero);
DTML_BASIC_ARITHMETIC(multiplies, * , untyped_one);
DTML_BASIC_ARITHMETIC(divides,    / , untyped_one);
DTML_BASIC_ARITHMETIC(modulus,    % , untyped_zero);
DTML_BASIC_ARITHMETIC(negate,     - , untyped_zero);
DTML_BASIC_ARITHMETIC(bit_and,    & , untyped_constant<-1>{});
DTML_BASIC_ARITHMETIC(bit_or,     | , untyped_zero);
DTML_BASIC_ARITHMETIC(bit_xor,    ^ , untyped_zero);
DTML_BASIC_ARITHMETIC(bit_not,    ^ , untyped_constant<-1>{});

#undef DTML_BASIC_ARITHMETIC


LAZY_LIST_FUNCTIONAL(equal_to) {
	template<LongerThan<2> L> struct unbox<equal_to<L>>: box<false_value> {};

	template<LongerThan<2> L> requires (car_v<L> == cadr_v<L>) struct unbox<equal_to<L>>: call<equal_to, cdr<L>> {};

	template<class... Ts> requires (sizeof...(Ts) < 2) struct unbox<equal_to<q<Ts...>>>: box<true_value> {};
}


// Folds.
LAZY_LIST_SCALAR(left_accumulate) {
	template<LongerThan<3> L> struct unbox<left_accumulate<L>>:
		call<left_accumulate, car_t<L>, cons<invoke_t<car_t<L>, cadr_t<L>, caddr_t<L>>, cdddr<L>>> {};

	template<ExactSize<2> L> struct unbox<left_accumulate<L>>: call<cadr, L> {};
}


LAZY_LIST_SCALAR(right_accumulate) {
	template<LongerThan<3> L> struct unbox<right_accumulate<L>>: invoke<car_t<L>, cadr_t<L>, call<right_accumulate, cons<car_t<L>, cddr<L>>>> {};

	template<ExactSize<2> L> struct unbox<right_accumulate<L>>: call<cadr, L> {};
}


LAZY_LIST_SCALAR(left_accumulate2) {
	template<LongerThan<4> L> struct unbox<left_accumulate2<L>>:
		call<left_accumulate2, car_t<L>, cons<invoke_t<car_t<L>, cadr<L>, caddr<L>, cadddr<L>>, cdddr<L>>> {};

	template<Between<2, 4> L> struct unbox<left_accumulate2<L>>: call<cadr, L> {};
}


LAZY_LIST_SCALAR(right_accumulate2) {
	template<LongerThan<4> L> struct unbox<right_accumulate2<L>>:
		invoke<car_t<L>, cadr_t<L>, caddr_t<L>, call<right_accumulate2, car_t<L>, cddr<L>>> {};

	template<ExactSize<3> L> struct unbox<right_accumulate2<L>>: call<caddr, L> {};

	template<ExactSize<2> L> struct unbox<right_accumulate2<L>>: call<cadr, L> {};
}



#define STL_ORDERED(name, sign, inverse, su_bool_op, us_bool_op)				\
LAZY_LIST_FUNCTIONAL(name) {									\
	template<LongerThan<2> L> class unbox<name<L>> {					\
		template<auto v1, auto v2> struct cmp: call<cond,				\
			  	bool_value<(std::is_signed_v<decltype(v1)> && std::is_unsigned_v<decltype(v2)>)>, bool_value<(v1 sign 0 su_bool_op v1 sign v2)>,\
			  	bool_value<(std::is_unsigned_v<decltype(v1)> && std::is_signed_v<decltype(v2)>)>, bool_value<(v2 inverse 0 us_bool_op v1 sign v2)>,\
				bool_value<(v1 sign v2)>> {};					\
	public:											\
		using type = call<logical_and, cmp<car_v<L>, cadr_v<L>>, call<name, cdr<L>>>;	\
	};											\
												\
	template<ShorterThan<2> L> struct unbox<name<L>>: box<true_value> {};			\
}

STL_ORDERED(less         , < , > , ||, &&);
STL_ORDERED(less_equal   , <=, >=, ||, &&);
STL_ORDERED(greater      , > , < , &&, ||);
STL_ORDERED(greater_equal, >=, <=, &&, ||);

#undef STL_ORDERED

LAZY_LIST_FUNCTIONAL(not_equal_to) {
	template<LongerThan<2> L> struct unbox<not_equal_to<L>>: call<logical_or, bool_value<(car_v<L> != cadr_v<L>)>, call<not_equal_to, cdr<L>>> {};

	template<ShorterThan<2> L> struct unbox<not_equal_to<L>>: box<false_value> {};
}


LAZY_VALUE_TEMPLATE(dec) {
	template<class... Vs> struct unbox<dec<Vs...>>: call<minus, car<Vs...>, one_value> {};
}


LAZY_VALUE_TEMPLATE(inc) {
	template<class... Vs> struct unbox<inc<Vs...>>: call<plus, car<Vs...>, one_value> {};
}


LAZY_LIST_FUNCTIONAL(zero) {
	template<LongerThan<1> L> struct unbox<zero<L>>: call<logical_and, equal_to<car<L>, zero_value>, call<zero, cdr<L>>> {};

	template<> struct zero<nil>: box<true_value> {};
}


LAZY_LIST_FUNCTIONAL(positive) {
	template<LongerThan<1> L> struct unbox<positive<L>>: call<logical_and, greater<car<L>, zero_value>, call<positive, cdr<L>>> {};

	template<> struct positive<nil>: box<true_value> {};
}


LAZY_LIST_FUNCTIONAL(negative) {
	template<LongerThan<1> L> struct unbox<negative<L>>: call<logical_and, less<car<L>, zero_value>, call<negative, cdr<L>>> {};

	template<> struct negative<nil>: box<true_value> {};
}


LAZY_LIST_FUNCTIONAL(non_negative) {
	template<LongerThan<1> L> struct unbox<non_negative<L>>: call<logical_and, greater_equal<car<L>, zero_value>, call<non_negative, cdr<L>>> {};

	template<> struct non_negative<nil>: box<true_value> {};
}


LAZY_LIST_FUNCTIONAL(non_positive) {
	template<LongerThan<1> L> struct unbox<non_positive<L>>: call<logical_and, less_equal<car<L>, zero_value>, call<non_positive, cdr<L>>> {};

	template<> struct non_positive<nil>: box<true_value> {};
}


template<class... Ts> concept Container = empty_v<Ts...> || Iterable<Ts...>;

template<class... Ts> using container = bool_value<Container<Ts...>>;

template<class... Ts> inline constexpr auto container_v = value_v<container<Ts...>>;


LAZY_LIST_SCALAR(length) {
	template<LongerThan<1> L> struct unbox<length<L>>: call<inc, length<cdr<L>>> {};

	template<ExactSize<0> L> struct unbox<length<L>>: box<size_value<0>> {};

	template<class... Ts> struct unbox<length<q<Ts...>>>: box<size_value<sizeof...(Ts)>> {};

	template<> struct unbox<length<nil>>: box<size_value<0>> {};
}


LAZY_TEMPLATE(append) {
	template<class T, class... Ts> struct unbox<append<T, Ts...>>: call<concat, Ts..., q<T>> {};

	template<class... Ts> struct short_circuiting<append<Ts...>, empty>: true_value {};
	template<class... Ts> struct unbox<empty<append<Ts...>>>: box<false_value> {};

	template<class... Ts> struct short_circuiting<append<Ts...>, car>: true_value {};
	template<class T, class... Ts> struct unbox<car<append<T, Ts...>>>: call<cond,
		empty<Ts...>, T,
		car<Ts...>> {};

	template<class... Ts> struct short_circuiting<append<Ts...>, cdr>: true_value {};
	template<class T, class... Ts> struct unbox<cdr<append<T, Ts...>>>: call<cond,
		empty<Ts...>, nil,
		append_t<cdr<Ts...>, T>> {};
}


LAZY_LIST_SCALAR(last) {
	template<LongerThan<2> L> struct unbox<last<L>>: call<last, cdr<L>> {};

	template<ExactSize<1> L> struct unbox<last<L>>: call<car, L> {};

	template<ExactSize<0> L> struct unbox<last<L>> { DTML_THROW(L, "last: empty list"); };
}


LAZY_LIST_OPERATOR(init) {
	template<LongerThan<2> L> struct unbox<init<L>>: call<cons, car<L>, call<init, cdr<L>>> {};

	template<ExactSize<1> L> struct unbox<init<L>>: box<nil> {};

	template<ExactSize<0> L> struct unbox<init<L>> { DTML_THROW(L, "init: empty list"); };


	template<class... Ts> struct short_circuiting<init<Ts...>, finite>: true_value {};
	template<class... Ts> struct finite<init<Ts...>>: call<finite, Ts...> {};

	template<class... Ts> struct short_circuiting<init<Ts...>, infinite>: true_value {};
	template<class... Ts> struct infinite<init<Ts...>>: call<infinite, Ts...> {};

	template<class... Ts> struct short_circuiting<init<Ts...>, car>: true_value {};
	template<class... Ts> struct car<init<Ts...>>: call<car, Ts...> {};

	template<class... Ts> struct short_circuiting<init<Ts...>, cdr>: true_value {};
	template<class... Ts> struct cdr<init<Ts...>>: call<init, cdr<Ts...>> {};

	template<class... Ts> struct short_circuiting<init<Ts...>, length>: true_value {};
	template<class... Ts> struct length<init<Ts...>>: call<dec, length<Ts...>> { static_assert(length_v<Ts...> > 0, "init: empty list"); };
}


LAZY_LIST_FUNCTIONAL(logical_xor) {
	template<LongerThan<3> L> struct unbox<logical_xor<L>>: call<left_accumulate, dtml::logical_xor, L> {};

	template<ExactSize<2> L> struct unbox<logical_xor<L>>: call<not_equal_to, to_bool<car<L>>, to_bool<cadr<L>>> {};

	template<ExactSize<1> L> struct unbox<logical_xor<L>>: call<car, L> {};

	template<ExactSize<0> L> struct unbox<logical_xor<L>>: box<false_value> {};
}


LAZY_LIST_FUNCTIONAL(max) {
	template<LongerThan<3> L> struct unbox<max<L>>: call<left_accumulate, dtml::max, L> {};

	template<ExactSize<2> L> struct unbox<max<L>>: call<cond, greater_equal<car<L>, cadr<L>>, car<L>, cadr<L>> {};

	template<ExactSize<1> L> struct unbox<max<L>>: call<car, L> {};
}


LAZY_LIST_FUNCTIONAL(min) {
	template<LongerThan<3> L> struct unbox<min<L>>: call<left_accumulate, dtml::min, L> {};

	template<ExactSize<2> L> struct unbox<min<L>>: call<cond, less_equal<car<L>, cadr<L>>, car<L>, cadr<L>> {};

	template<ExactSize<1> L> struct unbox<min<L>>: call<car, L> {};
}


LAZY_LIST_SCALAR(apply) {
	template<class F, class... Args> struct unbox<apply<q<F, Args...>>>: invoke<F, Args...> {};
}


template<std::size_t i> struct arg: size_value<i> {};

template<std::size_t i> struct self_referential<arg<i>>: true_value {};

template<std::size_t i> struct has_type<arg<i>>: false_value {};


template<class... Ts> using is_arg = std::is_same<root_t<Ts...>, DTML_FUNC_BOX(arg)>;

template<class... Ts> inline constexpr auto is_arg_v = value_v<is_arg<Ts...>>;


LAZY_LIST_OPERATOR(compose) {
	template<HasFunc Root, HasFunc... Branches> struct unbox<compose<q<Root, Branches...>>> {
		struct type {
			template<class... Ts> using func = invoke<Root, invoke<Branches, Ts...>...>;
		};
	};
}


LAZY_LIST_OPERATOR(until) {
	template<LongerThan<2> L> struct unbox<until<L>>: call<cond,
		apply<car<L>, cddr<L>>, call<cddr, L>,
		call<until, car<L>, cadr<L>, apply<cadr<L>, cddr<L>>>> {};
}


LAZY_TEMPLATE(maybe) {
	template<class Default, HasFunc F, class... Ts> requires HasType<Ts...> struct unbox<maybe<Default, F, Ts...>>: invoke<F, Ts...> {};

	template<class Default, class F, class... Ts> requires (!HasType<Ts...>) struct unbox<maybe<Default, F, Ts...>>: box<Default> {};

	DTML_STRICT_NON_UNARY(maybe);
}


LAZY_TEMPLATE(fmap) {
	template<class... Ts> struct unbox<fmap<Ts...>>: call<maybe, null, Ts...> {};

	DTML_STRICT_NON_UNARY(fmap);
}


LAZY_LIST_SCALAR(list_to_maybe) {
	template<LongerThan<1> L> struct unbox<list_to_maybe<L>>: box<escape<car<L>>> {};

	template<ExactSize<0> L> struct unbox<list_to_maybe<L>>: box<null> {};
}


LAZY_TEMPLATE(maybe_to_list) {
	template<class... Ts> struct unbox<maybe_to_list<Ts...>>: call<maybe, nil, quote, Ts...> {};
}


LAZY_LIST_SCALAR(when) {
	template<LongerThan<2> L> struct unbox<when<L>>: box<null> {};
	
	template<LongerThan<2> L> requires car_v<L> struct unbox<when<L>>: box<escape<cadr<L>>> {};
}


LAZY_LIST_SCALAR(power) {
	template<ExactSize<2> L> struct unbox<power<L>>: call<get_func, cond<
		equal_to<car<L>, one_value>, cadr<L>,
		positive<car<L>>, compose<cadr<L>, power<dec<car<L>>, cadr<L>>>,
		identity>> {};

	template<class V, HasFunc F> requires (value_v<V> == 1) struct unbox<power<q<V, F>>>: box<F> {};
}


LAZY_LIST_FUNCTIONAL(almost_same) {
	namespace almost_same_ {
	template<class A, class B, class Accum> struct f: make_lazy<call<logical_and, 
		logical_or<std::is_same<A, B>,
		           logical_and<has_value<A>,
		                       has_value<B>,
		                       equal_to<A, B>>>,
		Accum>> {};

	template<template<class...> class F, class... Ts, class... Us, class Accum> requires (sizeof...(Us) == sizeof...(Ts)) struct f<F<Ts...>, F<Us...>, Accum>
		: make_lazy<call<logical_and, f<Ts, Us, Accum>...>> {};
	}

	template<LongerThan<2> L> struct unbox<almost_same<L>>: call<right_accumulate2, DTML_FUNC_BOX(almost_same_::f), L, true_value> {};

	template<ShorterThan<2> L> struct unbox<almost_same<L>>: box<true_value> {};
}


LAZY_LIST_SCALAR(find_if) {
	template<LongerThan<2> L> struct unbox<find_if<L>>: call<cond,
		invoke<car<L>, cadr<L>>, cdr<L>,
		find_if<cons<car<L>, cddr<L>>>> {};

	template<ShorterThan<2> L> struct unbox<find_if<L>>: box<nil> {};
}


LAZY_LIST_SCALAR(find) {
	template<LongerThan<2> L> struct unbox<find<L>>: call<logical_or, almost_same<q<car<L>, cadr<L>>>, call<find, cons<car_t<L>, cddr<L>>>> {};

	template<ShorterThan<2> L> struct unbox<find<L>>: box<false_value> {};
}

}

#endif
