#ifndef DTML__THUNK_H_ff56452e16cb8bda5d2badb471775dd9eda8d5df
#define DTML__THUNK_H_ff56452e16cb8bda5d2badb471775dd9eda8d5df

#include <type_traits>

#include "basic.h"
#include "box.h"

namespace dtml {

template<class T, template<class...> class... Chain> struct thunk {};


template<class... Ts> struct delayed;

template<class... Ts> inline constexpr auto delayed_v = delayed<Ts...>::value;

template<class... Ts> struct delayed: bool_value<(delayed_v<Ts> || ...)> {};

template<class T> struct delayed<T>: false_value {};

template<HasType T> struct delayed<T>: delayed<typename T::type> {};

template<template<class...> class AST, class... Branches> struct delayed<AST<Branches...>>: delayed<Branches...> {};

template<class T, template<class...> class... Chain> struct delayed<thunk<T, Chain...>>: true_value {};


template<class... Ts> using strict = bool_value<!(delayed_v<Ts> || ...)>;

template<class... Ts> static constexpr auto strict_v = strict<Ts...>::value;


struct force {
	template<class... Us> struct call: konst<Us...> {};
	template<class... Us> using call_t = type_t<call<Us...>>;
	template<class... Us> static constexpr auto call_v = value_v<call<Us...>>;
	template<class T> struct call<T>: cond<
		has_type<T>, call<typename T::type>,
		konst<T>> {};
	template<class T, template<class...> class Head, template<class...> class... Chain> struct call<thunk<T, Head, Chain...>>: call<thunk<Head<unbox_t<T>>, Chain...>> {};
	template<class... Us, template<class...> class Head, template<class...> class... Chain> struct call<thunk<values<Us...>, Head, Chain...>>: call<thunk<Head<unbox_t<Us>...>, Chain...>> {};
	template<class T> struct call<thunk<T>>: konst<T> {};
	template<template<class...> class AST, class... Branches> struct call<AST<Branches...>>: AST<unbox_t<Branches>...> {};
};


template<class... Ts> requires delayed_v<Ts...> struct get_type<Ts...> { using type = force::call_t<Ts...>; };

template<class... Ts> requires delayed_v<Ts...> struct get_value<Ts...> { static constexpr auto value = force::call_v<Ts...>; };


template<template<class...> class F, class... Ts> struct chain: konst<thunk<values<Ts...>, F>> {};

template<template<class...> class F, class... Ts> using chain_t = typename chain<F, Ts...>::type;

template<template<class...> class F, class T, template<class...> class... Chain> struct chain<F, thunk<T, Chain...>>: konst<thunk<T, Chain..., F>> {};


template<template<class...> class F, class... Ts> requires delayed_v<Ts...> struct call<F, Ts...>: chain<F, Ts...> {};

}

#endif
