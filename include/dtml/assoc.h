#ifndef DTML__ASSOC_H_ed606e3b254fdc7c1e6be85ce74a54f0977d4f64
#define DTML__ASSOC_H_ed606e3b254fdc7c1e6be85ce74a54f0977d4f64

/**
 * This library introduces a naive key-value storage, mostly a list of pairs (inspired by Scheme's `assoc` standard function), with possibly a default element for failed lookups.
 */

#include "basic.h"

namespace dtml {

/**
 * Add a key-val pair to assoc list, update the value if key is found on the list.
 *
 * Note: technically, this function works even if its list argument is not truly assoc (e.g. has duplicates or non-assoc structure past the element with the key updated).
 *
 * Note 2: both calls are possible, `insert<Key, Val, Map>` and `using KeyVal = q<Key, Val>; insert<KeyVal, Map>`.
 *
 * @param car<L> a key to insert or update
 * @param cadr<L> associated value
 * @param cddr<L> an assoc list
 * @return the updated list
 */
LAZY_LIST_OPERATOR(insert) {
	template<LongerThan<3> L> class unbox<insert<L>> {
		using Key = car_t<L>;
		using Val = cadr_t<L>;
		using KeyVal = q<Key, Val>;
		using Map = cddr<L>;
		using FirstItem = car_t<Map>;
		using RestItems = cdr<Map>;
	public:
		using type = call<cond,
		      // Map's head is a singleton list, which means it's a default element
		      exact_size<one_size, FirstItem>, call<cons, KeyVal, Map>,
		      // Map's head's key matches Key, mapped value should be updated
		      call<almost_same, car<FirstItem>, Key>, call<cons, KeyVal, RestItems>,
		      // KeyVal is to be inserted somewhere in Map's tail
		      call<cons, FirstItem, call<insert, KeyVal, RestItems>>>;
	};

	// only two arguments given, which means Map is empty, so KeyVal should be inserted right at this point
	template<ExactSize<2> L> struct unbox<insert<L>>: box<q<L>> {};
}


/**
 * Update or create the default value in an assoc list
 *
 * Note: technically, this function works even if its list argument is not truly assoc (e.g. has duplicates or non-assoc structure past the default updated).
 *
 * @param car<L> new default value for failed lookups
 * @param cdr<L> an assoc list
 * @return the updated list
 */
LAZY_LIST_OPERATOR(insert_default) {
	template<LongerThan<2> L> class unbox<insert_default<L>> {
		using Default = car_t<L>;
		using Map = cdr<L>;
		using FirstItem = car_t<Map>;
		using RestItems = cdr<Map>;
	public:
		using type = call<cond,
		      // Map's head is a singleton list, which means it's a default element to be updated
		      exact_size<one_size, FirstItem>, call<cons, q<Default>, RestItems>,
		      // Default is to be inserted somewhere in Map's tail
		      call<cons, FirstItem, call<insert_default, cons<Default, RestItems>>>>;
	};

	// only one argument given, which means Map is empty, so Default should be inserted right at this point
	template<ExactSize<1> L> struct unbox<insert_default<L>>: box<L> {};
}


/**
 * The superposition of the above: add/update a key-val pair, if the first argument is a pair (`q<Key, Val>`), or add/update default otherwise.
 *
 * Note: a key-val item should be quoted twice (`insert_item_t<q<q<Key, Val>>, map>`) or be the head of the arg list (`insert_item_t<cons_t<q<Key, Val>, map>>`).
 *
 * @param car<L> either a key-value association, or a new default element
 * @param cdr<L> an assoc list
 * @return the updated list
 */
LAZY_LIST_OPERATOR(insert_item) {
	template<LongerThan<1> L> struct unbox<insert_item<L>>: call<insert_default, L> {};

	template<LongerThan<1> L> requires ExactSize<car_t<L>, 2> struct unbox<insert_item<L>>: call<insert, car<L>, cdr<L>> {};
};


/**
 * assoc list
 *
 * Accepts a flat list and splits it into pairs, so that `<Key1, Val1, Key2, Val2, ...>` becomes `<<Key1, Val1>, <Key2, Val2>, ...>`.
 *
 * If the list is finite and has odd number of elements, the last element becomes the 'default' value that is returned (inside a singleton suffix) for unknown keys.
 *
 * @param L list of keys and values, interspersed
 * @return L split into pairs
 */
LAZY_LIST_OPERATOR(unordered_map) {
	template<LongerThan<2> L> struct unbox<unordered_map<L>>: call<insert,
		q<car_t<L>, cadr_t<L>>,	// key-val pair
		call<unordered_map, cddr<L>>> {};

	template<ExactSize<1> Default> struct unbox<unordered_map<Default>>: call<q, Default> {};

	template<ExactSize<0> Default> struct unbox<unordered_map<Default>>: box<nil> {};
}


/**
 * Accepts a key Key and a list of key-val pairs `<<Key1, Val1>, ...>`, and returns the suffix of list starting with `<Key, Val>`.
 *
 * If an element of the list is a singleton (a list of one element), it is regarded as default and the suffix starting with it is returned unconditionally, provided no key to the left of it matches.
 *
 * Note: technically, this function works even if its list argument is not truly assoc (e.g. has duplicates or non-assoc structure past the element with the key queried).
 *
 * @param car<L> index to lookup in the assoc list
 * @param cdr<L> assoc list of pairs
 * @return suffix of cdr<L> such that the key in its first element is Idx (nil if not found)
 */
LAZY_LIST_OPERATOR(assoc) {
	template<LongerThan<2> L> struct unbox<assoc<L>>: call<assoc, cons<car_t<L>, cddr<L>>> {};

	template<LongerThan<2> L> requires (LongerThan<cadr_t<L>, 2> && almost_same_v<q<car<L>, caadr<L>>>) struct unbox<assoc<L>>: call<cdr, L> {};

	template<LongerThan<2> L> requires ExactSize<cadr_t<L>, 1> struct unbox<assoc<L>>: call<cdr, L> {};

	template<ExactSize<1> L> struct unbox<assoc<L>>: box<nil> {};
}


/**
 * Boolean predicate whether an assoc list contains a key.
 *
 * Note: technically, this function works even if its list argument is not truly assoc (e.g. has duplicates or non-assoc structure past the element with the key queried).
 *
 * @param car<L> index to lookup in the assoc list
 * @param cdr<L> assoc list of pairs
 * @return true_value iff this list contains the key
 */
LAZY_LIST_SCALAR(contains) {
	template<LongerThan<1> L> requires empty_v<assoc<L>> struct unbox<contains<L>>: box<false_value> {};

	template<LongerThan<1> L> requires (!empty_v<assoc<L>>) struct unbox<contains<L>>: call<longer_than, size_value<2>, car_t<assoc<L>>> {};
};


/**
 * Accepts an index Idx and a list of key-val pairs <<Key1, Val1>, ...>, and returns the value associated with Idx in the list.
 *
 * If an element of the list is a singleton (a list of one element), it is regarded as default and is returned unconditionally, provided no key to the left of it matches.
 *
 * Note: technically, this function works even if its list argument is not true-assoc (e.g. has duplicates or non-assoc structure past the element with the key queried).
 *
 * @param car<L> index to lookup in the assoc list
 * @param cdr<L> assoc list of pairs
 * @return suffix of cdr<L> such that the key in its first element is Idx (null if not found)
 */
LAZY_LIST_SCALAR(lookup) {
	namespace lookup_ {
	template<class Assoc> struct F: call<cadr, Assoc> {};
	template<ExactSize<1> Assoc> struct F<Assoc>: call<car, Assoc> {};
	}

	template<LongerThan<0> L> struct unbox<lookup<L>> {
		using type = call<fmap, DTML_FUNC_BOX(lookup_::F), list_to_maybe_t<assoc_t<L>>>;
	};
}


/**
 * Merge assoc maps, eliminating duplicates.
 *
 * The associations appear in the final result in first-come order: each key is associated with its leftmost value in simply concatenated maps, and default value, if any, is the leftmost one.
 *
 * @param L assoc lists
 * @return all the lists concatenated, with duplicates removed
 */
LAZY_LIST_OPERATOR(map_union) {
	template<LongerThan<1> L> struct unbox<map_union<L>>: call<insert_item, cons<car<L>, call<map_union, cdr<L>>>> {};

	template<ExactSize<0> L> struct unbox<map_union<L>>: box<nil> {};
}


/**
 * Check maps for equality.
 *
 * Two maps M1 and M2 are equal iff for every key-value association [K1, V1] in M1 there's an association [K2, V2] in M2 such that K2 is almost same as K1, and V2 is almost same as V1,
 * and either both maps don't have defaults, or they both have defaults that are almost same.
 *
 * Note: it is not checked whether the two lists both have assoc structure, but chances are, if they don't, they are not equal.
 *
 * @param L assoc list
 * @param R assoc list
 * @return whether L and R are equal
 */
LAZY_VALUE_TEMPLATE(map_equal_to) {
	namespace map_equal_to_ {
	template<class L> struct contains {
		template<class Item, class Accum, class...> using func = call<logical_and, find<cons<Item, L>>, Accum>;
	};
	}

	template<class L, class R> struct unbox<map_equal_to<L, R>>: call<logical_and,
		right_accumulate<map_equal_to_::contains<L>, R, true_value>,
		right_accumulate<map_equal_to_::contains<R>, L, true_value>> {};
}

}

#endif
