#ifndef DTML__LET_H_9594ed826b93258fd567cac8afe28fba78701980
#define DTML__LET_H_9594ed826b93258fd567cac8afe28fba78701980

#include <type_traits>

#include "assoc.h"
#include "basic.h"
#include "box.h"

/**
 * This library introduces let binding and pattern matching/decomposition mechanism for C++ types.
 * 
 * In brief, a let expression consists of a sequence of pattern-value pairs, that define bindings for variables, and an expression to be evaluated,
 * that may contain free variables that can be bound by matching against values. When all the patterns from bindings match their respective values, let expression returns the evaluated expression,
 * with free variables replaced with their respective bindings.
 * 
 * Few examples below illustrate this (note that `dtml::` qualification is omitted in DTML identifiers, as if by `using namespace dtml;` beforehand).
 * 
 * 1. A pattern may simply be a literal (i.e. a plain C++ type), in which case it matches itself, or an equivalent type, in the sense of [`almost_same`](basic.md#almostsame).
 *    ```cpp
 *    using String = std::string;
 * 
 *    using CharList = let_t<       // let invocation
 *            // pattern , value
 *            std::string, String,
 *            // evaluated expression
 *            std::list<char>>;
 * 
 *    static_assert(std::is_same_v<CharList, std::list<char>>);
 * 
 *    // using NoCharList = let_t<
 *    //         std::list<char>, String,  // different types — no match
 *    //         std::list<char>>;         // cannot be evaluated
 *    ```
 * 2. A pattern may be a variable, that is, `dtml::var<n>`.
 *    ```cpp
 *    using CharList = let_t<        // let invocation
 *            // pattern, value
 *            var<'c'>  , char,      // var<'c'> is bound to char
 *            // evaluated expression
 *            std::list<var<'c'>>>;
 * 
 *    static_assert(std::is_same_v<CharList, std::list<char>>);
 *    ```
 *    Of course, to use string literals as variable identifiers, you'd need to wrap them in a literal type, say, [`dtml::string`](string.md):
 *    ```cpp
 *    using CharList = let_t<                   // let invocation
 *            // pattern, value
 *            var<string{"char"}>, char,
 *            // evaluated expression
 *            std::list<var<string{"char"}>>>;
 * 
 *    static_assert(std::is_same_v<CharList, std::list<char>>);
 *    ```
 * 3. Another irrefutable pattern is `dtml::ignore` (in languages less verbose it is often called simply `_`). Like `std::ignore`, it matches any value (and creates no bindings):
 *    ```cpp
 *    using String = std::string;
 * 
 *    using CharList = let_t<
 *            ignore, String,    // matches irrefutably
 *            std::list<char>>;  // and creates no bindings
 * 
 *    static_assert(std::is_same_v<CharList, std::list<char>>);
 *    ```
 * 4. A pattern may be a template instantiation whose parameters are also patterns. It matches an instantiation of the same template when all parameter patterns match the actual type's parameters:
 *    ```cpp
 *    using String = std::string;  // std::basic_string<char>
 * 
 *    using CharList = let_t<                       // let invocation
 *            std::basic_string<var<'c'>>, String,  // pattern, value — var<'c'> is bound to char, first of String's parameters
 *            std::list<var<'c'>>>;                 // evaluated expression
 * 
 *    static_assert(std::is_same_v<CharList, std::list<char>>);
 *    ```
 *    The same variable can be bound several times in the same let-expression. In such a case, all the bindings must be identical, in the sense of [`almost_same`](basic.md#almostsame).
 *    (Note that it actually implicitly happens in the example above, as `std::basic_string` has parameter `Traits` defaulting to `std::char_traits`,
 *    so `std::char_traits<char>` in `String` is matched implicitly against `std::char_traits<var<'c'>>` in the pattern.)
 *    ```cpp
 *    using Point2D = std::pair<double, double>;
 * 
 *    using AnnotatedValue = std::pair<double, std::string>;
 * 
 *    using Real = let_t<
 *            std::pair<var<'R'>, var<'R'>>, Point2D,  // both times the variable matches the same type
 *            var<'R'>>;
 * 
 *    static_assert(std::is_same_v<Real, double>);
 * 
 *    // using Real = let_t<
 *    //         std::pair<var<'R'>, var<'R'>>, AnnotatedValue,  // two different values for the same variable, no match
 *    //         var<'R'>>;
 *    ```
 *    Mind the implicit bindings, like the one described above. For instance, this binding should fail:
 *    ```cpp
 *    using DamagedString = std::basic_string<char, std::char_traits<char>, std::allocator<std::char16_t>>;  // a strange instance
 *
 *    // using DamagedCharList = let_t<
 *    //         std::basic_string<var<'c'>>, DamagedString,  // error — no match, var<'c'> is bound to char and thus std::allocator<var<'c'>> does not match std::allocator<std::char16_t>
 *    //         std::list<var<'c'>>>;
 *    ```
 *    One special pattern is an instantiation of [`dtml::invoke`](basic.md). It matches any type which is an instantiation of a template with an appropriate number of arguments,
 *    and its first argument can as well be a pattern, matching the corresponding (boxed) template:
 *    ```cpp
 *    using String = std::string;
 *    using IntList = std::list<int>;
 * 
 *    using CharList = let_t<
 *            std::basic_string<var<'c'>>,      String,   // var<'c'> is bound to char
 *            invoke<var<'l'>, ignore, ignore>, IntList,  // var<'l'> is bound to (boxed) std::list
 *            invoke<var<'l'>, var<'c'>>>;                // one bound argument invoked on another
 * 
 *    static_assert(std::is_same_v<CharList, std::list<char>>);
 *    ```
 *    Note how several pattern-value matches can be combined in the same let-expression.
 * 5. One special, var-like, type of patterns is called `dtml::seq`. It is different from `dtml::var` in that it is designed specifically to match type parameter packs, or suffixes thereof.
 *    When matches, it is bound to [`dtml::q`](box.h) of actual parameters.
 *    ```cpp
 *    using Tuple = DTML_FUNC_BOX(std::tuple);  // boxed std::tuple
 * 
 *    using MyTuple = std::tuple<int, double, char, std::string>;
 * 
 *    using TupleSuffix = let_t<
 *            std::tuple<ignore, ignore, seq<'S'>>, MyTuple,  // seq<'S'> is bound to q<char, std::string>
 *            apply<Tuple, seq<'S'>>>;                        // construct tuple with seq<'S'> elements as arguments
 * 
 *    static_assert(std::is_same_v<TupleSuffix, std::tuple<char, std::string>>);
 *    ```
 *    To ignore the whole (sub)sequence, use `ignore{}` as seq's parameter (ignore is a literal type):
 *    ```cpp
 *    using MyTuple = std::tuple<int, double, char, std::string>;
 * 
 *    using TuplePrefix = let_t<
 *            std::tuple<var<0>, var<1>, seq<ignore{}>>, MyTuple,
 *            std::tuple<var<0>, var<1>>>;
 * 
 *    static_assert(std::is_same_v<TuplePrefix, std::tuple<int, double>>);
 *    ```
 * 6. vars and seqs can be used in value parts of bindings same as in expression. They are not evaluated until the expression itself is.
 *    ```cpp
 *    using String = std::string;
 * 
 *    using CharList = let_t<
 *            std::basic_string<var<'c'>>, String,  // var<'c'> is bound to char
 *            var<'l'>, std::list<var<'c'>>,        // var<'l'> is bount to std::list<char>
 *            var<'l'>>;
 * 
 *    static_assert(std::is_same_v<CharList, std::list<char>>);
 *
 * ## TODO
 * Currently, `let` is abundantly strict (e.g. all the bindings are parsed and forced before evaluating the expression). This might change in the future. For instance,
 * ```cpp
 * using CharList = let_t<
 *         var<0>, char,
 *         std::pair<var<string{"ambiguous var"}>, var<string{"ambiguous var"}>>, std::pair<int, double>,  // no strict match
 *         std::list<var<0>>>;                                                                             // unambiguously evaluated, even though var<string{"ambiguous var"}> is unbindable
 * ```
 * could yield a concrete type even though not all patterns can be matched against all values.
 *
 * This let is non-recursive. `letrec` is to be implemented somewhere in the future.
 *
 * Pattern packs, possibly `seq`-ended, can only be bound to real template parameter packs.
 * It might prove useful, and not very hard to implement, to extend their area of definition on arbitrary DTML lists, especially infinite ones.
 */

namespace dtml {

/**
 * Irrefutable wildcard pattern.
 */
struct ignore {};


/**
 * Variable pattern.
 *
 * When matched successfully, variable becomes bound to the respective value that is to be substituted for all occurences of this variable in resulting expression.
 *
 * When unique across all binding pairs, this pattern is irrefutable and becomes bound to the corresponding value.
 *
 * When two or more entries of var<n> with the same n are found in patterns they should correspond to the same type in values, otherwise there's no match.
 */
template<auto n> struct var {};


/**
 * Sequence pattern.
 *
 * This pattern is used as the rightmost template parameter and matches the rest of the pack, not consumed by its siblings to the left. Think of it as the last parameter in a parameter pack,
 * the one with elipsis:
 *
 * ```cpp
 * template<class T> struct RebindToIntDouble;
 * template<template<class...> class F, class A, class B, class... Rest> struct RebindToIntDouble<F<A, B, Rest...>> {
 *     using type = std::tuple<int, double, Rest...>;
 * };
 * ```
 * is roughly equivalent to
 * ```cpp
 * using namespace dtml;
 * template<class T> using RebindToIntDouble = let<
 *                                                     // F     , A     , B     , Rest...
 *                                                 invoke<ignore, ignore, ignore, seq<'R'>>, T,
 *                                                 std::tuple<int, double, seq<'R'>>>;
 * ```
 * Note how `seq<'R'>` is spliced in parameter pack in the resulting expression, same as `Rest...` would do.
 *
 * Otherwise, seq follows the same rules as var.
 */
template<auto n> struct seq {};


LAZY_TEMPLATE(expand) {
	template<class Bindings, class Expression> struct unbox<expand<Bindings, Expression>>: box<Expression> {};

	template<class Bindings, HasType Expression> struct unbox<expand<Bindings, Expression>>: call<expand, Bindings, type_t<Expression>> {};

	template<class Bindings, template<class...> class AST, class... Branches> struct unbox<expand<Bindings, AST<Branches...>>>
		: call<apply, func_box<AST>, concat_t<expand_t<Bindings, Branches>...>> {};

	template<class Bindings, auto n> struct unbox<expand<Bindings, var<n>>>: call<expand, Bindings, lookup_t<var<n>, Bindings>> {};

	template<class Bindings, auto n> struct unbox<expand<Bindings, seq<n>>>: call<expand, Bindings, lookup_t<seq<n>, Bindings>> {};
}

namespace strict {

namespace let_ {

template<class...> struct expand {};

template<class... Ts> using expand_t = type_t<expand<Ts...>>;



namespace make_bindings_ {

template<class... Args> struct args {};

template<template<class...> class AST> struct not_args: true_value {};

template<template<class...> class AST> inline constexpr auto not_args_v = value_v<not_args<AST>>;

template<> struct not_args<args>: false_value {};


template<class T> struct not_seq: true_value {};

template<class T> inline constexpr auto not_seq_v = value_v<not_seq<T>>;

template<auto n> struct not_seq<seq<n>>: false_value {};

}

template<class... Ts> struct make_bindings: box<null> {
	DTML_THROW(Ts..., "let: no match");
};

template<class... Ts> using make_bindings_t = type_t<make_bindings<Ts...>>;

// irrefutable _-pattern
template<class Bindings, class Val, class... MoreBindings> struct make_bindings<Bindings, ignore, Val, MoreBindings...>: make_bindings<Bindings, MoreBindings...> {};

// refutable literal pattern
template<class Bindings, class Pat, class Val, class... MoreBindings> requires almost_same_v<Pat, Val> struct make_bindings<Bindings, Pat, Val, MoreBindings...>
	: make_bindings<Bindings, MoreBindings...> {};

// almost irrefutable var binding
template<class Bindings, auto n, class Val, class... MoreBindings> struct make_bindings<Bindings, var<n>, Val, MoreBindings...>
	: make_bindings<insert_t<q<var<n>, Val>, Bindings>, MoreBindings...> {};

template<class Bindings, auto n, class Val, class... MoreBindings> requires contains_v<var<n>, Bindings> struct make_bindings<Bindings, var<n>, Val, MoreBindings...>
	: make_bindings<Bindings, lookup_t<var<n>, Bindings>, Val, MoreBindings...> {};

// subtree of patterns
template<class Bindings, template<class...> class AST, class... Pats, class... Vals, class... MoreBindings> requires make_bindings_::not_args_v<AST>
struct make_bindings<Bindings, AST<Pats...>, AST<Vals...>, MoreBindings...>
	: make_bindings<Bindings, make_bindings_::args<Pats...>, make_bindings_::args<Vals...>, MoreBindings...> {};

// invoke-pattern, a special case, when invoke<pat_f, pat_0, ...> is matched against an arbitrary template instantiation
template<class Bindings, class F, class... Pats, class AST, class... MoreBindings> struct make_bindings<Bindings, invoke<F, Pats...>, AST, MoreBindings...>
	: make_bindings<Bindings, F, root_t<AST>, make_bindings_::args<Pats...>, apply_t<func_box<make_bindings_::args>, args_t<AST>>, MoreBindings...> {};

// argument sequence of patterns matched against argument sequence of values
template<class Bindings, class Pat, class... Pats, class Val, class... Vals, class... MoreBindings> requires make_bindings_::not_seq_v<Pat>
struct make_bindings<Bindings, make_bindings_::args<Pat, Pats...>, make_bindings_::args<Val, Vals...>, MoreBindings...>
	: make_bindings<Bindings, Pat, Val, make_bindings_::args<Pats...>, make_bindings_::args<Vals...>, MoreBindings...> {};

template<class Bindings, class... MoreBindings>
struct make_bindings<Bindings, make_bindings_::args<>, make_bindings_::args<>, MoreBindings...>
	: make_bindings<Bindings, MoreBindings...> {};

// almost irrefutable seq<n>-pattern
template<class Bindings, auto n, class... Vals, class... MoreBindings>
struct make_bindings<Bindings, make_bindings_::args<seq<n>>, make_bindings_::args<Vals...>, MoreBindings...>
	: make_bindings<insert_t<q<seq<n>, q<Vals...>>, Bindings>, MoreBindings...> {};

template<class Bindings, auto n, class... Vals, class... MoreBindings> requires contains_v<seq<n>, Bindings>
struct make_bindings<Bindings, make_bindings_::args<seq<n>>, make_bindings_::args<Vals...>, MoreBindings...>
	: make_bindings<Bindings, lookup_t<seq<n>, Bindings>, q<Vals...>, MoreBindings...> {};

// irrefutable ignored seq
template<class Bindings, class... Vals, class... MoreBindings>
struct make_bindings<Bindings, make_bindings_::args<seq<ignore{}>>, make_bindings_::args<Vals...>, MoreBindings...>
	: make_bindings<Bindings, MoreBindings...> {};

// end of recursion
template<class Bindings> struct make_bindings<Bindings>: box<Bindings> {};


template<class... Ts> using bindings = make_bindings<nil, Ts...>;

template<class... Ts> using bindings_t = make_bindings_t<bindings<Ts...>>;


template<class BindingsAndExpr> class let {
	using Bindings = init_t<BindingsAndExpr>;
	using Expr = last_t<BindingsAndExpr>;
public:
	using type = expand_t<apply_t<func_box<bindings_t>, Bindings>, Expr>;
};

}

}


/**
 * Attempts to match patterns against values and, if there's a match, binds variables to corresponding subexpressions and evaluates the resulting expression.
 *
 * @param init<L> interpersed pattern-value pairs
 * @param last<L> an expression to be evaluated
 * @return the result of expression with free variables bound by pattern matching
 * @throw an exception if there's no match
 */
LAZY_VALUE_TEMPLATE(let) {
	template<class... Ts> struct unbox<let<Ts...>>: call<let, q<Ts...>> {};
	
	template<class L> class unbox<let<L>> {
		using Bindings = apply_t<func_box<let_::bindings_t>, init_t<L>>;
		using Expr = last_t<L>;
	public:
		using type = expand_t<Bindings, Expr>;
	};
}

}

#endif
