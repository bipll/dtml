#ifndef DTML__UNTYPED_CONSTANT_H_d6cfafb4438763df95ac07d63cb1bf48bc9094e5
#define DTML__UNTYPED_CONSTANT_H_d6cfafb4438763df95ac07d63cb1bf48bc9094e5

namespace dtml {

template<auto n> struct untyped_constant {
	template<class T> constexpr operator T() const { return static_cast<T>(n); }
};

#ifdef DTML_TEST_GEN
template<auto n> constexpr auto to_string(untyped_constant<n>) { return "dtml::untyped_constant<" + to_string(n) + '>'; }
#else
template<auto n> constexpr auto to_string(untyped_constant<n>) { return '$' + to_string(n) + '$'; }
#endif

#define DTML_UNTYPED_OPERATOR(sign)														\
template<auto a, auto b> constexpr auto operator sign(untyped_constant<a>, untyped_constant<b>) { return untyped_constant<(a sign b)>{}; }	\
																		\
template<class A, auto b> constexpr auto operator sign(A a, untyped_constant<b>) { return a sign static_cast<A>(b); }				\
																		\
template<auto a, class B> constexpr auto operator sign(untyped_constant<a>, B b) { return static_cast<B>(a) sign b; }

DTML_UNTYPED_OPERATOR(+);
DTML_UNTYPED_OPERATOR(-);
DTML_UNTYPED_OPERATOR(*);
DTML_UNTYPED_OPERATOR(/);
DTML_UNTYPED_OPERATOR(%);
DTML_UNTYPED_OPERATOR(&);
DTML_UNTYPED_OPERATOR(|);
DTML_UNTYPED_OPERATOR(^);

#undef DTML_UNTYPED_OPERATOR

#define DTML_UNTYPED_BOOL_OPERATOR(sign)										\
template<auto a, auto b> constexpr bool operator sign(untyped_constant<a>, untyped_constant<b>) { return a sign b; }	\
															\
template<class A, auto b> constexpr auto operator sign(A a, untyped_constant<b>) { return a sign static_cast<A>(b); }	\
															\
template<auto a, class B> constexpr auto operator sign(untyped_constant<a>, B b) { return static_cast<B>(a) sign b; }

DTML_UNTYPED_BOOL_OPERATOR(<);
DTML_UNTYPED_BOOL_OPERATOR(<=);
DTML_UNTYPED_BOOL_OPERATOR(>);
DTML_UNTYPED_BOOL_OPERATOR(>=);
DTML_UNTYPED_BOOL_OPERATOR(==);
DTML_UNTYPED_BOOL_OPERATOR(!=);
DTML_UNTYPED_BOOL_OPERATOR(<=>);

#undef DTML_UNTYPED_BOOL_OPERATOR

template<class A, auto b> constexpr decltype(auto) operator<<(A &&a, untyped_constant<b>) { return a << b; }
template<class A, auto b> constexpr decltype(auto) operator>>(A &&a, untyped_constant<b>) { return a >> b; }
template<auto a, class B> constexpr auto operator <<(untyped_constant<a>, B b) { return a << b; }
template<auto a, class B> constexpr auto operator >>(untyped_constant<a>, B b) { return a >> b; }

inline constexpr untyped_constant<0> untyped_zero{};
inline constexpr untyped_constant<1> untyped_one{};

}

#endif
