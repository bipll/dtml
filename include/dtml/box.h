#ifndef DTML__BOX_H_39a2dcaf7fd0bf33020c3f34c90e5fa8073eab66
#define DTML__BOX_H_39a2dcaf7fd0bf33020c3f34c90e5fa8073eab66

#include <type_traits>

#include "string.h"
#include "untyped_constant.h"

namespace dtml {

#define DTML_DELAYED_EVALUATION(...) static constexpr bool dtml_delayed_evaluation = __VA_ARGS__

struct lazy { DTML_DELAYED_EVALUATION(true); };


namespace strict {

template<class T, class... Ts> concept DelayedEvaluation = T::dtml_delayed_evaluation;

}


template<template<class...> class F, class... Args> concept LazyWith = strict::DelayedEvaluation<F<Args...>>;


template<class... Bases> struct make_lazy: lazy, Bases... {};

template<class... Bases> requires (strict::DelayedEvaluation<Bases> || ...) struct make_lazy<Bases...>: Bases... {};



struct null: lazy {};



// Prim unary concepts
template<auto v> struct value_box: make_lazy<std::integral_constant<decltype(v), v>> {};

template<std::size_t n> using size_value = value_box<n>;
using zero_size = size_value<0>;
using one_size = size_value<1>;

template<bool b> using bool_value = value_box<b>;
using false_value = bool_value<false>;
using true_value = bool_value<true>;

template<auto x> using untyped_value = value_box<untyped_constant<x>{}>;
using zero_value = untyped_value<0>;
using one_value = untyped_value<1>;

using empty_string = value_box<string{}>;



namespace strict {

template<template<class...> class F, class... Args> using lazy_with = bool_value<LazyWith<F, Args...>>;

template<template<class...> class F, class... Args> inline constexpr auto lazy_with_v = lazy_with<F, Args...>::value;

}

using strict::lazy_with;

using strict::lazy_with_v;



template<class T> struct self_referential: make_lazy<std::is_same<T, typename T::type>> {};

template<class T> inline constexpr auto self_referential_v = self_referential<T>::value;

template<auto v> struct self_referential<value_box<v>>: true_value {};


template<class T> concept HasMemberType = requires { typename T::type; } && !self_referential_v<T>;

template<class T> concept HasMemberValue = requires { T::value; };



template<class... Ts> struct box: lazy {};

template<class T, class... Ts> struct box<T, Ts...>: lazy { using type = T; };



namespace strict {

template<class... Ts> struct member: lazy {};

template<class... Ts> using member_t = typename member<Ts...>::type;

template<HasMemberType T, class... Ts> struct member<T, Ts...>: box<typename T::type> {};

}

using strict::member;

using strict::member_t;



namespace strict {

template<class... Ts> struct boxed: false_value {};

template<class... Ts> inline constexpr auto boxed_v = boxed<Ts...>::value;

}

using strict::boxed;

using strict::boxed_v;

template<class... Ts> concept Boxed = boxed_v<Ts...>;


namespace strict {

template<class... Ts> struct unbox: lazy {};

template<class... Ts> using unbox_t = typename unbox<Ts...>::type;

template<Boxed T, class... Ts> struct unbox<T, Ts...>: unbox<T> {};

}

using strict::unbox;

using strict::unbox_t;


template<class... Ts> struct escape: lazy {};


namespace strict {

template<class... Ts> struct has_type: false_value {};

template<class... Ts> inline constexpr auto has_type_v = has_type<Ts...>::value;

template<class... Ts> requires Boxed<Ts...> struct has_type<Ts...>: true_value {};

template<HasMemberType T, class... Ts> struct has_type<T, Ts...>: true_value {};

template<class... Ts> struct has_type<escape<Ts...>>: false_value {};

}

using strict::has_type;

using strict::has_type_v;

template<class... Ts> concept HasType = has_type_v<Ts...>;



namespace strict {

template<class... Ts> struct get_type: box<Ts...> {};

template<class... Ts> using type_t = typename get_type<Ts...>::type;

template<class... Ts> requires Boxed<Ts...> struct get_type<Ts...>: get_type<unbox_t<Ts...>> {};

template<class T, class... Ts> requires HasMemberType<T> struct get_type<T, Ts...>: get_type<member_t<T, Ts...>> {};

template<class... Ts> struct get_type<escape<Ts...>>: box<Ts...> {};

}

using strict::get_type;

using strict::type_t;



namespace strict {

template<class... Ts> struct get_value: lazy {};

template<class... Ts> inline constexpr auto value_v = get_value<Ts...>::value;

template<HasMemberValue T, class... Ts> struct get_value<T, Ts...>: value_box<T::value> {};

template<HasType T, class... Ts> requires (!HasMemberValue<T>) struct get_value<T, Ts...>: get_value<type_t<T>, Ts...> {};


template<class... Ts> struct has_value: false_value {};

template<class... Ts> inline constexpr auto has_value_v = value_v<has_value<Ts...>>;

template<HasMemberValue V, class... Ts> struct has_value<V, Ts...>: true_value {};

template<HasType T, class... Ts> requires (!HasMemberValue<T>) struct has_value<T, Ts...>: has_value<type_t<T>, Ts...> {};

}

using strict::get_value;

using strict::value_v;

template<class... Ts> using value_t = decltype(value_v<Ts...>);

using strict::has_value;

using strict::has_value_v;

template<class... Ts> concept HasValue = has_value_v<Ts...>;



template<class...> struct values_pack: lazy {};


template<class... Ts> struct values: box<values_pack<Ts...>> {};

template<class... Ts> using values_t = type_t<values<Ts...>>;

template<class T> struct values<T>: box<T> {};



namespace strict {

template<template<class...> class F, class... Args> struct call: box<F<type_t<Args>...>> {};

template<template<class...> class F, class... Args> using call_t = type_t<call<F, Args...>>;

template<template<class...> class F, class... Args> requires LazyWith<F, Args...> struct call<F, Args...>: box<F<Args...>> {};

template<template<class...> class F, class... Args> struct call<F, values_pack<Args...>>: call<F, Args...> {};



template<class Value, template<class...> class... Fs> struct short_circuiting: false_value {};

template<class Value, template<class...> class... Fs> inline constexpr auto short_circuiting_v = value_v<short_circuiting<Value, Fs...>>;

}

using strict::short_circuiting;

using strict::short_circuiting_v;

template<class Value, template<class...> class... Fs> concept ShortCircuiting = short_circuiting<Value, Fs...>::value;


template<class Value, template<class...> class... Then> struct thunk {};

template<class Value, template<class...> class... Then> struct boxed<thunk<Value, Then...>>: true_value {};


namespace thunk_unbox_ {

template<template<class...> class...> struct fseq {};

template<class, class Best, class Extra, template<class...> class... Unprocessed> struct longest_short_circuit {
using best = Best;
using extra = Extra;
// end-of-recursion assertion
static_assert(sizeof...(Unprocessed) == 0);
};

template<class Value, template<class...> class... Best, template<class...> class... Extra, template<class...> class Next, template<class...> class... More>
struct longest_short_circuit<Value, fseq<Best...>, fseq<Extra...>, Next, More...>
: std::conditional_t<ShortCircuiting<Value, Best..., Extra..., Next>
	, longest_short_circuit<Value, fseq<Best..., Extra..., Next>, fseq<>, More...>
	, longest_short_circuit<Value, fseq<Best...>, fseq<Extra..., Next>, More...>> {};

template<class Accum, template<class...> class... Fs> struct fold: get_type<Accum> {};
template<class Accum, template<class...> class... Fs> using fold_t = type_t<fold<Accum, Fs...>>;
template<class Accum, template<class...> class F, template<class...> class... Fs> struct fold<Accum, F, Fs...>: fold<strict::call_t<F, Accum>, Fs...> {};
template<class... Accum, template<class...> class F, template<class...> class... Fs> struct fold<values_pack<Accum...>, F, Fs...>: fold<strict::call_t<F, Accum...>, Fs...> {};

template<class Value, class Best, class Extra> struct collapse {};
template<class Value, class Best, class Extra> using collapse_t = type_t<collapse<Value, Best, Extra>>;
template<class Value, template<class...> class... Best, template<class...> class... Extra> struct collapse<Value, fseq<Best...>, fseq<Extra...>>: get_type<thunk<fold_t<Value, Best...>, Extra...>> {};
template<class Value, template<class...> class... Best> struct collapse<Value, fseq<Best...>, fseq<>>: fold<Value, Best...> {};
template<class Value, template<class...> class Next, template<class...> class... Extra> struct collapse<Value, fseq<>, fseq<Next, Extra...>>: get_type<thunk<strict::call_t<Next, Value>, Extra...>> {};
template<class... Values, template<class...> class Next, template<class...> class... Extra> struct collapse<values_pack<Values...>, fseq<>, fseq<Next, Extra...>>
	: get_type<thunk<strict::call_t<Next, Values...>, Extra...>> {};

}

template<class Value, template<class...> class... Then> class unbox<thunk<Value, Then...>> {
	using split = thunk_unbox_::longest_short_circuit<Value, thunk_unbox_::fseq<>, thunk_unbox_::fseq<>, Then...>;
public:
	using type = thunk_unbox_::collapse_t<Value, typename split::best, typename split::extra>;
};

namespace strict {

template<template<class...> class F, class Value, template<class...> class... Then> struct call<F, thunk<Value, Then...>>: box<thunk<Value, Then..., F>> {};

}


template<template<class...> class F, class... Args> struct call: box<thunk<values_t<Args...>, F>> {};

template<template<class...> class F, class Value, template<class...> class... Then> struct call<F, thunk<Value, Then...>>: box<thunk<Value, Then..., F>> {};

template<template<class...> class F, class... Args> using call_t = type_t<call<F, Args...>>;

template<template<class...> class F, class... Args> inline constexpr auto call_v = value_v<call<F, Args...>>;


template<class T, template<class...> class F, class... Ts> concept HasProperty = static_cast<bool>(call_v<F, Ts..., T>);

template<class T, template<class...> class F, class... Ts> concept LacksProperty = !HasProperty<T, F, Ts...>;



template<class... Ts> struct q {};
using nil = q<>;


template<class... Ts> struct konst: lazy {
	template<class...> using func = box<Ts...>;
};

template<class T> constexpr konst<T> put_func_in_a_box();

#define DTML_FUNC_BOX(...) decltype(::dtml::put_func_in_a_box<__VA_ARGS__>())


template<class...> struct factors {};

template<class T, class... Ts> struct factors<T, Ts...> {
	using root = konst<T>;
	using args = null;
};

template<class... Ts> using root_t = typename factors<Ts...>::root;
template<class... Ts> using args_t = typename factors<Ts...>::args;

template<class... Ts> using deepest_root_t = typename factors<type_t<Ts...>>::root;
template<class... Ts> using deepest_args_t = typename factors<type_t<Ts...>>::args;



template<template<class...> class F> struct func_box {
	template<class... Ts> using func = call<F, Ts...>;
};


template<template<class...> class F> constexpr func_box<F> put_func_in_a_box();


template<template<class...> class F, class... Ts, class... Us> struct factors<F<Ts...>, Us...> {
	using root = func_box<F>;
	using args = q<Ts...>;
};


struct quote: DTML_FUNC_BOX(q) { DTML_DELAYED_EVALUATION(false); };


namespace strict {

template<class... Ts> struct has_member_func;

template<class... Ts> inline constexpr auto has_member_func_v = value_v<has_member_func<Ts...>>;

template<class... Ts> struct has_member_func: bool_value<(has_member_func_v<Ts> && ...)> {};

template<template<class...> class...> using fvoid_t = void;

namespace member_func_ {
template<class U, class = void> struct has: false_value {};
template<class U> struct has<U, fvoid_t<U::template func>>: true_value {};
}

template<class T> struct has_member_func<T>: member_func_::has<T> {};

}

using strict::has_member_func;

using strict::has_member_func_v;

template<class... Ts> concept HasMemberFunc = has_member_func_v<Ts...>;


namespace strict {

template<class...> struct has_func;

template<class... Ts> inline constexpr auto has_func_v = value_v<has_func<Ts...>>;

template<class... Ts> struct has_func: bool_value<(has_func_v<Ts> && ...)> {};

template<class T> struct has_func<T>: false_value {};

template<HasMemberFunc F> struct has_func<F>: true_value {};

template<class T> requires (!HasMemberFunc<T> && HasType<T>) struct has_func<T>: has_func<unbox_t<T>> {};

}

using strict::has_func;

using strict::has_func_v;

template<class... Ts> concept HasFunc = has_func_v<Ts...>;


namespace strict {

template<class...> struct get_func: lazy {};

template<HasMemberFunc F, class... Us> struct get_func<F, Us...>: lazy {
	template<class... Ts> using func = call<F::template func, Ts...>;
};

template<HasType T, class... Us> requires (!HasMemberFunc<T>) struct get_func<T, Us...>: lazy {
	template<class... Ts> using func = call<get_func<unbox_t<T>, Ts...>::template func, Ts...>;
};

}

using strict::get_func;

}

#endif
