#ifndef DTML__LIST_H_a6b1b71e21bc6d86a275b64ca94c384948855a96
#define DTML__LIST_H_a6b1b71e21bc6d86a275b64ca94c384948855a96

#include <functional>
#include <numeric>
#include <type_traits>

#include "basic.h"
#include "func_box.h"

/**
 * This library is a small collection of functions operating on lists (mostly from PreludeList).
 *
 * Additionally, bind and lambda can receive their full definitions here, as they rely on few list operators.
 */

namespace dtml {

/**
 * Map a function to a list.
 *
 * @param car<L> a callable to be invoked on every element of the list
 * @param cdr<L> a list
 * @return a list comprised of results of the callable being applied to every element of the list
 */
LAZY_LIST_OPERATOR(transform) {
	template<LongerThan<2> L> struct unbox<transform<L>>: make_lazy<call<cons, invoke<car_t<L>, cadr<L>>, dtml::call<transform, car_t<L>, cddr<L>>>> {};

	//template<LongerThan<2> L> requires Infinite<L> struct unbox<transform<L>>: box<escape<transform<L>>> {};

	template<ExactSize<1> L> struct unbox<transform<L>>: box<nil> {};

	template<class... Ts> struct short_circuiting<transform<Ts...>, finite>: true_value {};
	template<class... Ts> struct finite<transform<Ts...>>: call<finite, Ts...> {};

	template<class... Ts> struct short_circuiting<transform<Ts...>, infinite>: true_value {};
	template<class... Ts> struct infinite<transform<Ts...>>: call<infinite, Ts...> {};

	template<class... Ts> struct short_circuiting<transform<Ts...>, empty>: true_value {};
	template<class... Ts> struct empty<transform<Ts...>>: call<empty, cdr<Ts...>> {};

	template<class... Ts> struct short_circuiting<transform<Ts...>, car>: true_value {};
	template<class... Ts> struct car<transform<Ts...>>: invoke<car_t<Ts...>, cadr_t<Ts...>> {};

	template<class... Ts> struct short_circuiting<transform<Ts...>, cdr>: true_value {};
	template<class... Ts> struct cdr<transform<Ts...>>: call<transform, car_t<Ts...>, cddr<Ts...>> {};
}


/**
 * Map a function to a list, concatenating the results.
 *
 * @param car<L> a callable to be invoked on every element of the list
 * @param cdr<L> a list
 * @return a list comprised of results of the callable being applied to every element of the list, concatenated
 */
LAZY_LIST_OPERATOR(flat_transform) {
	template<LongerThan<1> L> struct unbox<flat_transform<L>>: call<apply, dtml::concat, call<transform, L>> {};
}


/**
 * Filter a list through a predicate, discarding all the elements dissatisfying the latter.
 *
 * The closest analog from STL would be `std::remove_if`, save for the semantics of predicate is inverted there: while `filter` retains all the elements on which the predicate returns true,
 * `remove_if` does the inverse and discards them.
 *
 * @param car<L> a callable that returns a truth value (i.e. castable to bool) on each element of the list
 * @param cdr<L> a list to be filtered
 * @return filtered list
 */
LAZY_LIST_OPERATOR(filter) {
	template<LongerThan<2> L> struct unbox<filter<L>>: call<filter, car_t<L>, cddr<L>> {};
	template<LongerThan<2> L> requires Satisfies<cadr<L>, car<L>> struct unbox<filter<L>>: call<cons, cadr<L>, call<filter, car<L>, cddr<L>>> {};

	template<ExactSize<1> L> struct unbox<filter<L>>: box<nil> {};

	template<class... Ts> struct short_circuiting<finite<Ts...>, car>: true_value {};
	template<class... Ts> struct car<finite<Ts...>>: call<car, find_if<Ts...>> {};

	template<class... Ts> struct short_circuiting<filter<Ts...>, car>: true_value {};
	template<class... Ts> struct car<filter<Ts...>>: call<car, find_if<Ts...>> {};

	template<class... Ts> struct short_circuiting<filter<Ts...>, cdr>: true_value {};
	template<class... Ts> struct cdr<filter<Ts...>>: call<filter, car_t<Ts...>, cdr<find_if<Ts...>>> {};
}


/**
 * Take the prefix of specified length from a given list.
 *
 * @param car<L> an integral value
 * @param cdr<L> a list
 * @return a list comprised of first `car<L>` elements of `cdr<L>`, or the whole `cdr<L>` if it's shorter
 */
LAZY_LIST_OPERATOR(take) {
	template<LongerThan<2> L> struct unbox<take<L>>: call<cons, cadr<L>, call<take, dec<car<L>>, cddr<L>>> {};

	template<LongerThan<2> L> requires non_positive_v<car<L>> struct unbox<take<L>>: box<nil> {};

	template<ExactSize<1> L> struct unbox<take<L>>: box<nil> {};
}


/**
 * Return the longest prefix of elements satisfying a predicate.
 *
 * @param car<L> a callable that returns a truth value (i.e. castable to `bool`) on each element of the list, up until the first one that doesn't satisfy it
 * @param cdr<L> a list
 * @return
 */
LAZY_LIST_OPERATOR(take_while) {
	template<LongerThan<2> L> struct unbox<take_while<L>>: box<nil> {};

	template<LongerThan<2> L> requires Satisfies<cadr<L>, car<L>> struct unbox<take_while<L>>: call<cons, cadr<L>, call<take_while, car<L>, cddr<L>>> {};

	template<ExactSize<1> L> struct unbox<take_while<L>>: box<nil> {};
}


/**
 * Return suffix of the list past first N elements.
 *
 * The closest analog from STL would be `std::advance` of a container's `begin()`.
 *
 * @param car<L> amount of elements to be dropped
 * @param cdr<L> a list
 * @return a list comprised of all `cdr<L>`'s elements past first `car<L>` of them
 */
LAZY_LIST_OPERATOR(drop) {
	template<LongerThan<2> L> struct unbox<drop<L>>: call<drop, dec<car<L>>, cddr<L>> {};

	template<LongerThan<2> L> requires non_positive_v<car<L>> struct unbox<drop<L>>: call<cdr, L> {};

	template<ExactSize<1> L> struct unbox<drop<L>>: box<nil> {};
}


/**
 * Return suffix of the list starting with the first element that does not satisfy the predicate.
 *
 * @param car<L> a callable that returns a truth value (i.e. castable to `bool`) on each element of the list, up until the first element on which it returns a false value
 * @param cdr<L> a list
 * @return
 */
LAZY_LIST_OPERATOR(drop_while) {
	template<LongerThan<1> L> struct unbox<drop_while<L>>: call<cdr, L> {};

	template<LongerThan<1> L> requires (LongerThan<L, 2> && Satisfies<cadr<L>, car<L>>) struct unbox<drop_while<L>>: call<drop_while, car<L>, cddr<L>> {};
}


/**
 * Take list element by index, zero-based.
 *
 * It is an error if index is equal to, or greater than, the length of the list, or the list itself is empty.
 * On the other hand, if index in negative and list non-empty, the first element of the list is returned.
 *
 * @param car<L> an integral value
 * @param cdr<L> a list
 * @return `car<L>`'th element of the list, zero based, or the first one, when index is negative
 */
LAZY_LIST_SCALAR(at) {
	template<LongerThan<2> L> struct unbox<at<L>>: call<car, call<drop, L>> {};

/* This one crashes gcc 10.
template<class I, class... Ts> requires invoke_v<greater_equal, I, length<Ts...>> struct at<I, Ts...>
	: exception<string{"at: index "} + to_string(value_v<I>) + " is too large for a list of size " + to_string(length_v<Ts...>)> {};
 */
}


//#undef BOXED_BY_DEFAULT
//#define BOXED_BY_DEFAULT false_value

#define INFINITE_LIST(name, ...)								\
LAZY_LIST_OPERATOR(name, __VA_ARGS__) {								\
	template<LongerThan<0> L>  struct unbox<name<L>>: box<escape<name<L>>> {};		\
												\
	template<class... Ts> struct short_circuiting<name<Ts...>, finite>: true_value {};	\
	template<class... Ts> struct finite<name<Ts...>>: box<false_value> {};			\
												\
	template<class... Ts> struct short_circuiting<name<Ts...>, infinite>: true_value {};	\
	template<class... Ts> struct infinite<name<Ts...>>: box<true_value> {};			\
												\
	template<class... Ts> struct short_circuiting<name<Ts...>, empty>: true_value {};	\
	template<class... Ts> struct empty<name<Ts...>>: box<false_value> {};			\
												\
	template<class... Ts> struct short_circuiting<name<Ts...>, car>: true_value {};		\
	template<class... Ts> struct short_circuiting<name<Ts...>, cdr>: true_value {};		\
												\
}												\
namespace strict


/**
 * Infinite repetition of the same value(s).
 *
 * @param L list of values to cyclically repeat
 * @return an infinite list comprised of L's elements, looped
 */
INFINITE_LIST(repeat) {
	template<LongerThan<1> L> struct car<repeat<L>>: call<car, L> {};

	template<LongerThan<1> L> struct cdr<repeat<L>>: call<cdr, L, call<repeat, L>> {};
}


/**
 * Build a list by infinitely applying a callable to initial value(s) (those latter can be empty).
 *
 * @param car<L> a callable
 * @param cdr<L> starting values
 * @return a list starting with the first element of `cdr<L>`, if any, then with `car<L>` applied to `cdr<L>`, and then with each subsequent element being equal to `car<L>` applied to its predecessor
 */
INFINITE_LIST(iterate) {
	template<LongerThan<2> L> struct car<iterate<L>>: call<cadr, L> {};
	template<ExactSize<1> L> struct car<iterate<L>>: invoke<call_t<car, L>> {};

	template<LongerThan<2> L> struct cdr<iterate<L>>: call<sequence, call<iterate, car_t<L>, apply_t<car<L>, cdr<L>>>> {};
	template<ExactSize<1> L> struct cdr<iterate<L>>: call<cdr, call<iterate, call<append, L, invoke<call<car, L>>>>> {};
}


/**
 * The list of all natural numbers*.
 * ---
 * <sup>* Actually, up to the largest `size_t` value, and then foldover back to zero. Infinite precision integrals are currently not a part of this library but may be added later.</sup>
 */
using naturals = iterate_t<inc, zero_size>;


// Infinite lists are non-boxed on their own
#undef BOXED_BY_DEFAULT
#define BOXED_BY_DEFAULT true_value

/**
 * Python-style range function.
 *
 * When called with only one argument N it returns a sequence of natural numbers from 0 to N-1, inclusive.<br/>
 * When called with two arguments, N and M, it returns the sequence [N, N+1, ..., M-1] (empty list if M <= N).<br/>
 * With three arguments, N, M and K, the sequence returned is [N, N+K, ..., J] where J < M <= J+K (or J+K <= M < J, with negative K). It is an error if K is zero.
 *
 * @param car<L> N (_see above_)
 * @param cadr<L> M (_see above_)
 * @param caddr<L> K (_see above_)
 * @return
 */
LAZY_LIST_OPERATOR(iota) {
	template<ExactSize<1> L> struct unbox<iota<L>>: call<take, car<L>, naturals> {};

	template<ExactSize<2> L> struct unbox<iota<L>>: call<drop, car<L>, call<iota, cadr<L>>> {};

	template<ExactSize<3> L> struct unbox<iota<L>>: call<cons, car<L>, call<iota, call<plus, car<L>, caddr<L>>, cdr<L>>> {};

	template<ExactSize<3> L> requires (positive_v<caddr<L>> && greater_equal_v<car<L>, cadr<L>>) struct unbox<iota<L>>: box<nil> {};

	template<ExactSize<3> L> requires (negative_v<caddr<L>> && less_equal_v<car<L>, cadr<L>>) struct unbox<iota<L>>: box<nil> {};

	template<ExactSize<3> L> requires zero_v<caddr<L>> struct unbox<iota<L>> {
		DTML_THROW(L, "iota: increment of zero");
	};
}


#if 0
namespace strict {

template<class F, class... Prefix> struct bind<F, Prefix...> {
	template<class... Ts> struct prearg: box<Ts...> {};
	template<std::size_t n, class... Args> struct prearg<arg<n>, Args...>: at<size_value<n>, Args...> {};

	using placeholders = filter_t<DTML_FUNC_BOX(is_arg), Prefix...>;
	template<class... Ts> using not_a_placeholder = invoke<logical_not, find<car<Ts...>, placeholders>>;
	template<class... Args> using args_range = range<length<Args...>>;
	template<class... Args> using remaining_indices = filter<DTML_FUNC_BOX(not_a_placeholder), args_range<Args...>>;
	template<class... Args> struct remaining_arg { template<class... Ts> using func = at<Ts..., Args...>; };
	template<class... Args> using remaining_args = transform<remaining_arg<Args...>, remaining_indices<Args...>>;

	template<class... Args> using preprea = q<prearg<Prefix, Args...>...>;
	template<class... Args> using rem = remaining_args<Args...>;
public:
	template<class... Args> using func = apply<F, q<prearg<Prefix, Args...>...>, remaining_args<Args...>>;
};

}


template<class Expr, class... Ts> class lambda<Expr, Ts...> {
	template<class Node, class... Args> struct expand: box<Node> {};
	template<class Node, class... Args> using expand_t = type_t<expand<Node, Args...>>;
	template<std::size_t n, class... Args> struct expand<arg<n>, Args...>: invoke<at, size_value<n>, Args...> {};
	template<template<class...> class AST, class... Branches, class... Args> struct expand<AST<Branches...>, Args...>: box<AST<expand_t<Branches, Args...>...>> {};
public:
	using expr = Expr;
	template<class... Args> using func = expand_t<expr, Args...>;
};
#endif

}

#endif
