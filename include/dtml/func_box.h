#ifndef DTML__FUNC_BOX_H_e9fd68b9bb1a7acc4964eea3e8128f65bc1d3bcd
#define DTML__FUNC_BOX_H_e9fd68b9bb1a7acc4964eea3e8128f65bc1d3bcd

#include "box.h"
#include "macro.h"

namespace dtml {

#ifndef DTML_FUNC_BOX_MAX_SINGLE_SKIP
#	define DTML_FUNC_BOX_MAX_SINGLE_SKIP F
#endif


#ifndef DTML_FUNC_BOX_MAX_DOUBLE_SKIP
#	define DTML_FUNC_BOX_MAX_DOUBLE_SKIP 8
#endif


#ifndef DTML_FUNC_BOX_MAX_TRIPLE_SKIP
#	define DTML_FUNC_BOX_MAX_TRIPLE_SKIP 4
#endif


#ifndef DTML_FUNC_BOX_MAX_QUADRUPLE_SKIP
#	define DTML_FUNC_BOX_MAX_QUADRUPLE_SKIP 3
#endif


#define DTML_FUNC_BOX_N(N, ...)															\
template<template<DTML_MACRO_TRAILING_DEC1(N, DTML_MACRO_COMMA, DTML_MACRO_CONST, class) auto, class...> class F> struct func_box_ ## N {	\
	template<DTML_MACRO_TRAILING_DEC1(N, DTML_MACRO_COMMA, DTML_MACRO_CLASS_T0, T) class V, class... Ts>					\
	using func = make_lazy<F<DTML_MACRO_TRAILING_DEC1(N, DTML_MACRO_COMMA, DTML_MACRO_T0, T) value_v<V>, type_t<Ts>...>>;			\
};																		\
																		\
template<template<DTML_MACRO_TRAILING_DEC1(N, DTML_MACRO_COMMA, DTML_MACRO_CONST, class) auto, class...> class F,				\
	DTML_MACRO_TRAILING_DEC1(N, DTML_MACRO_COMMA, DTML_MACRO_CLASS_T0, T) auto v, class... Ts, class... Us>					\
struct factors<F<DTML_MACRO_TRAILING_DEC1(N, DTML_MACRO_COMMA, DTML_MACRO_T0, T) v, Ts...>, Us...>						\
{																		\
	using root = func_box_ ## N<F>;														\
	using args = q<DTML_MACRO_TRAILING_DEC1(N, DTML_MACRO_COMMA, DTML_MACRO_T0, T) value_box<v>, Ts...>;					\
};																		\
																		\
template<template<DTML_MACRO_TRAILING_DEC1(N, DTML_MACRO_COMMA, DTML_MACRO_CONST, class) auto, class...> class F> constexpr func_box_ ## N<F> put_func_in_a_box()

DTML_MACRO_DEC0(DTML_FUNC_BOX_MAX_SINGLE_SKIP, DTML_MACRO_SEMI, DTML_FUNC_BOX_N);

#undef DTML_FUNC_BOX_N


#define DTML_FUNC_BOX_MN(M, N, ...)											\
template<template<DTML_MACRO_TRAILING_DEC1(M, DTML_MACRO_COMMA, DTML_MACRO_CONST, class) auto,				\
                  DTML_MACRO_TRAILING_DEC1(N, DTML_MACRO_COMMA, DTML_MACRO_CONST, class) auto, class...> class F>	\
struct func_box_ ## M ## N												\
{															\
	template<DTML_MACRO_TRAILING_DEC1(M, DTML_MACRO_COMMA, DTML_MACRO_CLASS_T0, S) class V,				\
		 DTML_MACRO_TRAILING_DEC1(N, DTML_MACRO_COMMA, DTML_MACRO_CLASS_T0, T) class W, class... Ts>		\
	using func = make_lazy<F<DTML_MACRO_TRAILING_DEC1(M, DTML_MACRO_COMMA, DTML_MACRO_T0, S) value_v<V>,		\
		                 DTML_MACRO_TRAILING_DEC1(N, DTML_MACRO_COMMA, DTML_MACRO_T0, T) value_v<W>, Ts...>>;	\
};															\
															\
template<template<DTML_MACRO_TRAILING_DEC1(M, DTML_MACRO_COMMA, DTML_MACRO_CONST, class) auto,				\
                  DTML_MACRO_TRAILING_DEC1(N, DTML_MACRO_COMMA, DTML_MACRO_CONST, class) auto, class...> class F,	\
 	  DTML_MACRO_TRAILING_DEC1(M, DTML_MACRO_COMMA, DTML_MACRO_CLASS_T0, S) auto v,					\
	  DTML_MACRO_TRAILING_DEC1(N, DTML_MACRO_COMMA, DTML_MACRO_CLASS_T0, T) auto w, class... Ts, class... Us>	\
struct factors<F<DTML_MACRO_TRAILING_DEC1(M, DTML_MACRO_COMMA, DTML_MACRO_T0, S) v,					\
		 DTML_MACRO_TRAILING_DEC1(N, DTML_MACRO_COMMA, DTML_MACRO_T0, T) w, Ts...>, Us...>			\
{															\
	using root = func_box_ ## M ## N<F>;										\
	using args = q<DTML_MACRO_TRAILING_DEC1(M, DTML_MACRO_COMMA, DTML_MACRO_T0, S) value_box<v>,			\
			  DTML_MACRO_TRAILING_DEC1(N, DTML_MACRO_COMMA, DTML_MACRO_T0, T) value_box<w>, Ts...>;		\
};															\
															\
template<template<DTML_MACRO_TRAILING_DEC1(M, DTML_MACRO_COMMA, DTML_MACRO_CONST, class) auto,				\
		  DTML_MACRO_TRAILING_DEC1(N, DTML_MACRO_COMMA, DTML_MACRO_CONST, class) auto, class...> class F>	\
constexpr func_box_ ## M ## N<F> put_func_in_a_box()

DTML_MACRO_DEC0(DTML_FUNC_BOX_MAX_DOUBLE_SKIP, DTML_MACRO_SEMI, DTML_MACRO_APPLY_ROL0, DTML_MACRO_DEC2, DTML_FUNC_BOX_MAX_DOUBLE_SKIP, DTML_MACRO_SEMI, DTML_FUNC_BOX_MN);

#undef DTML_FUNC_BOX_MN


#define DTML_FUNC_BOX_MNK(M, N, K, ...)											\
template<template<DTML_MACRO_TRAILING_DEC1(M, DTML_MACRO_COMMA, DTML_MACRO_CONST, class) auto,				\
		  DTML_MACRO_TRAILING_DEC1(N, DTML_MACRO_COMMA, DTML_MACRO_CONST, class) auto,				\
		  DTML_MACRO_TRAILING_DEC1(K, DTML_MACRO_COMMA, DTML_MACRO_CONST, class) auto, class... Ts> class F>	\
struct func_box_ ## M ## N ## K												\
{															\
	template<DTML_MACRO_TRAILING_DEC1(M, DTML_MACRO_COMMA, DTML_MACRO_CLASS_T0, R) class V,				\
		 DTML_MACRO_TRAILING_DEC1(N, DTML_MACRO_COMMA, DTML_MACRO_CLASS_T0, S) class W,				\
		 DTML_MACRO_TRAILING_DEC1(K, DTML_MACRO_COMMA, DTML_MACRO_CLASS_T0, T) class X, class... Ts>		\
	using func = make_lazy<F<DTML_MACRO_TRAILING_DEC1(M, DTML_MACRO_COMMA, DTML_MACRO_T0, R) value_v<V>,		\
		                 DTML_MACRO_TRAILING_DEC1(N, DTML_MACRO_COMMA, DTML_MACRO_T0, S) value_v<W>,		\
		                 DTML_MACRO_TRAILING_DEC1(K, DTML_MACRO_COMMA, DTML_MACRO_T0, T) value_v<X>, Ts...>>;	\
};															\
															\
template<template<DTML_MACRO_TRAILING_DEC1(M, DTML_MACRO_COMMA, DTML_MACRO_CONST, class) auto,				\
		  DTML_MACRO_TRAILING_DEC1(N, DTML_MACRO_COMMA, DTML_MACRO_CONST, class) auto,				\
		  DTML_MACRO_TRAILING_DEC1(K, DTML_MACRO_COMMA, DTML_MACRO_CONST, class) auto, class... Ts> class F,	\
	  DTML_MACRO_TRAILING_DEC1(M, DTML_MACRO_COMMA, DTML_MACRO_CLASS_T0, R) auto v,					\
	  DTML_MACRO_TRAILING_DEC1(N, DTML_MACRO_COMMA, DTML_MACRO_CLASS_T0, S) auto w,					\
	  DTML_MACRO_TRAILING_DEC1(K, DTML_MACRO_COMMA, DTML_MACRO_CLASS_T0, T) auto x, class...Ts, class... Us>	\
struct factors<F<DTML_MACRO_TRAILING_DEC1(M, DTML_MACRO_COMMA, DTML_MACRO_T0, R) v,					\
		 DTML_MACRO_TRAILING_DEC1(N, DTML_MACRO_COMMA, DTML_MACRO_T0, S) w,					\
		 DTML_MACRO_TRAILING_DEC1(K, DTML_MACRO_COMMA, DTML_MACRO_T0, T) x, Ts...>, Us...>			\
{															\
	using root = func_box_ ## M ## N ## K<F>;									\
	using args = q<DTML_MACRO_TRAILING_DEC1(M, DTML_MACRO_COMMA, DTML_MACRO_T0, R) value_box<v>,			\
			  DTML_MACRO_TRAILING_DEC1(N, DTML_MACRO_COMMA, DTML_MACRO_T0, S) value_box<w>,			\
			  DTML_MACRO_TRAILING_DEC1(K, DTML_MACRO_COMMA, DTML_MACRO_T0, T) value_box<x>, Ts...>;		\
};															\
															\
template<template<DTML_MACRO_TRAILING_DEC1(M, DTML_MACRO_COMMA, DTML_MACRO_CONST, class) auto,				\
		  DTML_MACRO_TRAILING_DEC1(N, DTML_MACRO_COMMA, DTML_MACRO_CONST, class) auto,				\
		  DTML_MACRO_TRAILING_DEC1(K, DTML_MACRO_COMMA, DTML_MACRO_CONST, class) auto, class... Ts> class F>	\
constexpr func_box_ ## M ## N ## K<F> put_func_in_a_box()

DTML_MACRO_DEC0(DTML_FUNC_BOX_MAX_TRIPLE_SKIP, DTML_MACRO_SEMI, DTML_MACRO_APPLY_ROL0, DTML_MACRO_DEC2,
		DTML_FUNC_BOX_MAX_TRIPLE_SKIP, DTML_MACRO_SEMI, DTML_MACRO_APPLY_ROL1, DTML_MACRO_DEC4,
		DTML_FUNC_BOX_MAX_TRIPLE_SKIP, DTML_MACRO_SEMI, DTML_FUNC_BOX_MNK);

#undef DTML_FUNC_BOX_MNK


#define DTML_FUNC_BOX_MNKL(M, N, K, L, ...)										\
template<template<DTML_MACRO_TRAILING_DEC1(M, DTML_MACRO_COMMA, DTML_MACRO_CONST, class) auto,				\
		  DTML_MACRO_TRAILING_DEC1(N, DTML_MACRO_COMMA, DTML_MACRO_CONST, class) auto,				\
		  DTML_MACRO_TRAILING_DEC1(K, DTML_MACRO_COMMA, DTML_MACRO_CONST, class) auto,				\
		  DTML_MACRO_TRAILING_DEC1(L, DTML_MACRO_COMMA, DTML_MACRO_CONST, class) auto, class... Ts> class F>	\
struct func_box_ ## M ## N ## K ## L											\
{															\
	template<DTML_MACRO_TRAILING_DEC1(M, DTML_MACRO_COMMA, DTML_MACRO_CLASS_T0, Q) class V,				\
		 DTML_MACRO_TRAILING_DEC1(N, DTML_MACRO_COMMA, DTML_MACRO_CLASS_T0, R) class W,				\
		 DTML_MACRO_TRAILING_DEC1(K, DTML_MACRO_COMMA, DTML_MACRO_CLASS_T0, S) class X,				\
		 DTML_MACRO_TRAILING_DEC1(L, DTML_MACRO_COMMA, DTML_MACRO_CLASS_T0, T) class Y, class... Ts>		\
	using func = make_lazy<F<DTML_MACRO_TRAILING_DEC1(M, DTML_MACRO_COMMA, DTML_MACRO_T0, Q) value_v<V>,		\
		                 DTML_MACRO_TRAILING_DEC1(N, DTML_MACRO_COMMA, DTML_MACRO_T0, R) value_v<W>,		\
		                 DTML_MACRO_TRAILING_DEC1(K, DTML_MACRO_COMMA, DTML_MACRO_T0, S) value_v<X>,		\
		                 DTML_MACRO_TRAILING_DEC1(L, DTML_MACRO_COMMA, DTML_MACRO_T0, T) value_v<Y>, Ts...>>;	\
};															\
															\
template<template<DTML_MACRO_TRAILING_DEC1(M, DTML_MACRO_COMMA, DTML_MACRO_CONST, class) auto,				\
		  DTML_MACRO_TRAILING_DEC1(N, DTML_MACRO_COMMA, DTML_MACRO_CONST, class) auto,				\
		  DTML_MACRO_TRAILING_DEC1(K, DTML_MACRO_COMMA, DTML_MACRO_CONST, class) auto,				\
		  DTML_MACRO_TRAILING_DEC1(L, DTML_MACRO_COMMA, DTML_MACRO_CONST, class) auto, class... Ts> class F,	\
	  DTML_MACRO_TRAILING_DEC1(M, DTML_MACRO_COMMA, DTML_MACRO_CLASS_T0, Q) auto v,					\
	  DTML_MACRO_TRAILING_DEC1(N, DTML_MACRO_COMMA, DTML_MACRO_CLASS_T0, R) auto w,					\
	  DTML_MACRO_TRAILING_DEC1(K, DTML_MACRO_COMMA, DTML_MACRO_CLASS_T0, S) auto x,					\
	  DTML_MACRO_TRAILING_DEC1(L, DTML_MACRO_COMMA, DTML_MACRO_CLASS_T0, T) auto y, class... Ts, class... Us>	\
struct factors<F<DTML_MACRO_TRAILING_DEC1(M, DTML_MACRO_COMMA, DTML_MACRO_T0, Q) v,					\
		 DTML_MACRO_TRAILING_DEC1(N, DTML_MACRO_COMMA, DTML_MACRO_T0, R) w,					\
		 DTML_MACRO_TRAILING_DEC1(K, DTML_MACRO_COMMA, DTML_MACRO_T0, S) x,					\
		 DTML_MACRO_TRAILING_DEC1(L, DTML_MACRO_COMMA, DTML_MACRO_T0, T) y, Ts...>, Us...>			\
{															\
	using root = func_box_ ## M ## N ## K ## L<F>;									\
	using args = q<DTML_MACRO_TRAILING_DEC1(M, DTML_MACRO_COMMA, DTML_MACRO_T0, Q) value_box<v>,			\
			  DTML_MACRO_TRAILING_DEC1(N, DTML_MACRO_COMMA, DTML_MACRO_T0, R) value_box<w>,			\
			  DTML_MACRO_TRAILING_DEC1(K, DTML_MACRO_COMMA, DTML_MACRO_T0, S) value_box<x>,			\
			  DTML_MACRO_TRAILING_DEC1(L, DTML_MACRO_COMMA, DTML_MACRO_T0, T) value_box<y>, Ts...>;		\
};															\
															\
template<template<DTML_MACRO_TRAILING_DEC1(M, DTML_MACRO_COMMA, DTML_MACRO_CONST, class) auto,				\
		  DTML_MACRO_TRAILING_DEC1(N, DTML_MACRO_COMMA, DTML_MACRO_CONST, class) auto,				\
		  DTML_MACRO_TRAILING_DEC1(K, DTML_MACRO_COMMA, DTML_MACRO_CONST, class) auto,				\
		  DTML_MACRO_TRAILING_DEC1(L, DTML_MACRO_COMMA, DTML_MACRO_CONST, class) auto, class... Ts> class F>	\
constexpr func_box_ ## M ## N ## K ## L<F> put_func_in_a_box()

DTML_MACRO_DEC0(DTML_FUNC_BOX_MAX_QUADRUPLE_SKIP, DTML_MACRO_SEMI, DTML_MACRO_APPLY_ROL0, DTML_MACRO_DEC2,
		DTML_FUNC_BOX_MAX_QUADRUPLE_SKIP, DTML_MACRO_SEMI, DTML_MACRO_APPLY_ROL1, DTML_MACRO_DEC4,
		DTML_FUNC_BOX_MAX_QUADRUPLE_SKIP, DTML_MACRO_SEMI, DTML_MACRO_APPLY_ROL2, DTML_MACRO_DEC6,
		DTML_FUNC_BOX_MAX_QUADRUPLE_SKIP, DTML_MACRO_SEMI, DTML_FUNC_BOX_MNKL);

#undef DTML_FUNC_BOX_MNKL


template<template<class, auto...> class Seq> concept Sequence = requires { typename Seq<int, 0, 1, 2, 3, 4>; };

template<template<class, auto...> class Sequence> struct seq_box {
	template<class T, class... Ts> using func = make_lazy<Sequence<T, value_v<Ts>...>>;
};

template<template<class, auto...> class Seq, class T, auto... ts, class... Us> requires Sequence<Seq> struct factors<Seq<T, ts...>, Us...> {
	using root = seq_box<Seq>;
	using args = q<T, value_box<ts>...>;
};

template<template<class, auto...> class Seq> requires Sequence<Seq> constexpr seq_box<Seq> put_func_in_a_box();

}

#endif
