#ifndef DTML__SHOW_H_d3a9e4cf7db2a24bf5dbc0c51ec1363f394757ea
#define DTML__SHOW_H_d3a9e4cf7db2a24bf5dbc0c51ec1363f394757ea

#include <string>
#include <type_traits>

#include "basic.h"
#include "func_box.h"
#include "list.h"
#include "string.h"

namespace dtml {

namespace show_ {

template<class T> struct show_root: value_box<string{"$unknown$"}> {};

template<class T> static constexpr string show_root_v = value_v<show_root<root_t<T>>>;

template<class T> struct show_args;

template<class T> static constexpr string show_args_v = value_v<show_args<args_t<T>>>;

}

template<class... Ts> struct show;

template<class... Ts> static constexpr string show_v = value_v<show<Ts...>>;

template<class... Ts> struct show: value_box<string{", "}.join(show_v<Ts>...)> {};

template<class T> struct show<T>: value_box<show_::show_root_v<T> + show_::show_args_v<T>> {};


template<class T> struct show<const T>: value_box<string{"const "} + show_v<T>> {};

template<class T> struct show<volatile T>: value_box<string{"volatile "} + show_v<T>> {};

template<class T> struct show<const volatile T>: value_box<string{"const volatile "} + show_v<T>> {};

template<class T> struct show<T &>: value_box<show_v<T> + string{" &"}> {};

template<class T> requires (!std::is_reference_v<T>) struct show<T &&>: value_box<show_v<T> + string{" &&"}> {};

template<class T> struct show<T *>: value_box<show_v<T> + string{" *"}> {};

template<class T> struct show<T *const>: value_box<show_v<T> + string{" *const"}> {};


namespace show_ {

template<class T> struct show_args: value_box<'<' + value_v<apply_t<DTML_FUNC_BOX(show), T>> + '>'> {};

template<> struct show_args<null>: empty_string {};

}

namespace show_ {

#define DTML_SHOW_AS(name, ...) namespace dtml::show_ { template<> struct show_root<DTML_FUNC_BOX(__VA_ARGS__)>: value_box<string{name}> {}; }

#define DTML_SHOW(...) DTML_SHOW_AS(#__VA_ARGS__, __VA_ARGS__)

}

}

DTML_SHOW(dtml::show);

DTML_SHOW(void);
DTML_SHOW(nullptr_t);
DTML_SHOW(bool);
DTML_SHOW(signed char);
DTML_SHOW(unsigned char);
DTML_SHOW(char);
DTML_SHOW(wchar_t);
DTML_SHOW(char16_t);
DTML_SHOW(char32_t);
DTML_SHOW(int);
DTML_SHOW(short int);
DTML_SHOW(long int);
DTML_SHOW(long long int);
DTML_SHOW(unsigned int);
DTML_SHOW(unsigned short int);
DTML_SHOW(unsigned long int);
DTML_SHOW(unsigned long long int);
DTML_SHOW(float);
DTML_SHOW(double);
DTML_SHOW(long double);

// DTML types themselves.
namespace dtml {

#ifdef DTML_TEST_GEN
template<class T, auto v> struct show<std::integral_constant<T, v>>: box<value_box<string{"std::integral_constant<"} + show_v<T> + ", " + to_string(v) + '>'>> {};

template<auto v> struct show<value_box<v>>: box<value_box<string{"value_box<("} + show_v<typename value_box<v>::value_type> + ")" + to_string(value_box<v>::value) + '>'>> {};
#else
template<auto v> struct show<value_box<v>>: box<value_box<to_string(v)>> {};
#endif

template<auto v> struct show<untyped_constant<v>>: box<value_box<to_string(untyped_constant<v>{})>> {};

//template<template<class...> class F> struct show<func_box<F>>: value_box<string{"func_box<"} + show_::show_root_v<DTML_FUNC_BOX(F)> + '>'> {};
template<template<class...> class F> struct show<func_box<F>>: value_box<string{"func_box<"} + show_::show_root_v<F<>> + '>'> {};

}

#ifdef DTML_TEST_GEN
DTML_SHOW(string);

DTML_SHOW(strict::append);
DTML_SHOW(strict::apply);
//DTML_SHOW(strict::bind);
DTML_SHOW(strict::car);
DTML_SHOW(strict::cdr);
DTML_SHOW(strict::compose);
DTML_SHOW(strict::concat);
DTML_SHOW(strict::cond);
DTML_SHOW(strict::cons);
DTML_SHOW(strict::empty);
DTML_SHOW(strict::fmap);
DTML_SHOW(strict::force_list);
DTML_SHOW(strict::init);
DTML_SHOW(strict::power);
DTML_SHOW(strict::right_accumulate);
DTML_SHOW(strict::until);

DTML_SHOW(append);
DTML_SHOW(apply);
DTML_SHOW(arg);
//DTML_SHOW(bind);
DTML_SHOW(car);
DTML_SHOW(cdr);
DTML_SHOW(compose);
DTML_SHOW(concat);
DTML_SHOW(cond);
DTML_SHOW(cons);
DTML_SHOW(dec);
DTML_SHOW(empty);
DTML_SHOW(exception);
DTML_SHOW(factors);
DTML_SHOW(fmap);
DTML_SHOW(inc);
DTML_SHOW(init);
DTML_SHOW(invoke);
DTML_SHOW(is_arg);
DTML_SHOW(konst);
//DTML_SHOW(lambda);
DTML_SHOW(last);
DTML_SHOW(left_accumulate);
DTML_SHOW(length);
DTML_SHOW(maybe);
DTML_SHOW(null);
DTML_SHOW(power);
DTML_SHOW(q);
DTML_SHOW(right_accumulate);
DTML_SHOW(sequence);
DTML_SHOW(until);
//DTML_SHOW(value_box);
DTML_SHOW(values);
DTML_SHOW(force_list);

//DTML_SHOW(var);

/*
DTML_SHOW(strict::at);
DTML_SHOW(strict::conjunction);
DTML_SHOW(strict::disjunction);
DTML_SHOW(strict::drop);
DTML_SHOW(strict::filter);
DTML_SHOW(strict::iterate);
DTML_SHOW(strict::range);
DTML_SHOW(strict::take);
DTML_SHOW(strict::transform);

DTML_SHOW(at);
DTML_SHOW(conjunction);
DTML_SHOW(disjunction);
DTML_SHOW(drop);
DTML_SHOW(filter);
DTML_SHOW(iterate);
DTML_SHOW(range);
DTML_SHOW(take);
DTML_SHOW(transform);
*/
#else
DTML_SHOW(dtml::string);

DTML_SHOW(dtml::strict::append);
DTML_SHOW(dtml::strict::apply);
//DTML_SHOW(dtml::strict::bind);
DTML_SHOW(dtml::strict::car);
DTML_SHOW(dtml::strict::cdr);
DTML_SHOW(dtml::strict::compose);
DTML_SHOW(dtml::strict::concat);
DTML_SHOW(dtml::strict::cond);
DTML_SHOW(dtml::strict::cons);
DTML_SHOW(dtml::strict::empty);
DTML_SHOW(dtml::strict::fmap);
DTML_SHOW(dtml::strict::force_list);
DTML_SHOW(dtml::strict::init);
DTML_SHOW(dtml::strict::power);
DTML_SHOW(dtml::strict::right_accumulate);
DTML_SHOW(dtml::strict::until);

DTML_SHOW(dtml::append);
DTML_SHOW(dtml::apply);
DTML_SHOW(dtml::arg);
//DTML_SHOW(dtml::bind);
DTML_SHOW(dtml::car);
DTML_SHOW(dtml::cdr);
DTML_SHOW(dtml::compose);
DTML_SHOW(dtml::concat);
DTML_SHOW(dtml::cond);
DTML_SHOW(dtml::cons);
DTML_SHOW(dtml::dec);
DTML_SHOW(dtml::empty);
DTML_SHOW(dtml::exception);
DTML_SHOW(dtml::factors);
DTML_SHOW(dtml::fmap);
DTML_SHOW(dtml::inc);
DTML_SHOW(dtml::init);
DTML_SHOW(dtml::invoke);
DTML_SHOW(dtml::is_arg);
DTML_SHOW(dtml::konst);
//DTML_SHOW(dtml::lambda);
DTML_SHOW(dtml::last);
DTML_SHOW(dtml::left_accumulate);
DTML_SHOW(dtml::length);
DTML_SHOW(dtml::maybe);
DTML_SHOW(dtml::null);
DTML_SHOW(dtml::power);
DTML_SHOW(dtml::q);
DTML_SHOW(dtml::quote);
DTML_SHOW(dtml::right_accumulate);
DTML_SHOW(dtml::sequence);
DTML_SHOW(dtml::until);
//DTML_SHOW(dtml::value_box);
DTML_SHOW(dtml::values);
DTML_SHOW(dtml::values_pack);
DTML_SHOW(dtml::force_list);


//DTML_SHOW(dtml::strict::at);
DTML_SHOW(dtml::strict::drop);
DTML_SHOW(dtml::strict::filter);
DTML_SHOW(dtml::strict::iterate);
//DTML_SHOW(dtml::strict::range);
DTML_SHOW(dtml::strict::take);
DTML_SHOW(dtml::strict::transform);

//DTML_SHOW(dtml::at);
DTML_SHOW(dtml::drop);
DTML_SHOW(dtml::filter);
DTML_SHOW(dtml::iterate);
//DTML_SHOW(dtml::range);
DTML_SHOW(dtml::take);
DTML_SHOW(dtml::transform);
#endif

DTML_SHOW(std::basic_string);
DTML_SHOW(std::integral_constant);

#endif
