#!/bin/sh

IMPROPERLY_DOCUMENTED=macro.h,show.h
HEADERS=$(git diff --cached --name-only $against | gawk -F / 'BEGIN {
	split("'"IMPROPERLY_DOCUMENTED"'", improperly_documented, /,/)
	for(i in improperly_documented) {
		improperly_documented[improperly_documented[i]] = ""
		delete improperly_documented[i]
	}
}
/^include\/dtml\// && !($3 in improperly_documented) {
	$1 = $2 = ""
	print
}')

echo "Headers: $HEADERS" > /home/denistrapeznikov/wat.txt

for h in $HEADERS ; do
	DOC="doc/${h%.h}.md"
	echo Header $h >> /home/denistrapeznikov/wat.txt
	gawk -f scripts/document.awk include/dtml/$h > $DOC
	git add $DOC
done
