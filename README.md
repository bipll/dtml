# DTML - a Different Template Metaprogramming Library

This library follows a filosophy to be describer later in this file. For now, other doc files are available:

- [dtml/assoc.h](doc/assoc.md) — assoc lists, compile-time key-value storages
- [dtml/let.h](doc/let.md) — [let expressions](https://en.wikipedia.org/wiki/Let_expression) producing types at compile time, with pattern matching
- [dtml/macro.h](doc/macro.md) — a low-level, partly expressive macro library
- [dtml/show.h](doc/show.md) — definitions to facilitate (almost auto-generate) string representations of C++ types
