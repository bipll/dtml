BEGIN {
	paragraph = 0
	macro[0] = ""
	delete macro[0]
	const[0] = ""
	delete const[0]
	value[0] = ""
	delete value[0]
	name[0] = ""
	delete name[0]
}
/^\/\*\*/ {
	on = 1
	next
}
on == 2 && /^#\s*define / {
	addname("^#\\s*define\\s*", macro)
#	sub(/^#\s*define\s*/, "")
#	sub(/\>.*/, "")
#	macro[paragraph] = $0
#	on = 0
#	++paragraph
#	next
}
on == 2 && /^(class|struct|using) / {
	addname("^(class|struct|using) ", const)
#	sub(/^(class|struct) /, "")
#	sub(/\>.*/, "")
#	const[paragraph] = $0
#	on = 0
#	++paragraph
#	next
}
on == 2 && /^template<.*> (class|struct|using) / {
	addname("^template<.*> (class|struct|using) ", value)
}
on == 2 {
	addname(".*\\(", name)
#	sub(/.*\(/, "")
#	sub(/\>.*/, "")
#	name[paragraph] = $0
#	on = 0
#	++paragraph
#	next
}
on && /^ \*\// {
	descr[paragraph] = text
	params[paragraph] = pars
	returns[paragraph] = ret
	throws[paragraph] = thrs
	text = pars = ret = thrs = ""
	if(paragraph > 0) on = 2
	else {
		on = 0
		++paragraph
	}
	next
}
on && /^ \* @param / {
	# assuming param description cannot be empty
	param = $3
	$1 = $2 = $3 = ""
	sub(/^\s*/, "")
	pars = pars "|`" param "`|" $0 "|\n"
	next
}
on && /^ \* @return/ {
	$1 = $2 = ""
	sub(/^\s*/, "")
	sub(/^$/, "_see above_")
	ret = $0
	next
}
on && /^ \* @throw/ {
	$1 = $2 = ""
	sub(/^\s*/, "")
	sub(/^$/, "_see above_")
	thrs = thrs "- " $0 "\n"
	next
}
on && /^ \*/ {
	text = text substr($0, 4) "\n"
	next
}
END {
	sub(/^include\//, "", FILENAME)
	print FILENAME
	filename = FILENAME
	gsub(/./, "=", filename)
	print filename "\n"
	print descr[0]
	sub(/h$/, "md", FILENAME)

	if(nonempty(macro) || nonempty(const) || nonempty(value) || nonempty(name)) {
		print("## Table of contents")
		toc("macros", "macros", macro)
		toc("singletons", "singletons-ie-plain-c-types", const)
		toc("simple types", "simple-types-ie-simple-c-templates", value)
		toc("functions", "functions-ie-lazy-dtml-templates", name)
	}

	printchapter("macros", macro)
	printchapter("singletons (i.e. plain C++ types)", const)
	printchapter("simple types (i.e. simple C++ templates)", value)
	printchapter("functions (i.e. lazy DTML templates)", name)

#	if(nonempty(macro)) {
#		print "## The following macros are defined in this header"
#		print ""
#		for(i = 1; i < paragraph; ++i) if(i in macro) {
#			print "- **[`" macro[i] "`](#" tolower(macro[i]) ")**"
#		}
#		hl = 0
#		for(i = 1; i < paragraph; ++i) {
#			if(!(i in macro)) continue
#			if(hl) print "\n---"
#			++hl
#			print "\n### `" name[i] "`"
#			print descr[i]
#			if(params[i] != "") {
#				print "#### Params"
#				print "|||"
#				print "|---|---|"
#				printf("%s", params[i])
#			}
#			if(returns[i] != "") {
#				print "\n#### Returns"
#				print returns[i]
#			}
#			if(throws[i] != "") {
#				print "\n#### Exceptions"
#				print throws[i]
#			}
#		}
#	}
#	if(nonempty(name)) {
#		print "## The following functions are defined in this header"
#		print ""
#		for(i = 1; i < paragraph; ++i) if(i in name) {
#			print "- **[`" name[i] "`](#" name[i] ")**"
#		}
#		print ""
#		for(i = 1; i < paragraph; ++i) {
#			if(!(i in name)) continue
#			if(hl) print "\n---"
#			++hl
#			print "\n### `" name[i] "`"
#			print descr[i]
#			print "##### Params"
#			print "|||"
#			print "|---|---|"
#			printf("%s", params[i])
#			print "\n##### Returns"
#			print returns[i]
#			if(throws[i] != "") {
#				print "\n##### Exceptions"
#				print throws[i]
#			}
#		}
#	}
}
function nonempty(a) {
	for(x in a) return 1
	return 0
}
function addname(keyword, a) {
	sub(keyword,  "")
	sub(/\>.*/, "")
	a[paragraph] = $0
	on = 0
	++paragraph
	next
}
function printchapter(name, a) {
	if(nonempty(a)) {
		print "## The following " name " are defined in this header"
		print ""
		for(i = 1; i < paragraph; ++i) if(i in a) {
			print "- **[`" a[i] "`](#" tolower(a[i]) ")**"
		}
		hl = 0
		for(i = 1; i < paragraph; ++i) {
			if(!(i in a)) continue
			if(hl) print "\n---"
			++hl
			print "\n### `" a[i] "`"
			print descr[i]
			if(params[i] != "") {
				print "#### Params"
				print "|||"
				print "|---|---|"
				printf("%s", params[i])
			}
			if(returns[i] != "") {
				print "\n#### Returns"
				print returns[i]
			}
			if(throws[i] != "") {
				print "\n#### Exceptions"
				print throws[i]
			}
		}
	}
}
function toc(name, link, a) {
	if(nonempty(a)) {
		print "* [" name "](#the-following-" link "-are-defined-in-this-header)"
	}
}
