all: tests

tests: test_assoc test_basic test_let test_list

test_assoc: tests/test_assoc.cpp
	$(CXX) -std=c++20 tests/test_assoc.cpp -Iinclude -o build/test_assoc $(CXXFLAGS)

test_basic: tests/test_basic.cpp
	$(CXX) -std=c++20 tests/test_basic.cpp -Iinclude -o build/test_basic $(CXXFLAGS)

test_let: tests/test_let.cpp
	$(CXX) -std=c++20 tests/test_let.cpp -Iinclude -o build/test_let $(CXXFLAGS)

test_list: tests/test_list.cpp
	$(CXX) -std=c++20 tests/test_list.cpp -Iinclude -o build/test_list $(CXXFLAGS)
